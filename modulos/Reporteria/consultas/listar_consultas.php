
    <script type="text/javascript">
        $(document).ready(function () {
            createdt($("#tabla-consulta"))
        });
    </script>
</head>
<body>

<section id="principal">
    <article id="cuerpo">
        <article id="contenido">
            <div class="reporte">

            </div>
            <table id="tabla-consulta" class="table table-hover" style='width:100%;'>
                <thead>
                <tr>
                    <th>Consulta</th>
                </tr>
                </thead>
                <tbody>
                <?php

                $perfil = $rowlog['perid'];
                $qryConsultas = $db->query("SELECT cdiid, cdinombre FROM perfil_reporte INNER JOIN consultasdinamicas ON reporte = cdiid WHERE perfil = $perfil AND tipo = 1 AND cdiactiva = true ORDER BY cdinombre ASC");

                while ($row = $qryConsultas->fetch(PDO::FETCH_ASSOC)) {

                    ?>
                    <tr>
                        <td title="<?php echo $row['cdiid'] ?>">
                            <a onclick="go_to_report_consulta(<?php echo $row['cdiid'] ?>);" style="cursor:pointer; color:black;">  <?php echo $row['cdinombre'] ?> </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </article>
    </article>
</body>
<script>
function go_to_report_consulta(reporte)
{
    var url = " <?php echo $r . 'modulos/Reporteria/consultas/consulta.php?id='?>" + reporte;
    // Abrir nuevo tab
    var win = window.open(url, '_blank');
    // Cambiar el foco al nuevo tab (punto opcional)
    win.focus();
}
</script>