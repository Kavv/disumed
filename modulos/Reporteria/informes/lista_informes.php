    <script type="text/javascript">
		$(document).ready(function() {
            createdt($("#tabla"));
        });
    </script>
<section id="principal">
    <article id="cuerpo">
        <article id="contenido">
            <div class="reporte">
            </div>
            <table id="tabla" class="table table-hover" style='width:100%;'>
                <thead>
                <tr>
                    <th>Consulta</th>
                    <th>Concepto</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $perfil = $rowlog['perid'];
                $qryConsultas = $db->query("SELECT idiid, idinombre, ididescripcion FROM perfil_reporte INNER JOIN informesdinamicos ON reporte = idiid WHERE perfil = $perfil AND tipo = 2 and ocultar = false ORDER BY idinombre ASC");
                while ($row = $qryConsultas->fetch(PDO::FETCH_ASSOC)) {

                    ?>
                    <tr>
                        <td title="<?php echo $row['idiid'] ?>">
                            <a onclick="go_to_report(<?php echo $row['idiid'] ?>);" style="cursor:pointer; color:black;"> <?php echo $row['idinombre'] ?> </a>
                        </td>
                        <td><?php echo $row['ididescripcion'] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </article>
    </article>
</section>
<script>
function go_to_report(reporte)
{
    var url = " <?php echo $r . 'modulos/Reporteria/informes/informe.php?id='?>" + reporte;
    // Abrir nuevo tab
    var win = window.open(url, '_blank');
    // Cambiar el foco al nuevo tab (punto opcional)
    win.focus();
}
</script>