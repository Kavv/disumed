<?php

$r = '../../../';

require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/phpjasper/src/PHPJasper.php');
require($r . 'incluir/funciones.php');

use PHPJasper\PHPJasper;

if (isset($_GET['idiid'])) {

    $idiId = $_GET['idiid'];

    $extension = $_GET['exportOption'];

    $rowInfo = $db->query("select * from informesdinamicos where idiId = $idiId and ocultar = false")->fetch(PDO::FETCH_ASSOC);

    $jasperFileName = $rowInfo['idinombrejasper'];
    $nombreInforme = $rowInfo['idinombre'];

    $outputName = nombreAlaeotorioDeArchivo(15, "", "");

    $input = __DIR__ . '/files/' . $jasperFileName;
    $output = __DIR__ . '/output/' . $nombreInforme . " - " . $outputName;
    /* $input = $r. 'modulos/Reporteria/informes/files/' . $jasperFileName; */
    /* $output = $r. 'modulos/Reporteria/informes/output/' . $nombreInforme . " - " . $outputName; */

    $options = [
        'format' => ["$extension"],
        'locale' => 'en',
        'db_connection' => [
            'driver' => 'postgres',
            'username' => 'postgres',
            'password' => 'Disumed2023',
            'host' => 'disumed.cfczb0nahlhq.us-east-1.rds.amazonaws.com',
            'database' => 'Disumed',
            'port' => '5432'
        ]
    ];



    $buildingParams = [];

    foreach ($_GET as $key => $value) {
        if ($key != "idiid" && $key != "consultar" && $key != "exportOption") {
            $buildingParams += array("$key" => "$value");
        }
    }
    $options += array("params" => $buildingParams);

    $PHPJasper = new PHPJasper();
    /* echo $PHPJasper->process(
        $input,
        $output,
        $options
    )->output();
    exit(); */

    $PHPJasper->process(
        $input,
        $output,
        $options
    )->execute();

    $path = $output . '.' . $extension;
    
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . basename($path) . '"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($path));
    flush(); // Flush system output buffer
    
    readfile($path);
    unlink($path);
    
    die();

}