<?php

$codigo = $_GET['id1'];
$row = $db->query("SELECT * FROM 
(
	(
		(
			(
				usuarios LEFT JOIN perfiles ON usuperfil = perid
			) 
			LEFT JOIN departamentos ON usudepto = depid
		) 
		LEFT JOIN ciudades ON 
		(
			usudepto = ciudepto AND usuciudad = ciuid
		)
	) 
) LEFT JOIN grupos ON usuGrupo = grunombre WHERE usuid = '$codigo'")->fetch(PDO::FETCH_ASSOC);
$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre'] . '&perfil=' . $_GET['perfil'];
?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script>
		$(document).ready(function() {

			$('#fechaingreso').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecharetiro').datepicker('option', 'minDate', selectedDate);
				}
			});
			$('#fecharetiro').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fechaingreso').datepicker('option', 'maxDate', selectedDate);
				}
			});
			$('#depto').change(function(event) {
				var id = $('#depto').find(':selected').val();
				$('#ciudad').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id);
				$('#sede').load('<?php echo $r ?>incluir/carga/sedes.php');
			});
			$('#ciudad').change(function(event) {
				var id = $('#depto').find(':selected').val();
				var id2 = $('#ciudad').find(':selected').val();
				$('#sede').load('<?php echo $r ?>incluir/carga/sedes.php?id=' + id + '&id2=' + id2);
			});

		});
	</script>
	<section id="principal">
		<article id="cuerpo">
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="Controlador/trabajadorControlador.php?<?php echo $filtro ?>" method="post">
					
						<fieldset class="col-md-12">
							<legend class="ui-widget ui-corner-all">Insertar usuario</legend>
							<div class='row'>
								<div class="col-md-6">
									<p>
										<label for="id"><spam style="color:red">*</spam>Código:</label>
										<input type="hidden" name="id" value="<?php echo $row['usuid'] ?>">
										<input style="background:none" disabled value="<?php echo $row['usuid'] ?>" type="text" class="id validate[required]" title="Digite la identificacion" autocomplete="false"/>
									</p>
									<p>
										<label class="not-w" for="password">Cambiar contraseña:</label>
										<div class="input-group">
											<input type="text" class="form-control not-w" id='password' name='password' disabled aria-label="Amount (to the nearest dollar)">
											<div class="input-group-append">
												<span class="input-group-text">CAMBIAR</span>
												<span class="input-group-text"><input type="checkbox" value="1" onclick="document.getElementById('password').disabled = !this.checked" style="height: auto;"/></span>
											</div>
										</div>
									</p>
									<p>
										<label for="nombre"><spam style="color:red">*</spam>Nombre Completo:</label>
										<input value="<?php echo $row['usunombre'] ?>" type="text" style="width:400px" name="nombre" class="nombre validate[required] text-input" title="Digite el nombre del usuario" />
									</p>
									
									<p>
										<label for="perfil"><spam style="color:red">*</spam>Perfil:</label>
										
										<select id="perfil" name="perfil" class="validate[required] selectpicker" data-live-search="true" title="Seleccione el perfil" data-width="100%">
											<?php
												$qry = $db->query("SELECT * FROM perfiles ORDER BY pernombre");
												while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
													if($row2['perid'] == $row['usuperfil'])
														echo '<option selected value=' . $row2['perid'] . '>' . $row2['pernombre'] . '</option>';
													else
														echo '<option value=' . $row2['perid'] . '>' . $row2['pernombre'] . '</option>';
												}
											?>
										</select>
										
									</p>
									<p>
										<label for="relacionista" style="width:auto!important;">Cobrador:</label>
										<select name="relacionista" id="relacionista">
											
											<?php 
												if ($row['usurelacionista'] == 1)
												{
													echo '<option value="0">No</option>';
													echo '<option value="1" selected>Sí</option>';
												} 
												else {
													echo '<option value="0" selected>No</option>';
													echo '<option value="1" >Sí</option>';
												}
											?>
										</select>
									</p>
									<p>
										<label for="asesor" style="width:auto!important;">Asesor de ventas:</label>
										<select name="asesor" id="asesor">
											<?php 
												if ($row['usuasesor'] == 1)
												{
													echo '<option value="0">No</option>';
													echo '<option value="1" selected>Sí</option>';
												} 
												else {
													echo '<option value="0" selected>No</option>';
													echo '<option value="1" >Sí</option>';
												}
											?>
										</select>
										<label for="promotor">Asignar un grupo:</label>
										<select id="grupo" name="grupo" class="selectpicker validate[]" data-live-search="true" title="Seleccione un grupo" data-width="100%">
											<option value="">SELECCIONE</option>
											<?php
												$qry = $db->query('SELECT * FROM grupos ORDER BY grunombre');
												while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
													if($row['usugrupo'] == $row2['grunombre'])
													echo '<option selected value="' . $row2['grunombre'] . '">' . $row2['grunombre'] . '</option>';
													else
													echo '<option value="' . $row2['grunombre'] . '">' . $row2['grunombre'] . '</option>';
												}
											?>
										</select>
										<em style="font-size:13px">Debe seleccionar la opcion cobrador para asignar un grupo</em>
									</p>
								</div>
								<div class="col-md-6">
									<p>
										<label for="email">E-mail:</label>
										<input value="<?php echo $row['usuemail'] ?>" type="text" name="email" class="email validate[custom[email]] text-input" />
									</p>
									<p>
										<label for="telefono">Telefono:</label>
										<input value="<?php echo $row['usutelefono'] ?>" type="text" name="telefono" class="telefono validate[] text-input" />
									</p>
									<p>
										<label for="direccion">Direccion:</label>
										<input value="<?php echo $row['usudireccion'] ?>" type="text" name="direccion" class="direccion validate[] text-input" />
									</p>
									<p>
										<label for="depto">Departamento:</label>
										<select id="depto" name="depto" class="validate[] text-input">
											<option value="<?php echo $row['usudepto'] ?>"><?php echo $row['depnombre'] ?></option>
											<?php
											$qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['usudepto'] . "' ORDER BY depnombre");
											while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
												echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
											}
											?>
										</select>
									</p>
									<p>
										<label for="ciudad">Ciudad:</label>
										<select id="ciudad" name="ciudad" class="validate[] text-input">
											<option value="<?php echo $row['usuciudad'] ?>"><?php echo $row['ciunombre'] ?></option>
											<?php
											$depto = $row['usudepto'] ;
											$ciu = $row['usuciudad'] ;
											$qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = '$depto' AND ciuid <> '$ciu' ");
											
											while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
												echo '<option value="' . $row2['ciuid'] . '">' . $row2['ciunombre'] . '</option>';
											}
											?>
										</select>
									</p>
									<?php
										$qryem = $db->query("SELECT * FROM empresas ORDER BY empnombre")->fetch(PDO::FETCH_OBJ);
										echo "<input type='hidden' name='empresa' value='$qryem->empid'>";
									?>

									<p>
										<label for="cargo">Cargo:</label> <!-- CAMPO CARGO-->
										<input  value="<?php echo $row['usucargo'] ?>" type="text" name="cargo" class="text-input" />
									</p>
									
								</div>
							</div>
							
							
							
							
								<input type="hidden" name="sede" value=''>
								<input type="hidden" name="modalidad" value=''>

								<input type="hidden" name="fechaingreso" id="fechaingreso" class="text-input fecha" />
								<input type="hidden" name="fecharetiro" id="fecharetiro" class="text-input fecha" />
								<input type='hidden' name='salario' id='salario' class='valor validate[custom[onlyNumberSp], min[0], required] text-input' style="text-align:left!important; padding-left:2.5em;" value="0"/>
								<input type='hidden' name='auxilio' id='auxilio' class='valor validate[custom[onlyNumberSp], min[0], required] text-input' style="text-align:left!important; padding-left:2.5em;" value="0"/>
								<input type='hidden' name='topeauxilio' id='topeauxilio' class='valor validate[custom[onlyNumberSp], min[0], required] text-input' style="text-align:left!important; padding-left:2.5em;" value="0"/>

								<input type="hidden" name="director" value="1" style="width:auto!important;"/>
								<input type="hidden" name="directorcall" value="1" style="width:auto!important;" />
								<input type="hidden" id="promotor" name="promotor" value="1" style="width:auto!important;" />
								<input type="hidden" id="callcenter" name="callcenter" value="1" style="width:auto!important;" />
							
							
								<div class="row">
									<div class="col-md-6">
										<button type="button" class="btn btn-block btn-info btnatras" onClick="carga(); location.href='mostrar_trabajador.php?<?php echo $filtro ?>'">Atras</button>
									</div>
									<div class="col-md-6">
										<button type="submit" class="btn btn-block btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button>
									</div>
								</div>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
	</section>
</body>

</html>