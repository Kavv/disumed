<?php

  
	$id = $nombre = $perfil = "";
	if (isset($_POST['consultar'])) {
		$id = strtoupper($_POST['id']);
		$nombre = strtoupper($_POST['nombre']);
		$perfil = $_POST['perfil'];
	} else if (isset($_GET['id'])) {
		$id = strtoupper($_GET['id']);
		$nombre = strtoupper($_GET['nombre']);
		$perfil = $_GET['perfil'];
	}
	$con = "SELECT * FROM ((usuarios LEFT JOIN perfiles ON usuperfil = perid) LEFT JOIN departamentos ON usudepto = depid) LEFT JOIN ciudades ON (usudepto = ciudepto AND usuciudad = ciuid)";
		
	/* Los parametros de la consulta sql se genera dinamicamente 
	en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	if($id != "")
			array_push($parameters, "usuid LIKE '%$id%'" );
	if($nombre != "")
			array_push($parameters, "usunombre LIKE '%$nombre%'" );
	if($perfil != "")
			array_push($parameters, "usuperfil = '$perfil'" );


	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
			// Se agregan los parametros del WHERE
			if($index == 0)
					$sql .= " WHERE " . $parameter;
			else
					$sql .= " AND " . $parameter;
	}

	$filtro = 'id=' . $id . '&nombre=' . $nombre . '&perfil=' . $perfil;
	$qry = $db->query($sql);
?>
	<script>
		$(document).ready(function() {
			dt = createdt($("#tabla"));

			$('.confirmar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el usuario?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});
		});
	</script>
	<section id="principal">
		<div id="msj"></div>
		<article id="cuerpo">
			<article id="contenido">
				<h2>Listado de Trabajadores</h2>
				<div class="reporte">

				</div>
				<table id="tabla" class="table table-hover" style='width:100%;'>
					<thead>
						<tr>
							<th>Identificacion</th>
							<th>Nombre</th>
							<th>Perfil</th>
							<th>Departamento</th>
							<th>Ciudad</th>
							<th>Email</th>
							<th>Tel</th>
							<th>Dir</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td><?php echo $row['usuid'] ?></td>
								<td><?php echo $row['usunombre'] ?></td>
								<td><?php echo $row['pernombre'] ?></td>
								<td><?php echo $row['depnombre'] ?></td>
								<td><?php echo $row['ciunombre'] ?></td>
								<td align="center"><a href="mailto:<?php echo $row['usuemail'] ?>"><img src="<?php echo $r ?>imagenes/iconos/email.png" title="<?php echo $row['usuemail'] ?>" /></a></td>
								<td align="center"><a href="tel:<?php echo $row['usutelefono'] ?>"><img src="<?php echo $r ?>imagenes/iconos/telefono.png" title="<?php echo $row['usutelefono'] ?>" /></a></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/casa.png" title="<?php echo $row['usudireccion'] ?>" /></td>
								<td align="center"><a href="modificar.php?<?php echo 'id1=' . $row['usuid'] . '&' . $filtro ?>" onClick="carga()" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td>
								<td align="center"><a href="Controlador/trabajadorControlador.php?<?php echo 'id1=' . $row['usuid'] . '&' . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='index_trabajador.php'">Atras</button>
				</p>
			</article>
		</article>
	</section>
	
	<script src="<?php echo $r.'incluir/kavv_js/makemsj.js'?>" ></script>

	<?php
	if (isset($error)) {?>
		<script>make_alert({'type': 'danger', 'message': '<?php echo $error?>'});</script>
	<?php }
	elseif (isset($mensaje)) 
	{ ?>
		<script>make_alert({'message': '<?php echo $mensaje?>'});</script>
	<?php } ?>