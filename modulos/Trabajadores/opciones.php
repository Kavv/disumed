<?php
$r = '../../';

?>

<style>
  .first-option{
    height: 15em;
    background: #5b92f7;
    font-weight: bold;
  }    
  .first-option:hover{
    background: #3a7efb;
  }   
  .second-option{
    height: 15em;
    background: #ffc36d;
    font-weight: bold;
  }    
  .second-option:hover{
    background: #f1a436;
  }   
  .third-option{
    height: 15em;
    background: #5b92f7;
    font-weight: bold;
  }    
  .third-option:hover{
    background: #3a7efb;
  }    
</style>

<div class="row">
  <div class="col-md-4">
    <a href="index_trabajador.php" class="btn first-option d-flex align-items-center justify-content-center">GESTIONAR TRABAJADORES</a>
  </div>
  <div class="col-md-4">
    <a href="#" class="btn second-option d-flex align-items-center justify-content-center">GESTIONAR ASESORES</a>
  </div>
  <div class="col-md-4">
    <a href="#" class="btn third-option d-flex align-items-center justify-content-center">GESTIONAR COBRADORES</a>
  </div>
</div>

