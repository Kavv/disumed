

	<script>
			$(document).ready(function() {
					var die_depa_res = -1;
					var dir_ciudad_res = -1;

					var die_depa_co = -1;
					var dir_ciudad_co = -1;

					$('#departamento').change(function(event) {
							var id1 = $('#departamento').find(':selected').val();
							$("#ciudad").load('ciudades-re.php?depa=' + id1 + '&die_depa_res=' + die_depa_res + '&dir_ciudad_res=' + dir_ciudad_res);
					});
					$("#departamento").selectpicker();
					
			});
	</script>
	
	<section id="principal">
		<article id="cuerpo">
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="mostrar_trabajador.php" method="post">
						<fieldset class="col-md-6">
							<legend class="ui-widget ui-corner-all">Consultar trabajadores</legend>
							<p>
								<label for="id">Usuario:</label>
								<input type="text" name="id" class="id text-input" title="Digite la identificacion" />
							</p>
							<p>
								<label for="nombre">Nombre:</label>
								<input type="text" name="nombre" class="nombre validate[custom[onlyLetterSp]] text-input" title="Digite el nombre del usuario" />
							</p>
							<p>
								<label for="perfil">Perfil:</label>
								<select name="perfil">
									<option value="">TODOS</option>
									<?php
										$qry = $db->query("SELECT * FROM perfiles ORDER BY pernombre");
										while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row['perid'] . '>' . $row['pernombre'] . '</option>';
										}
									?>
								</select>
							</p>
							<div class="row">
								<div class="col-md-12 col-lg-12">
									<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
	</section>