<?php
  $r = '../../../';
  require($r . 'incluir/session.php');
  require($r . 'incluir/connection.php');

  // PARAMETROS DE BUSQUEDA
  $idBusqueda = $nombreBusqueda = $perfilBusqueda = "";
  if (isset($_GET['id'])) {
    $idBusqueda = strtoupper($_GET['id']);
    $nombreBusqueda = strtoupper($_GET['nombre']);
    $perfilBusqueda = $_GET['perfil'];
  }
  
  $filtro = 'id=' . $idBusqueda . '&nombre=' . $nombreBusqueda . '&perfil=' . $perfilBusqueda;


  // AGREGAR REGISTRO
  if (isset($_POST['insertar'])) 
  {
    $id = trim(strtoupper($_POST['id']));
    // SI SE DIFINE UNA CONTRASEÑA LA ENCRIPTAMOS
    $password = (isset($_POST['password']) && $_POST['password'] != '') ? sha1($_POST['password']) : null;
    $nombre = trim(strtoupper($_POST['nombre']));
    $email = trim(strtolower($_POST['email']));
    $telefono = trim($_POST['telefono']);
    $direccion = trim($_POST['direccion']);

    $fechaingreso = $_POST['fechaingreso'];
    $fecharetiro = $_POST['fecharetiro'];
    $salario = $_POST['salario'];
    $auxilio = $_POST['auxilio'];
    $topeauxilio = $_POST['topeauxilio'];
    $empresa = $_POST['empresa'];
    $cargo = strtoupper(trim($_POST['cargo']));
    $modalidad = $_POST['modalidad'];
    
    $perfil = $_POST['perfil'];
    $depto = $_POST['depto'];
    $ciudad = $_POST['ciudad'];
    $sede = $_POST['sede'];
    $director = $directorcall = $asesor = $promotor = $callcenter = $relacionista = 0;
    if(isset($_POST['director'])) 
      $director = $_POST['director'];
    if(isset($_POST['directorcall'])) 
      $directorcall = $_POST['directorcall'];
    if(isset($_POST['asesor'])) 
      $asesor = $_POST['asesor'];
    if(isset($_POST['promotor'])) 
      $promotor = $_POST['promotor'];
    if(isset($_POST['callcenter'])) 
      $callcenter = $_POST['callcenter'];
    if(isset($_POST['relacionista'])) 
      $relacionista = $_POST['relacionista'];
    if(!isset($_POST['grupo'])) 
      $_POST['grupo'] = "";
    if(!isset($_POST['jefecall'])) 
      $_POST['jefecall'] = "";
    if(!isset($_POST['jefeasesor'])) 
      $_POST['jefeasesor'] = "";

    $grupo = $_POST['grupo'];
    $jefecall = $_POST['jefecall'];
    $jefeasesor = $_POST['jefeasesor'];
    
    $qry = $db->query("INSERT INTO usuarios (usuid, usupassword, usunombre, usuperfil, usudepto, usuciudad, ususede, usuemail, usutelefono, usudireccion, usudirector, usudirectorcall, usuasesor, usupromotor, usuGrupo, usurelacionista, usucallcenter, usujefecallcenter, usufechaingreso, usufecharetiro, ususalariocotizacion, usuauxiliotransporte, usutopeauxiliotransporte, usuempresa, usucargo, usumodalidad, usujefeasesor) VALUES ('$id', '$password', '$nombre', $perfil, '$depto', '$ciudad', '$sede', '$email', '$telefono', '$direccion', '$director', '$directorcall', '$asesor', '$promotor', '$grupo', '$relacionista', '$callcenter', '$jefecall', '$fechaingreso', '$fecharetiro', '$salario', '$auxilio', '$topeauxilio', '$empresa', '$cargo', '$modalidad', '$jefeasesor')");
    
    if ($qry) $mensaje = 'Se inserto el usuario';
    else $error = 'No se pudo insertar el usuario';
    
    $parametro = isset($error) ? "?error=$error" : "?mensaje=$mensaje";
    header("Location:../index.php".$parametro);
    exit();
  }

  // MODIFICAR REGISTRO
  if (isset($_POST['modificar'])) {
    $id = trim(strtoupper($_POST['id']));
    $nombre = trim(strtoupper($_POST['nombre']));
    $email = trim(strtolower($_POST['email']));
    $telefono = trim($_POST['telefono']);
    $direccion = trim($_POST['direccion']);
  
    $empresa = $_POST['empresa'];
    $cargo = strtoupper(trim($_POST['cargo']));
    $modalidad = $_POST['modalidad'];
  
    $perfil = $_POST['perfil'];
    $depto = $_POST['depto'];
    $ciudad = $_POST['ciudad'];
    $sede = 1;
    $asesor = $promotor = $relacionista = 0;
    if (isset($_POST['asesor']))
      $asesor = $_POST['asesor'];
    if (isset($_POST['promotor']))
      $promotor = $_POST['promotor'];
    if (isset($_POST['relacionista']))
      $relacionista = $_POST['relacionista'];
    if(!isset($_POST['grupo'])) 
      $_POST['grupo'] = "";
  
    $grupo = $_POST['grupo'];
    
    if (!isset($_POST['password']))
      $qry = $db->query("UPDATE usuarios SET usunombre = '$nombre', usuperfil = $perfil, usudepto = '$depto', usuciudad = '$ciudad', ususede = $sede, usuemail = '$email', usutelefono = '$telefono', usudireccion = '$direccion',  usuasesor = '$asesor', usupromotor = '$promotor', usuGrupo = '$grupo', usurelacionista = '$relacionista', usuempresa = '$empresa', usucargo = '$cargo', usumodalidad = '$modalidad' WHERE usuid = '$id'") or die($db->errorInfo()[2]);
    else {
      $password = sha1($_POST['password']);
      $qry = $db->query("UPDATE usuarios SET usupassword = '$password', usunombre = '$nombre', usuperfil = $perfil, usudepto = '$depto', usuciudad = '$ciudad', ususede = $sede, usuemail = '$email', usutelefono = '$telefono', usudireccion = '$direccion',  usuasesor = '$asesor', usupromotor = '$promotor', usuGrupo = '$grupo', usurelacionista = '$relacionista', usuempresa = '$empresa', usucargo = '$cargo', usumodalidad = '$modalidad' WHERE usuid = '$id'");
    }
    if ($qry) $mensaje = 'Se actualizo el trabjador';
    else $error = 'No se actualizo el trabjador';
    
    $parametro = $error ? "?error=$error" : "?mensaje=$mensaje";
    $parametro .= "&$filtro";
    header("Location:../mostrar_trabajador.php".$parametro);
    exit();
  }

  // ELIMINAMOS EL REGISTRO
  if (isset($_GET['id1'])) {
    $id1 = $_GET['id1'];
    // VERIFICAMOS EL TRABAJADOR TIENE ASOCIADO ALGUN RAZONAMIENTO
    $num = $db->query("SELECT * FROM hiscarteras WHERE hisusuario = '$id1'")->rowCount();
    // EN CASO DE NO TENER ENTONCES
    if ($num < 1) {
      // VERIFICAMOS SI TIENE ALGUNA FACTURA/CONTRATO RELACIOANDO
      $num = $db->query("SELECT * FROM solicitudes WHERE solasesor like '$id1'")->rowCount();
      // EN CASO DE NO TENER
      if ($num < 1) {
        // VERIFICAMO SI TIENE ALGUNA CARTERA RELACIONADA
        $num = $db->query("SELECT * FROM carteras WHERE carpromotor = '$id1'")->rowCount();
        // EN CASO DE NO TENER
        if ($num < 1) {
          // ELIMINAMOS EL REGISTRO DEL TRABAJADOR
          $qry = $db->query("DELETE FROM usuarios WHERE usuid = '$id1'");
          if ($qry) $mensaje = 'Se elimino el trabjador';
          else $error = 'No se pudo eliminar el trabjador';
        } else $error = 'El trabjador tiene cartera asignada';
      } else $error = 'El trabjador tiene movimiento en historial de cartera';
    } else $error = 'El trabjador posee solicitudes';
    
    $parametro = $error ? "?error=$error" : "?mensaje=$mensaje";
    $parametro .= "&$filtro";
    header("Location:../mostrar_trabajador.php".$parametro);
    exit();
  }




?>