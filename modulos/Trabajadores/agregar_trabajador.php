
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script>
		$(document).ready(function() {

			$('#fechaingreso').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecharetiro').datepicker('option', 'minDate', selectedDate);
				}
			});
			$('#fecharetiro').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fechaingreso').datepicker('option', 'maxDate', selectedDate);
				}
			});
			$('#depto').change(function(event) {
				var id1 = $('#depto').find(':selected').val();
				$('#ciudad').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
				$('#sede').load('<?php echo $r ?>incluir/carga/sedes.php');
			});
			$('#ciudad').change(function(event) {
				var id1 = $('#depto').find(':selected').val();
				var id2 = $('#ciudad').find(':selected').val();
				$('#sede').load('<?php echo $r ?>incluir/carga/sedes.php?id1=' + id1 + '&id2=' + id2);
			});
			$('#asesor').change(function() {
				if ($('#asesor').val() == 1)
					$('#grupo').prop('disabled', false)
				else $('#grupo').prop('disabled', true)
			});
			
		});
	</script>
	<section id="principal">
		<article id="cuerpo">
			<article id="contenido">
				<form id="form" name="form" action="Controlador/trabajadorControlador.php" method="post">
					<fieldset class="col-md-12">
						<legend class="ui-widget ui-corner-all">Insertar Trabajador</legend>
						<div class='row'>
							<div class="col-md-6">
								<p>
									<label for="id"><spam style="color:red">*</spam>Código:</label>
									<input type="text" name="id" class="id validate[required, ajax[ajaxUserCall]] text-input" title="Digite la identificacion" autocomplete="false"/>
								</p>
								<p>
									<label for="password">Contraseña:</label>
									<input type="password" name="password" class="password validate[] text-input" title="Digite el password" />
								</p>
								<p>
									<label for="nombre"><spam style="color:red">*</spam>Nombre Completo:</label>
									<input type="text" style="width:400px" name="nombre" class="nombre validate[required] text-input" title="Digite el nombre del usuario" />
								</p>
								
								<p>
									<label for="perfil"><spam style="color:red">*</spam>Perfil:</label>
									<select id="perfil" name="perfil" class="validate[required] selectpicker" data-live-search="true" title="Seleccione el perfil" data-width="100%">
										<?php
										$qry = $db->query("SELECT * FROM perfiles ORDER BY pernombre");
										while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row['perid'] . '>' . $row['pernombre'] . '</option>';
										}
										?>
									</select>
								</p>
								<p>
									<label for="relacionista" style="width:auto!important;">Cobrador:</label>
									<select name="relacionista" id="relacionista">
										<option value="0">No</option>
										<option value="1">Sí</option>
									</select>
								</p>
								<p>
									<label for="asesor" style="width:auto!important;">Asesor de ventas:</label>
									<select name="asesor" id="asesor">
										<option value="0">No</option>
										<option value="1">Sí</option>
									</select>
									<label for="promotor">Asignar un grupo:</label>
									<select id="grupo" name="grupo" class="selectpicker validate[]" data-live-search="true" title="Seleccione un grupo" data-width="100%">
										<option value="">SELECCIONE</option>
										<?php
											$qry = $db->query("SELECT * FROM grupos ORDER BY grunombre ASC");
											while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
												echo '<option value=' . $row['grunombre'] . '>' . $row['grunombre'] . '</option>';
											}
										?>
									</select>
									<em style="font-size:13px">Debe seleccionar la opcion cobrador para asignar un grupo</em>
								</p>
							</div>
							<div class="col-md-6">
								<p>
									<label for="email">E-mail:</label>
									<input type="text" name="email" class="email validate[custom[email]] text-input" />
								</p>
								<p>
									<label for="telefono">Telefono:</label>
									<input type="text" name="telefono" class="telefono validate[] text-input" />
								</p>
								<p>
									<label for="direccion">Direccion:</label>
									<input type="text" name="direccion" class="direccion validate[] text-input" />
								</p>
								<p>
									<label for="depto">Departamentos:</label>
									<select id="depto" name="depto" class="validate[] text-input">
										<option value="">SELECCIONE</option>
										<?php
										$qry = $db->query("SELECT * FROM departamentos ORDER BY depnombre");
										while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row['depid'] . '>' . $row['depnombre'] . '</option>';
										}
										?>
									</select>
								</p>
								<p>
									<label for="ciudad">Ciudad:</label>
									<select id="ciudad" name="ciudad" class="validate[] text-input">
										<option value="">SELECCIONE</option>
										<?php
										$qry = $db->query("SELECT * FROM ciudades ORDER BY ciunombre");
										while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row['ciuid'] . '>' . $row['ciunombre'] . '</option>';
										}
										?>
									</select>
								</p>
								<?php
									$qryem = $db->query("SELECT * FROM empresas ORDER BY empnombre")->fetch(PDO::FETCH_OBJ);
									echo "<input type='hidden' name='empresa' value='$qryem->empid'>";
								?>

								<p>
									<label for="cargo">Cargo:</label> <!-- CAMPO CARGO-->
									<input type="text" name="cargo" class="text-input" />
								</p>
								
							</div>
						</div>
						
						
						
						
							<input type="hidden" name="sede" value=''>
							<input type="hidden" name="modalidad" value=''>

							<input type="hidden" name="fechaingreso" id="fechaingreso" class="text-input fecha" />
							<input type="hidden" name="fecharetiro" id="fecharetiro" class="text-input fecha" />
							<input type='hidden' name='salario' id='salario' class='valor validate[custom[onlyNumberSp], min[0], required] text-input' style="text-align:left!important; padding-left:2.5em;" value="0"/>
							<input type='hidden' name='auxilio' id='auxilio' class='valor validate[custom[onlyNumberSp], min[0], required] text-input' style="text-align:left!important; padding-left:2.5em;" value="0"/>
							<input type='hidden' name='topeauxilio' id='topeauxilio' class='valor validate[custom[onlyNumberSp], min[0], required] text-input' style="text-align:left!important; padding-left:2.5em;" value="0"/>

							<input type="hidden" name="director" value="1" style="width:auto!important;"/>
							<input type="hidden" name="directorcall" value="1" style="width:auto!important;" />
							<input type="hidden" id="promotor" name="promotor" value="1" style="width:auto!important;" />
							<input type="hidden" id="callcenter" name="callcenter" value="1" style="width:auto!important;" />
						
						
						<p class="boton">
							<button type="submit" class="btn btn-primary btninsertar btn-block" name="insertar" value="insertar">Insertar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
	</section>