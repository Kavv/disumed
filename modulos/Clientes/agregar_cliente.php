

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

<script>
    $(document).ready(function() {
        var die_depa_res = -1;
        var dir_ciudad_res = -1;

        var die_depa_co = -1;
        var dir_ciudad_co = -1;

        $('#depresidencia').change(function(event) {
            var id1 = $('#depresidencia').find(':selected').val();
            $("#ciuresidencia").load('ciudades-re.php?depa=' + id1 + '&die_depa_res=' + die_depa_res + '&dir_ciudad_res=' + dir_ciudad_res);
        });
        $('#depcomercio').change(function(event) {
            var id1 = $('#depcomercio').find(':selected').val();
            $("#ciucomercio").load('ciudades-co.php?depa=' + id1 + '&die_depa_co=' + die_depa_co + '&dir_ciudad_co=' + dir_ciudad_co);
        });
        $("#depresidencia").selectpicker();
        
    });
</script>


<section id="principal">
    <article id="cuerpo">
        <article id="contenido">
            <div class="ui-widget">
                <form id="form" name="form" action="Controlador/clienteControlador.php" method="post">
                    <!-- FORMULARIO ENVIADO POR POST A LISTAR.PHP JUNTO CON EL FILTRO GENERAL  -->
                    <div class="row">
                        <fieldset class="ui-widget ui-corner-all col-md-6">
                            <legend class="ui-widget ui-corner-all">Insertar cliente</legend>
                            <p> 
                                <label for="id"><span class="text-danger">*</span>Identificacion: </label> <!-- NUM IDENTIFICACION -->
                                <input type="text" name="id" class="id validate[required, ajax[ajaxUserCall]]" style="text-transform: uppercase"/>
                                
                            </p>
                            <p>
                                <label for="nom1"><span class="text-danger">*</span>Nombre: </label> <!-- CAMPO PRIMER NOMBRE  -->
                                <input type="text" name="nombre" style="text-transform: uppercase" required />
                            </p>
                            <p>
                                <label for="ape1">Email:</label> <!-- CAMPO EMAIL -->
                                <input type="text" class="email" name="email" />
                            </p>
                            <p>
                                <label><span class="text-danger">*</span>Celular: </label>
                                <input type="text" name="celular" class="telefono validate[required, custom[phone]] text-input" title="Digite numero celular" /> <!-- CAMPO NUM DE CELULAR -->
                            </p>

                        </fieldset>
                        <fieldset class="col-md-6">
                            <legend class="ui-widget ui-corner-all">Datos ubicacion residencia</legend> <!-- DATOS DE UBICACION 1 -->
                            <p>
                                <label><span class="text-danger">*</span>Departamento: </label> <!-- CAMPO DEPARTAMENTO -->
                                <select id="depresidencia" name="depresidencia" class="selectpicker" required 
                                data-live-search="true" title="Seleccione un departamento" data-width="100%">
                                    <option value="">SELECCIONE</option>
                                    <?php
                                        $qry = $db->query("SELECT * FROM departamentos ORDER BY depnombre");
                                        while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                            echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>'; // APARECEN TODOS LOS DEPARTAMENTOS PARA QUE EL USUARIO ELIGA EL PERTINENTE
                                        }
                                    ?>
                                </select>
                            </p>
                            <p>
                                <label><span class="text-danger">*</span>Ciudad: </label> <!-- CAMPO CIUDAD -->
                                <select name="ciuresidencia" id="ciuresidencia" class="validate[required] text-input">

                                </select>
                            </p>
                            <p>
                                <label><span class="text-danger">*</span>Direccion: </label>
                                <input type="text" name="direccion" style="text-transform: uppercase" class="direccion validate[required] text-input" /> <!-- CAMPO DIRECCION -->
                            </p>
                            <p>
                                <label>Telefono: </label>
                                <input type="text" name="telresidencia" class="telefono validate[custom[phone]] text-input" /> <!-- CAMPO TELEFONO -->
                            </p>
                        </fieldset>
                        <fieldset class="col-md-6">
                            <input type='hidden' id="depcomercio" name="depcomercio">
                            <input type='hidden' name="ciucomercio" id="ciucomercio" class="text-input">
                            <input type='hidden' type="text" name="barcomercio" class="barrio  text-input" /> <!-- CAMPO BARRIO -->
                            <input type='hidden' type="text" name="dircomercio" class="direccion  text-input" /> <!-- CAMPO DIRECCION -->
                            <input type='hidden' type="text" name="telcomercio" class="telefono validate[custom[phone]] text-input" /> <!-- CAMPO TELEFONO -->
                        </fieldset>
                    </div>
                    
                    <p class="boton">
                        <button type="submit" class="btn btn-block btn-primary btninsertar" name="insertar" value="insertar">Insertar</button> <!-- BOTON MODIFICAR -->
                    </p>
                </form>
            </div>
        </article>
    </article>
    <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>



    