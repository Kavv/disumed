<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');



// VALORES DE BUSQUEDA
if (isset($_GET['id'])) {
	$id = $_GET['id'];
	$nombre = strtoupper($_GET['nombre']);
	$departamento = strtoupper($_GET['departamento']);
	$ciudad = strtoupper($_GET['ciudad']);
	$direccion = strtoupper($_GET['direccion']);
	$tel = $_GET['telefono'];
    $filtro = 'id=' . $id . '&nombre=' . $nombre. '&departamento=' . $departamento. '&ciudad=' . $ciudad . '&direccion=' . $direccion. '&telefono=' . $tel;
}


// AGREGAMOS UN NUEVO CLIENTE
if (isset($_POST['insertar'])) { //VALIDAMOS SI POST MODIFICAR ES TRUE
    // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA Y REMOVEMOS ESPACIOS VACIOS
    $id = strtoupper(trim($_POST['id']));
    $nombre = strtoupper(trim($_POST['nombre']));
    $direccion = trim(strtoupper($_POST['direccion']));
    $email = trim(strtoupper($_POST['email']));
    $celular = trim($_POST['celular']);
    $departamento = trim($_POST['depresidencia']);
    $ciudad = trim($_POST['ciuresidencia']);
    $telefono = trim($_POST['telresidencia']);

    $qry = $db->query("INSERT INTO clientes (
        cliid, clinombre, cliemail, clicelular, 
        clidepresidencia, cliciuresidencia, clidirresidencia, clitelresidencia
    ) VALUES ( 
        '$id', '$nombre', '$email', '$celular', 
        '$departamento', '$ciudad', '$direccion', '$telefono'
    )");

    if ($qry) {
        $mensaje = 'Cliente guardado correctamente'; //MENSAJE EXITOSO
        header("Location:../index.php?mensaje=$mensaje");
    } else {
        $error = 'Ocurrio un error!'; //MENSAJE ERROR
        header("Location:../index.php?error='$error'");
    }

    exit();
}

// MODIFICAR REGISTRO
if (isset($_POST['modificar'])) { //VALIDAMOS SI POST MODIFICAR ES TRUE
	$id = $_POST['id'];
	$nombre = strtoupper(trim($_POST['nom1']));
	$email = strtolower(trim($_POST['email']));
	$celular = trim($_POST['celular']);
	$departamento = trim($_POST['depresidencia']);
	$ciudad = trim($_POST['ciuresidencia']);
	$direccion = trim(strtoupper($_POST['dirresidencia']));
	$telefono = trim($_POST['telresidencia']);

    // CONSULTA SQL PARA ACTUALIZAR EL REGISTRO
	$qry = $db->query("UPDATE clientes SET clinombre = '$nombre', cliemail = '$email', clicelular = '$celular', 
    clidepresidencia = '$departamento', cliciuresidencia = '$ciudad' , clidirresidencia = '$direccion' , clitelresidencia = '$telefono' 
    WHERE cliid = '$id'"); 

	if ($qry) $mensaje = 'Se actualizo el cliente'; //MENSAJE EXITOSO
	else $error = 'No se actualizo el cliente'; //MENSAJE ERROR

    $parametro = isset($error) ? "?error=$error" : "?mensaje=$mensaje";
    // CONCATENAMOS LOS PARAMETROS DEL FILTRO DE BUSQUEDA
    $parametro .= "&$filtro";
    // REDIRACCION
    header("Location:../listar.php".$parametro);
    exit();
}

// ELIMINAR REGISTRO
if (isset($_GET['id1'])) {
    // id1 = IDENTIFICADOR DEL REGISTRO A ELIMINAR
	$id1 = $_GET['id1'];
	$num = $db->query("SELECT * FROM solicitudes WHERE solcliente = '$id1'")->rowCount();  // REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
	if ($num < 1) { // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		$qry = $db->query("DELETE FROM clientes WHERE cliid = '$id1'"); //ELIMINAMOS LA CAUSAL
		if ($qry) $mensaje = 'Se elimino el cliente';
		else $error = 'No se pudo eliminar el cliente';
	} else $error = 'El cliente tiene solictudes asociadas';

    $parametro = isset($error) ? "?error=$error" : "?mensaje=$mensaje";
    $parametro .= "&$filtro";
    header("Location:../listar.php".$parametro);
    exit();
}


?>