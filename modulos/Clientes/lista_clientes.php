<?php
$id = $nombre = 0;
if (isset($_POST['consultar'])) {
	$id = strupperEsp($_POST['id']);
	$nombre = strtoupper($_POST['nombre']);
	$departamento = $_POST['departamento'];
	$ciudad = $_POST['ciudad'] ?? '';
	$direccion = strtoupper($_POST['direccion']);
	$tel = $_POST['telefono'];
} else if (isset($_GET['id'])) {
	$id = $_GET['id'];
	$nombre = strtoupper($_GET['nombre']);
	$departamento = $_GET['departamento'];
	$ciudad = $_GET['ciudad'];
	$direccion = strtoupper($_GET['direccion']);
	$tel = $_GET['telefono'];
}

$con = "SELECT  cliid,  clidigito,  clinombre,  clinom2,  cliape1, cliape2,  depnombre,  ciunombre,  clibarresidencia,  clidirresidencia,  clitelresidencia,  clitelcomercio,  clicelular,  cliemail FROM (clientes LEFT JOIN departamentos ON clidepresidencia = depid) LEFT JOIN ciudades ON (clidepresidencia = ciudepto AND cliciuresidencia = ciuid)";

/* Los parametros de la consulta sql se genera dinamicamente 
en base a los datos recibidos para delimitar los resultados */
$parameters = [];
if($id != "")
	array_push($parameters, "cliid LIKE '%$id%'");
if($nombre != "")
	array_push($parameters, "clinombre LIKE '%$nombre%'" );
if($departamento != "")
	array_push($parameters, "clidepresidencia = '$departamento'" );
if($ciudad != "")
	array_push($parameters, "cliciuresidencia = '$ciudad'" );
if($direccion != "")
	array_push($parameters, "clidirresidencia LIKE '%$direccion%'" );
if($tel != "")
	array_push($parameters, "clicelular LIKE '%$tel%' OR clitelresidencia LIKE '%$tel%' OR clitelcomercio LIKE '%$tel%' OR clitelfamiliar LIKE '%$tel%' OR cliteladicional LIKE '%$tel%'" );


// Consulta base
$sql = $con;
foreach ($parameters as $index => $parameter) {
	// Se agregan los parametros del WHERE
	if($index == 0)
		$sql .= " WHERE " . $parameter;
	else
		$sql .= " AND " . $parameter;
}


$filtro = 'id=' . $id . '&nombre=' . $nombre. '&departamento=' . $departamento. '&ciudad=' . $ciudad . '&direccion=' . $direccion. '&telefono=' . $tel;

$qry = $db->query($sql);
$cantidad_clientes = $qry->rowCount();
?>

	<script>
		
		$(document).ready(function() {
			
			dt = createdt($("#tabla"),{col:1});

			$('.confirmar').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el cliente?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});
            
		});
	</script>
    

	<section id="principal">


		<article id="cuerpo">
			<article id="contenido">
				<h2>Listado de clientes</h2>
				<div class="reporte">

				</div>
				<!-- INICIO DE TABLA -->
				<div id="msj"></div>
				<table id="tabla" class="table table-hover" style='width:100%;'>
					<thead>
						<tr>
							<th>Identificacion</th>
							<th>Nombre</th>
							<th>Departamento</th>
							<th>Municipio</th>
							<th>Direccion</th>
							<th class="text-center">Telefonos</th>
							<th>Email</th>
							<?php if ($rowlog['peradmcli'] == '1') { ?>
								<th></th>
								<th></th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {  //INFORMACION DE LA CONSULTA
						?>
							<tr>
								<td><?php echo $row['cliid'] . ' ' . $row['clidigito'] ?></td>
								<td><?php echo $row['clinombre']?></td>

								<td><?php echo $row['depnombre'] ?></td>
								<td><?php echo $row['ciunombre'] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/casa.png" title="<?php echo $row['clidirresidencia'] ?>" /></td>
								<td align="center"><?php echo $row['clicelular'] . ' / ' . $row['clitelresidencia']?></td>
								<td align="center"><a href="mailto:<?php echo $row['cliemail'] ?>"><img src="<?php echo $r ?>imagenes/iconos/email.png" title="<?php echo $row['cliemail'] ?>" /></a></td>
								
								<?php if ($rowlog['peradmcli'] == '1') { ?>
								<td align="center"><a href="modificar.php?<?php echo 'id1=' . $row['cliid'] . '&' . $filtro ?>" onClick="" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td><!-- ENVIA POR GET LAS VARIABLES NECESARIAS PARA MODIFICAR UN CLIENTE -->
								<td align="center"><a href="Controlador/clienteControlador.php?<?php echo 'id1=' . $row['cliid'] . '&' . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td> <!-- ENVIA LAS VARIABLES NECESARIAS PARA ELIMINAR UN CLIENTE JUNTO CON VENTANA MODAL PARA CONFIRMAR -->
								<?php } ?>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table> <!-- FIN DE LA TABLA -->
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="location.href='index.php'">Atras</button> <!-- BOTON PARA VOLVER A CONSULTAR -->
				</p>
			</article>
		</article>
	</section>
	<script src="<?php echo $r.'incluir/kavv_js/makemsj.js'?>" ></script>

	<?php
	if (isset($error)) {?>
		<script>make_alert({'type': 'danger', 'message': '<?php echo $error?>'});</script>
	<?php }
	elseif (isset($mensaje)) 
	{ ?>
		<script>make_alert({'message': '<?php echo $mensaje?>'});</script>
	<?php } ?>