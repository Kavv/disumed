
	<script>
			$(document).ready(function() {
					var die_depa_res = -1;
					var dir_ciudad_res = -1;

					var die_depa_co = -1;
					var dir_ciudad_co = -1;

					$('#departamento').change(function(event) {
							var id1 = $('#departamento').find(':selected').val();
							$("#ciudad").load('ciudades-re.php?depa=' + id1 + '&die_depa_res=' + die_depa_res + '&dir_ciudad_res=' + dir_ciudad_res);
					});
					$("#departamento").selectpicker();
					
			});
	</script>

	<section id="principal">
		<article id="cuerpo">
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar.php" method="post">
						<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
						<fieldset class="col-md-6">
							<legend class="ui-widget ui-corner-all">Consultar clientes</legend>
							<p>
								<label for="id">Identificacion:</label>
								<input type="text" name="id" class="id text-input" title="Digite la identificacion del cliente" /> <!-- CONSULTAR CLIENTE POR NUMERO DE IDENTIFICACION -->
							</p>
							<p>
								<label for="nombre">Nombre:</label>
								<input type="text" name="nombre" class="nombre text-input" title="Digite el nombre del cliente" /> 
							</p>
							
							<p>
								<label>Departamento: </label> <!-- CAMPO DEPARTAMENTO -->
								<select id="departamento" name="departamento" class="selectpicker" data-live-search="true" title="Seleccione un departamento" data-width="100%">
									<option value="">SELECCIONE</option>
									<?php
										$qry = $db->query("SELECT * FROM departamentos ORDER BY depnombre");
										while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
											// APARECEN TODOS LOS DEPARTAMENTOS PARA QUE EL USUARIO ELIGA EL PERTINENTE
											echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>'; 
										}
									?>
								</select>
							</p>
							<p>
								<label>Ciudad: </label> <!-- CAMPO CIUDAD -->
								<select name="ciudad" id="ciudad" class="text-input">
								</select>
							</p>

							<p>
								<label for="nombre">Dirección:</label>
								<input type="text" name="direccion" class="nombre text-input" title="Digite la direccion del cliente" /> 
							</p>
							<p>
								<label for="nombre">Teléfono:</label>
								<input type="text" name="telefono" class="nombre text-input" title="Digite el numero de teléfono" /> 
							</p>
							<div class="row  d-flex justify-content-center">
								<div class="col-md-12 col-lg-12">
									<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
