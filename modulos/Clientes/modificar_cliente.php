<?php

$row = $db->query("SELECT clientes.*, residencia.ciunombre as ciuresidencia, comercio.ciunombre as ciucomercio FROM clientes
LEFT JOIN ciudades AS residencia ON (residencia.ciudepto = clidepresidencia AND residencia.ciuid = cliciuresidencia)
LEFT JOIN ciudades AS comercio ON (comercio.ciudepto = clidepcomercio AND comercio.ciuid = cliciucomercio)
WHERE cliid = '" . $_GET['id1'] . "'")->fetch(PDO::FETCH_ASSOC); // CONSULTA A LA BD CON VARUABLES RECIBIDAS POR GET

$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre'] . '&departamento=' . $_GET['departamento'] . '&ciudad=' . $_GET['ciudad'] . '&direccion=' . $_GET['direccion'] . '&telefono=' . $_GET['telefono'];
?>

<script>
	var $ciudad = <?php if ($row['cliciucomercio'] != "") {
						echo $row['cliciuresidencia'];
					} else {
						echo "0";
					} ?>;
	$(document).ready(function() {
		var die_depa_res = <?php if ($row['clidepresidencia'] != "") echo $row['clidepresidencia'];
							else echo '-1'; ?>;
		var dir_ciudad_res = <?php if ($row['cliciuresidencia'] != "") echo $row['cliciuresidencia'];
								else echo '-1'; ?>;

		var die_depa_co = <?php if ($row['clidepcomercio'] != "") echo $row['clidepcomercio'];
							else echo '-1'; ?>;
		var dir_ciudad_co = <?php if ($row['cliciucomercio'] != "") echo $row['cliciucomercio'];
							else echo '-1'; ?>;

		$('#depresidencia').change(function(event) {
			var id1 = $('#depresidencia').find(':selected').val();
			$("#ciuresidencia").load('ciudades-re.php?depa=' + id1 + '&die_depa_res=' + die_depa_res + '&dir_ciudad_res=' + dir_ciudad_res);
		});
		$('#depcomercio').change(function(event) {
			var id1 = $('#depcomercio').find(':selected').val();
			$("#ciucomercio").load('ciudades-co.php?depa=' + id1 + '&die_depa_co=' + die_depa_co + '&dir_ciudad_co=' + dir_ciudad_co);
		});
	});
</script>
<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
<style type="text/css">
</style>
</head>

<body>
	<section id="principal">
		<article id="cuerpo">
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="Controlador/clienteControlador.php?<?php echo $filtro ?>" method="post">
						<!-- FORMULARIO ENVIADO POR POST A LISTAR.PHP JUNTO CON EL FILTRO GENERAL  -->
						<div class="row">
							<fieldset class="col-md-6">
								<legend class="ui-widget ui-corner-all">Modificar cliente</legend>
								<p>
									<label for="id"><span class="text-danger">*</span>Identificacion:</label> <!-- NUM IDENTIFICACION -->
									<input type="text" name="id" class="id" value="<?php echo $row['cliid'] ?>" readonly />
								</p>
								<p>
									<label for="nom1"><span class="text-danger">*</span>Nombre:</label> <!-- CAMPO PRIMER NOMBRE  -->
									<input type="text" name="nom1" style="text-transform: uppercase" value="<?php echo $row['clinombre'] ?>" required />
								</p>
							<p>
								<label for="ape1">Email:</label> <!-- CAMPO EMAIL -->
								<input type="text" class="email" name="email" value="<?php echo $row['cliemail'] ?>" />
							</p>
							<p>
								<label>Celular: </label><input type="text" name="celular" class="telefono validate[custom[phone]] text-input" value="<?php echo $row['clicelular'] ?>" title="Digite numero celular" /> <!-- CAMPO NUM DE CELULAR -->
							</p>

							</fieldset>

							<fieldset class="col-md-6">
								<legend class="ui-widget ui-corner-all">Datos ubicacion residencia</legend> <!-- DATOS DE UBICACION 1 -->
								<p>
									<label><span class="text-danger">*</span>Departamento:</label> <!-- CAMPO DEPARTAMENTO -->
									<select id="depresidencia" name="depresidencia" class=" text-input">
										<?php
										if ($row['clidepresidencia'] != '') { // SI EL DEPARTAMENTO DEL CLIENTE ES DISTINTO A VACIO 
											$row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['clidepresidencia'] . "'")->fetch(PDO::FETCH_ASSOC); // SE BUSCA ESPECIFICAMENTE EL DEPARTAMENTO 
											echo '<option value=' . $row['clidepresidencia'] . '>' . $row2['depnombre'] . '</option>'; // SE MUESTAR LOS RESULTADOS TANTO NOMBRE DEL DEPARTAMENTO COMO SU ID EN UN SELECT 
										} // SI EL DEPARTAMENTO DEL CLIENTE ESTA VACIO
										echo '<option value="">SELECCIONE</option>';
										$qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['clidepresidencia'] . "' ORDER BY depnombre");
										while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>'; // APARECEN TODOS LOS DEPARTAMENTOS PARA QUE EL USUARIO ELIGA EL PERTINENTE
										}
										?>
									</select>
								</p>
								<p>
									<label><span class="text-danger">*</span>Ciudad: </label> <!-- CAMPO CIUDAD -->
									<select name="ciuresidencia" id="ciuresidencia" class=" text-input">
										<?php
										if ($row['cliciuresidencia'] != '') {
											echo '<option value=' . $row['cliciuresidencia'] . '>' . $row['ciuresidencia'] . '</option>';
										}
										?>
									</select>
								</p>
								<label><span class="text-danger">*</span>Direccion: </label><input style="text-transform: uppercase" type="text" name="dirresidencia" class="direccion  text-input" value="<?php echo $row['clidirresidencia'] ?>" /> <!-- CAMPO DIRECCION -->
								</p>
								<p>
									<label>Telefono: </label><input type="text" name="telresidencia" class="telefono validate[custom[phone]] text-input" value="<?php echo $row['clitelresidencia'] ?>" /> <!-- CAMPO TELEFONO -->
								</p>
							</fieldset>
						</div>

						<p class="boton">
							<button type="button" class="btn btn-primary btnatras" onClick="location.href='listar.php?<?php echo $filtro ?>'">Atras</button> <!-- BOTON ATRAS -->
							<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button> <!-- BOTON MODIFICAR -->
						</p>
					</form>
				</div>
			</article>
		</article>
	</section>
</body>

</html>