<?php

if (isset($_POST['insertar'])) { // VALIDAMOS QUE POST INSERTAR SEA TRUE
	$nombre = trim(strtoupper($_POST['nombre']));
	$_POST['admin'] = $_POST['callcenter'] = $_POST['solicitud'] 
	= $_POST['factura'] = $_POST['bodega'] = $_POST['cartera'] 
	= $_POST['comision'] = $_POST['estadistico'] 
	= $_POST['contrato'] = $_POST['recibo'] = 0;
	
	/* Admin */
	if(isset($_POST['admpar']) || isset($_POST['admemp']) || isset($_POST['admege']) || isset($_POST['admper']) 
	|| isset($_POST['admusu']) || isset($_POST['admcli']) || isset($_POST['admprv']) || isset($_POST['admcau']) 
	|| isset($_POST['admpro']) || isset($_POST['admfpa']) || isset($_POST['admmde']) || isset($_POST['admman'])
	|| isset($_POST['admaut']) || isset($_POST['admgesdoc']))
		$_POST['admin'] = 1;

	if (!isset($_POST['admpar']))
		$_POST['admpar'] = 0;
	if (!isset($_POST['admemp']))
		$_POST['admemp'] = 0;
	if (!isset($_POST['admege']))
		$_POST['admege'] = 0;
	if (!isset($_POST['admper']))
		$_POST['admper'] = 0;
	if (!isset($_POST['admusu']))
		$_POST['admusu'] = 0;
	if (!isset($_POST['admcli']))
		$_POST['admcli'] = 0;
	if (!isset($_POST['admprv']))
		$_POST['admprv'] = 0;
	if (!isset($_POST['admcau']))
		$_POST['admcau'] = 0;
	if (!isset($_POST['admpro']))
		$_POST['admpro'] = 0;
	if (!isset($_POST['admfpa']))
		$_POST['admfpa'] = 0;
	if (!isset($_POST['admmde']))
		$_POST['admmde'] = 0;
	if (!isset($_POST['admman']))
		$_POST['admman'] = 0;
	if (!isset($_POST['admaut']))
		$_POST['admaut'] = 0;
	if (!isset($_POST['admgesdoc']))
		$_POST['admgesdoc'] = 0;

	
	/* Call Center */
	if(isset($_POST['callLlamar']) || isset($_POST['CallCampana']) || isset($_POST['CallGeneral']) || isset($_POST['CallCapaci']) 
	|| isset($_POST['CallSemaforo']) || isset($_POST['CallRepo']) || isset($_POST['CallViaInser']) || isset($_POST['Callviavalor']) 
	|| isset($_POST['CallViaCons']))
		$_POST['callcenter'] = 1;

	if (!isset($_POST['callLlamar']))
		$_POST['callLlamar'] = 0;
	if (!isset($_POST['CallCampana']))
		$_POST['CallCampana'] = 0;
	if (!isset($_POST['CallGeneral']))
		$_POST['CallGeneral'] = 0;
	if (!isset($_POST['CallCapaci']))
		$_POST['CallCapaci'] = 0;
	if (!isset($_POST['CallSemaforo']))
		$_POST['CallSemaforo'] = 0;
	if (!isset($_POST['CallRepo']))
		$_POST['CallRepo'] = 0;
	if (!isset($_POST['CallViaInser']))
		$_POST['CallViaInser'] = 0;
	if (!isset($_POST['Callviavalor']))
		$_POST['Callviavalor'] = 0;
	if (!isset($_POST['CallViaCons']))
		$_POST['CallViaCons'] = 0;


	/* Solicitudes */
	if(isset($_POST['solnor']) || isset($_POST['sollib']) || isset($_POST['solest']) || isset($_POST['Solmod'])
	|| isset($_POST['Solrea']) || isset($_POST['Solcan']) || isset($_POST['solanu']) || isset($_POST['Soleli'])
	|| isset($_POST['solcon']) || isset($_POST['solscr']) || isset($_POST['solche']) || isset($_POST['Solsopor'])
	|| isset($_POST['Solrepor']) || isset($_POST['Solprop']) )
		$_POST['solicitud'] = 1;
	if (!isset($_POST['solnor']))
		$_POST['solnor'] = 0;
	if (!isset($_POST['sollib']))
		$_POST['sollib'] = 0;
	if (!isset($_POST['solest']))
		$_POST['solest'] = 0;
	if (!isset($_POST['Solmod']))
		$_POST['Solmod'] = 0;
	if (!isset($_POST['Solrea']))
		$_POST['Solrea'] = 0;
	if (!isset($_POST['Solcan']))
		$_POST['Solcan'] = 0;
	if (!isset($_POST['solanu']))
		$_POST['solanu'] = 0;
	if (!isset($_POST['Soleli']))
		$_POST['Soleli'] = 0;
	if (!isset($_POST['solcon']))
		$_POST['solcon'] = 0;
	if (!isset($_POST['solscr']))
		$_POST['solscr'] = 0;
	if (!isset($_POST['solche']))
		$_POST['solche'] = 0;
	if (!isset($_POST['Solsopor']))
		$_POST['Solsopor'] = 0;
	if (!isset($_POST['Solrepor']))
		$_POST['Solrepor'] = 0;
	if (!isset($_POST['Solprop']))
		$_POST['Solprop'] = 0;

	
	/* Facturacion */
	if(isset($_POST['faccon']) || isset($_POST['facfac']) || isset($_POST['facrep']) || isset($_POST['Facrepcall']) )
		$_POST['factura'] = 1;
	if (!isset($_POST['facfac']))
		$_POST['facfac'] = 0;
	if (!isset($_POST['faccon']))
		$_POST['faccon'] = 0;
	if (!isset($_POST['facrep']))
		$_POST['facrep'] = 0;
	if (!isset($_POST['Facrepcall']))
		$_POST['Facrepcall'] = 0;


	/* Bodega */
	if(isset($_POST['bodkar']) || isset($_POST['bodpla']) || isset($_POST['boddoc']) 
	|| isset($_POST['bodrep']) || isset($_POST['boddes']) || isset($_POST['bofactura']) )
		$_POST['bodega'] = 1;
	if (!isset($_POST['bodkar']))
		$_POST['bodkar'] = 0;
	if (!isset($_POST['bodpla']))
		$_POST['bodpla'] = 0;
	if (!isset($_POST['boddoc']))
		$_POST['boddoc'] = 0;
	if (!isset($_POST['bodrep']))
		$_POST['bodrep'] = 0;	
	if (!isset($_POST['boddes']))
		$_POST['boddes'] = 0;
	if (!isset($_POST['bofactura']))
		$_POST['bofactura'] = 0;

	/* Tesoreria */
	if (!isset($_POST['tesoreria']))
		$_POST['tesoreria'] = 0;

	/* Cartera */
	if(isset($_POST['carpag']) || isset($_POST['carrep']) || isset($_POST['carnot']) || isset($_POST['carmas']) 
	|| isset($_POST['carpaz']) || isset($_POST['carcon']) || isset($_POST['carfec']) || isset($_POST['carpro']) 
	|| isset($_POST['carcas']) || isset($_POST['cardat']))
		$_POST['cartera'] = 1;

	if (!isset($_POST['carpag']))
		$_POST['carpag'] = 0;
	if (!isset($_POST['carrep']))
		$_POST['carrep'] = 0;
	if (!isset($_POST['carnot']))
		$_POST['carnot'] = 0;
	if (!isset($_POST['carmas']))
		$_POST['carmas'] = 0;
	if (!isset($_POST['carpaz']))
		$_POST['carpaz'] = 0;
	if (!isset($_POST['carcon']))
		$_POST['carcon'] = 0;
	if (!isset($_POST['carfec']))
		$_POST['carfec'] = 0;
	if (!isset($_POST['carpro']))
		$_POST['carpro'] = 0;
	if (!isset($_POST['carcas']))
		$_POST['carcas'] = 0;
	if (!isset($_POST['cardat']))
		$_POST['cardat'] = 0;
	
	/* Caja */
	if (!isset($_POST['caja']))
		$_POST['caja'] = 0;

	/* Comision */
	if(isset($_POST['Cominormal']) || isset($_POST['Comilibranza']) || isset($_POST['Comiestimulacion']) || isset($_POST['Comiprop']) )
		$_POST['comision'] = 1;
	if (!isset($_POST['Cominormal']))
		$_POST['Cominormal'] = 0;
	if (!isset($_POST['Comilibranza']))
		$_POST['Comilibranza'] = 0;
	if (!isset($_POST['Comiestimulacion']))
		$_POST['Comiestimulacion'] = 0;
	if (!isset($_POST['Comiprop']))
		$_POST['Comiprop'] = 0;


	/* Estadistico */

	if(isset($_POST['EstaVent']) || isset($_POST['EstaRepor']))
		$_POST['estadistico'] = 1;

	if (!isset($_POST['EstaVent']))
		$_POST['EstaVent'] = 0;
	if (!isset($_POST['EstaRepor']))
		$_POST['EstaRepor'] = 0;



	/* Contrato */
	if(isset($_POST['contrato_agregar']) || isset($_POST['contrato_editar']) || isset($_POST['contrato-ver']))
		$_POST['contrato'] = 1;

	if (!isset($_POST['contrato_agregar']))
		$_POST['contrato_agregar'] = 0;
	if (!isset($_POST['contrato_editar']))
		$_POST['contrato_editar'] = 0;

		
	/* Recibos */
	if(isset($_POST['r_cobrarse_add']) || isset($_POST['r_cobrarse_edit']) 
	|| isset($_POST['r_pagados_add']) || isset($_POST['r_pagados_edit']) 
	|| isset($_POST['recibo-ver']) || isset($_POST['r_imprimir']) )
		$_POST['recibo'] = 1;

	if (!isset($_POST['r_cobrarse_add']))
		$_POST['r_cobrarse_add'] = 0;
	if (!isset($_POST['r_cobrarse_edit']))
		$_POST['r_cobrarse_edit'] = 0;
	if (!isset($_POST['r_pagados_add']))
		$_POST['r_pagados_add'] = 0;
	if (!isset($_POST['r_pagados_edit']))
		$_POST['r_pagados_edit'] = 0;
	if (!isset($_POST['r_imprimir']))
		$_POST['r_imprimir'] = 0;


	$qry = $db->query("INSERT INTO perfiles ( pernombre,
		peradmpar, peradmemp, peradmege, peradmper, peradmusu, peradmcli, peradmprv, peradmcau, peradmpro, peradmfpa, peradmmde, peradmman, peradmaut, peradmgesdoc, peradmin,
		percallllamar, percallcampana, percallgeneral, percallcapaci, percallsemaforo, percallrepo, percallviainser, percallviavalor, percallviacons, percallcenter,
		persolnor, persollib, persolest, persolmod, persolrea, persolcan, persolanu, persoleli, persolcon, persolscr, persolche, persolsopor, persolrepor, persolprop, persolicitud,
		perfacfac, perfaccon, perfacrep, perfacrepcall, perfactura,
		perbodkar, perbodpla, perboddoc, perbodrep, perboddes, perbodega, percontratofac, 
		pertesoreria,
		percarpag, percarrep, percarnot, percarmas, percarpaz, percarcon, percarfec, percarpro, percarcas, percardat, percartera,
		percaja, 
		percominormal, percomilibranza, percomiestimulacion, percomiprop, percomision,
		perestavent, perestarepor, perestadistico,
		percontrato, percontratoadd, percontratoedit,
		perrecibo, percobrarseadd, percobrarseedit, perpagadoadd, perpagadoedit, perimprimirrecibos
		) 
	VALUES ('$nombre', 
	'" . $_POST['admpar'] . "', '" . $_POST['admemp'] . "', '" . $_POST['admege'] . "', '" . $_POST['admper'] . "', 
	'" . $_POST['admusu'] . "', '" . $_POST['admcli'] . "', '" . $_POST['admprv'] . "', '" . $_POST['admcau'] . "', 
	'" . $_POST['admpro'] . "', '" . $_POST['admfpa'] . "', '" . $_POST['admmde'] . "', '" . $_POST['admman'] . "', 
	'" . $_POST['admaut'] . "', '" . $_POST['admgesdoc'] . "', '" . $_POST['admin'] . "', 

	'" . $_POST['callLlamar'] . "', '" . $_POST['CallCampana'] . "', '" . $_POST['CallGeneral'] . "', '" . $_POST['CallCapaci'] . "', 
	'" . $_POST['CallSemaforo'] . "', '" . $_POST['CallRepo'] . "', '" . $_POST['CallViaInser'] . "', '" . $_POST['Callviavalor'] . "', 
	'" . $_POST['CallViaCons'] . "', '" . $_POST['callcenter'] . "', 
	
	'" . $_POST['solnor'] . "', '" . $_POST['sollib'] . "', '" . $_POST['solest'] . "', '" . $_POST['Solmod'] . "', 
	'" . $_POST['Solrea'] . "', '" . $_POST['Solcan'] . "', '" . $_POST['solanu'] . "', '" . $_POST['Soleli'] . "', 
	'" . $_POST['solcon'] . "', '" . $_POST['solscr'] . "', '" . $_POST['solche'] . "', '" . $_POST['Solsopor'] . "', 
	'" . $_POST['Solrepor'] . "', '" . $_POST['Solprop'] . "', '" . $_POST['solicitud'] . "',  

	'" . $_POST['facfac'] . "', '" . $_POST['faccon'] . "', '" . $_POST['facrep'] . "', '" . $_POST['Facrepcall'] . "', 
	'" . $_POST['factura'] . "', 

	'" . $_POST['bodkar'] . "', '" . $_POST['bodpla'] . "', '" . $_POST['boddoc'] . "', '" . $_POST['bodrep'] . "', 
	'" . $_POST['boddes'] . "', '" . $_POST['bodega'] . "', '" . $_POST['bofactura'] . "',   
	
	'" . $_POST['tesoreria'] . "',

	'" . $_POST['carpag'] . "', '" . $_POST['carrep'] . "', '" . $_POST['carnot'] . "', 
	'" . $_POST['carmas'] . "', '" . $_POST['carpaz'] . "', '" . $_POST['carcon'] . "', '" . $_POST['carfec'] . "', 
	'" . $_POST['carpro'] . "', '" . $_POST['carcas'] . "', '" . $_POST['cardat'] . "', '" . $_POST['cartera'] . "', 

	'" . $_POST['caja'] . "',
	
	'" . $_POST['Cominormal'] . "', '" . $_POST['Comilibranza'] . "', '" . $_POST['Comiestimulacion'] . "', '" . $_POST['Comiprop'] . "', 
	'" . $_POST['comision'] . "', 
	
	'" . $_POST['EstaVent'] . "', '" . $_POST['EstaRepor'] . "', '" . $_POST['estadistico'] . "', 
	
	
	'" . $_POST['contrato'] . "', '" . $_POST['contrato_agregar'] . "', '" . $_POST['contrato_editar'] . "', 

	'" . $_POST['recibo'] . "', '" . $_POST['r_cobrarse_add'] . "', '" . $_POST['r_cobrarse_edit'] . "', '" . $_POST['r_pagados_add'] . "', '" . $_POST['r_pagados_edit'] . "', '" . $_POST['r_imprimir'] . "')"); 
	
	$consultas = $informes = [];
	$perfil_id = $db->lastInsertId();
	if(isset($_POST['consultas']))
	{
		$consultas = $_POST['consultas'];
		$cant = count($consultas);
		for($i = 0; $i < $cant; $i++)
		{
			$reporte = $consultas[$i];
			$db->query("INSERT INTO perfil_reporte (perfil, reporte, tipo) VALUES ($perfil_id, $reporte, 1)");
		}
	}
	if(isset($_POST['informes']))
	{
		$informes = $_POST['informes'];
		$cant = count($informes);
		for($i = 0; $i < $cant; $i++)
		{
			$reporte = $informes[$i];
			$db->query("INSERT INTO perfil_reporte (perfil, reporte, tipo) VALUES ($perfil_id, $reporte, 2)");
		}
	}

	if ($qry) $mensaje = 'Se inserto el rol'; // MENSAJE MODAL EXITOSO
	else $error = "No se inserto el rol";  // MENSAJE MODAL ERROR
}
?>

	<script>
		$(document).ready(function() {
			$('#acordeon').accordion({
				heightStyle: "content"
			});
			$('input:checkbox').click(function() {
				var element = $(this).attr('id');
				if ($(this).is(":checked")) {
					$('.' + element).removeAttr('disabled');
				} else {
					$('.' + element).attr('disabled', 'disabled');
				}
			});
		});
	</script>
	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style type="text/css">
		.permission-list{
			background: #e6e6e6;
			border-radius: 15px;
			padding-top: 10px;
			padding-bottom: 10px;
		}
		input[type="checkbox"]{
			width: 25px!important;
		}
		.btn-k-i{
			background: #63abfc;
		}
		.btn-k-i:hover{
			background: #3b97fe;
		}
		.btn-k-w{
			background: #ffd690;
		}
		.btn-k-w:hover{
			background: #f2c16d;
		}
	</style>
	
	<section id="principal">
		<article id="cuerpo">
			<article id="contenido">
				<form id="form" name="form" action="index.php" method="post">
					<p>
						<label for="nombre" class="tabx">Nombre del perfil:</label> <!-- CAMPO NOMBRE -->
						<div class="row">
							<div class="col-md-6">
								<input type="text" name="nombre" style="text-transform: uppercase" class="nombre validate[required, ajax[ajaxNameCall]] text-input" title="Digite el nombre del perfil" />
							</div>
							<div class="col-md-6">
								<button type="submit" class="btn btn-block btn-success btninsertar" name="insertar" value="insertar">GUARDAR</button> <!-- BOTON INSERTAR -->
							</div>
						</div>
					</p>
					<div class="row">
						<div class="col-md-6">
							<div class="list-group" id="list-tab" role="tablist">
								<a class="list-group-item list-group-item-action active" id="list-Gerencia-list" data-toggle="list" href="#list-Gerencia" role="tab" aria-controls="Gerencia">Gerencia</a>
								<a class="list-group-item list-group-item-action" id="list-Ventas-list" data-toggle="list" href="#list-Ventas" role="tab" aria-controls="Ventas">Ventas</a>
								<a class="list-group-item list-group-item-action" id="list-Cobranza-list" data-toggle="list" href="#list-Cobranza" role="tab" aria-controls="Cobranza">Cobranza</a>
								<a class="list-group-item list-group-item-action" id="list-Inventario-list" data-toggle="list" href="#list-Inventario" role="tab" aria-controls="Inventario">Inventario</a>
								<a class="list-group-item list-group-item-action" id="list-PDF-list" data-toggle="list" href="#list-PDF" role="tab" aria-controls="PDF">Reporteria - PDF</a>
								<a class="list-group-item list-group-item-action" id="list-EXCEL-list" data-toggle="list" href="#list-EXCEL" role="tab" aria-controls="EXCEL">Reporteria - EXCEL</a>
							</div>
						</div>
						

						<div class="col-md-6 permission-list">
							<div class="tab-content" id="nav-tabContent" style="height:25em; overflow-y:auto; overflow-x:hidden;">
								<!-- Administracion -->
								<div class="tab-pane fade show active" id="list-Gerencia" role="tabpanel" aria-labelledby="list-Gerencia-list">

									<table class="table table-hover">
										<thead class="thead-dark">
											<tr>
												<th>Acción</th>
												<th></th>
											</tr>
										</thead>
										<tbody>

											<tr>
												<td><label>Modulo Clientes</label></td>
												<td><input type="checkbox" name="admcli" class="admin" value="1" /></td>
											</tr>
											<tr>
												<td><label>Modulo Trabajadores</label></td>
												<td><input type="checkbox" name="admusu" class="admin" value="1" /></td>
											</tr>
											<tr>
												<td><label>Modulo Roles</label></td>
												<td><input type="checkbox" name="admper" class="admin" value="1" /></td>
											</tr>
											
											<input type="hidden" name="admege" class="admin" value="1" />
											<input type="hidden" name="admpar" class="admin" value="1" />
											<input type="hidden" name="admemp" class="admin" value="1" />
											<input type="hidden" name="admprv" class="admin" value="1" />
											<input type="hidden" name="admcau" class="admin" value="1" />
											<input type="hidden" name="admfpa" class="admin" value="1" />
											<input type="hidden" name="admmde" class="admin" value="1" />
											<input type="hidden" name="admman" class="admin" value="1" />
											<input type="hidden" name="admaut" class="admin" value="1" />
											<input type="hidden" name="admgesdoc" class="admin" value="1" />

											
										</tbody>
									</table>
								</div>

								<!-- Contratos -->
								<div class="tab-pane fade" id="list-Ventas" role="tabpanel" aria-labelledby="list-Ventas-list">
									
									<table class="table table-hover">
										<thead class="thead-dark">
											<tr>
												<th>Acción</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Generar Venta</td>
												<td><input type="checkbox" name="contrato_agregar" value="1" /></td>
											</tr>
											<tr>
												<td>Editar Venta</td>
												<td><input type="checkbox" name="contrato_editar" value="1" /></td>
											</tr>
											<tr>
												<td>Ver Venta</td>
												<td><input type="checkbox" name="contrato-ver" value="1" /></td>
											</tr>
											<tr>
												<td>Facturación</td>
												<td><input type="checkbox" name="bofactura" class="bodega" value="1" /></td>
											</tr>
										</tbody>
									</table>
								</div>

								<!-- Recibos -->
								<div class="tab-pane fade" id="list-Cobranza" role="tabpanel" aria-labelledby="list-Cobranza-list">
																		
									<table class="table table-hover">
										<thead class="thead-dark">
											<tr>
												<th>Acción</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<input type="hidden" name="r_cobrarse_add" value="1" />
											<input type="hidden" name="r_cobrarse_edit" value="1" />
											<tr>
												<td>Agregar recibos cobrados</td>
												<td><input type="checkbox" name="r_pagados_add" value="1" /></td>
											</tr>
											<tr>
												<td>Editar recibos cobrados</td>
												<td><input type="checkbox" name="r_pagados_edit" value="1" /></td>
											</tr>
											<tr>
												<td>Imprimir Recibos</td>
												<td><input type="checkbox" name="r_imprimir" value="1" /></td>
											</tr>
										</tbody>
									</table>
								
								</div>
								
								<!-- Consultas -->
								<div class="tab-pane fade" id="list-Inventario" role="tabpanel" aria-labelledby="list-Inventario-list">
																		
									<table class="table table-hover">
										<thead class="thead-dark">
											<tr>
												<th>Acción</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><label>Productos</label></td>
												<td><input type="checkbox" name="admpro" class="admin" value="1" /></td>
											</tr>
											<tr>
												<td>Documentos</td>
												<td><input type="checkbox" name="boddoc" class="bodega" value="1" /></td>
											</tr>
								

										</tbody>
									</table>

								</div>
								
								<!-- Informes -->
								<div class="tab-pane fade" id="list-PDF" role="tabpanel" aria-labelledby="list-PDF-list">
										
									<div class="row">
										<button type="button" class="btn btn-k-i" onclick="seleccionar_todo('.check-informe');">Seleccionar Todo</button>
										<button type="button" class="btn btn-k-w" onclick="deseleccionar('.check-informe');">Quitar Todo</button>
									</div>
									<table class="table table-hover">
										<thead class="thead-dark">
											<tr>
												<th>Acción</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											
											<?php
												$consultas = $db->query("SELECT * FROM informesdinamicos WHERE ocultar = false ORDER BY idinombre ASC");
												while($row_consulta = $consultas->fetch(PDO::FETCH_ASSOC))
												{
													$inf_nombre = $row_consulta['idinombre'];
													$inf_id = $row_consulta['idiid'];
													echo "<tr>".
															"<td>$inf_nombre</td>".
															"<td><input type='checkbox' class='check-informe' name='informes[]' value='$inf_id' /></td>".
													"</tr>";
												}
											?>
										</tbody>
									</table>
								</div>
								<!-- CallCenter -->
								<div class="tab-pane fade" id="list-EXCEL" role="tabpanel" aria-labelledby="list-EXCEL-list">
									
									<div class="row">
										<button type="button" class="btn btn-k-i" onclick="seleccionar_todo('.check-consulta');">Seleccionar Todo</button>
										<button type="button" class="btn btn-k-w" onclick="deseleccionar('.check-consulta');">Quitar Todo</button>
									</div>
									<table class="table table-hover">
										<thead class="thead-dark">
											<tr>
												<th>Acción</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											
											<?php
												$consultas = $db->query("SELECT * FROM consultasdinamicas where cdiactiva = true ORDER BY cdinombre ASC");
												while($row_consulta = $consultas->fetch(PDO::FETCH_ASSOC))
												{
													$con_nombre = $row_consulta['cdinombre'];
													$con_id = $row_consulta['cdiid'];
													
													echo "<tr>".
															"<td>$con_nombre</td>".
															"<td><input type='checkbox' class='check-consulta' name='consultas[]' value='$con_id' /></td>".
													"</tr>";
												}
											?>
										</tbody>
									</table>
								
								
								
									<input type="hidden" name="callLlamar" class="admin" value="1" /> 
									<input type="hidden" name="CallCampana" class="admin" value="1" /> 
									<input type="hidden" name="CallGeneral" class="admin" value="1" /> 
									<input type="hidden" name="CallCapaci" class="admin" value="1" /> 
									<input type="hidden" name="CallSemaforo" class="admin" value="1" /> 
									<input type="hidden" name="CallRepo" class="admin" value="1" /> 
									<input type="hidden" name="CallViaInser" class="admin" value="1" /> 
									<input type="hidden" name="Callviavalor" class="admin" value="1" /> 
									<input type="hidden" name="CallViaCons" class="admin" value="1" /> 
								</div>

								<!-- Solicitudes -->
								<div class="tab-pane fade" id="list-Solicitudes" role="tabpanel" aria-labelledby="list-Solicitudes-list">
									<input type="checkbox" name="solnor" class="solicitud" value="1" /> Solicitud normal<br />
									<input type="checkbox" name="sollib" class="solicitud" value="1" /> Solicitud libranza<br />
									<input type="checkbox" name="solest" class="solicitud" value="1" /> Solicitud estimulacion<br />
									<input type="checkbox" name="Solmod" class="solicitud" value="1" /> Modificar solicitud<br />
									<input type="checkbox" name="Solrea" class="solicitud" value="1" /> Cancelar solicitud<br />
									<input type="checkbox" name="Solcan" class="solicitud" value="1" /> Reactivar solicitud<br />
									<input type="checkbox" name="solanu" class="solicitud" value="1" /> Anular solicitud<br />
									<input type="checkbox" name="Soleli" class="solicitud" value="1" /> Eliminar solicitud<br />
									<input type="checkbox" name="solcon" class="solicitud" value="1" /> Consultar solicitud<br />
									<input type="checkbox" name="solscr" class="solicitud" value="1" /> Screen<br />
									<input type="checkbox" name="solche" class="solicitud" value="1" /> Check fisico<br />
									<input type="checkbox" name="Solsopor" class="solicitud" value="1" /> Soporte<br />
									<input type="checkbox" name="Solrepor" class="solicitud" value="1" /> Reporte<br />
									<input type="checkbox" name="Solprop" class="solicitud" value="1" /> Mis Solicitudes<br />
								</div>

								<!-- Facturacion -->
								<div class="tab-pane fade" id="list-Facturacion" role="tabpanel" aria-labelledby="list-Facturacion-list">
									<input type="checkbox" name="facfac" class="factura" value="1" /> Facturar<br />
									<input type="checkbox" name="faccon" class="factura" value="1" /> Consultar<br />
									<input type="checkbox" name="facrep" class="factura" value="1" /> Reportes<br />
									<input type="checkbox" name="Facrepcall" class="factura" value="1" /> Reportes Call<br />
								</div>

								<!-- Bodega -->
								<div class="tab-pane fade" id="list-Bodega" role="tabpanel" aria-labelledby="list-Bodega-list">
									
									
									
									<input type="checkbox" name="boddes" class="bodega" value="1" /> Despacho<br />
									<input type="checkbox" name="bodpla" class="bodega" value="1" /> Planillas<br />
									<input type="checkbox" name="bodrep" class="bodega" value="1" /> Reportes<br />
								</div>

								<!-- tesoreria -->
								<div class="tab-pane fade" id="list-Tesoreria" role="tabpanel" aria-labelledby="list-Tesoreria-list">
									<input type="checkbox" name="tesoreria" class="tesoreria" value="1" /> Tesoreria<br />
								</div>
								
								<!-- Cartera -->
								<div class="tab-pane fade" id="list-Cartera" role="tabpanel" aria-labelledby="list-Cartera-list">
									<input type="checkbox" name="carnot" class="cartera" value="1" /> Notas<br />
									<input type="checkbox" name="carcon" class="cartera" value="1" /> Consultar cartera<br />
									<input type="checkbox" name="carpro" class="cartera" value="1" /> Promotores<br />
									<input type="checkbox" name="carpag" class="cartera" value="1" /> Aplicar pago<br />
									<input type="checkbox" name="carmas" class="cartera" value="1" /> Consulta Masiva<br />
									<input type="checkbox" name="carfec" class="cartera" value="1" /> Cambiar fecha<br />
									<input type="checkbox" name="carcas" class="cartera" value="1" /> Cartera castigada<br />
									<input type="checkbox" name="cardat" class="cartera" value="1" /> Datacredito<br />
									<input type="checkbox" name="carrep" class="cartera" value="1" /> Reportes<br />
									<input type="checkbox" name="carpaz" class="cartera" value="1" /> Paz y Salvo<br />
								</div>

								<!-- Caja -->
								<div class="tab-pane fade" id="list-Caja" role="tabpanel" aria-labelledby="list-Caja-list">
									<input type="checkbox" name="caja" value="1" /> Caja<br />
								</div>

								<!-- Comisiones -->
								<div class="tab-pane fade" id="list-Comisiones" role="tabpanel" aria-labelledby="list-Comisiones-list">
									<input type="checkbox" name="Cominormal" value="1" /> Comizion normal<br />
									<input type="checkbox" name="Comilibranza" value="1" /> Comizion libranza<br />
									<input type="checkbox" name="Comiestimulacion" value="1" /> Comision estimulacion<br />
									<input type="checkbox" name="Comiprop" value="1" /> Mi comision<br />
								</div>

								<!-- Estadisticos -->
								<div class="tab-pane fade" id="list-Estadisticos" role="tabpanel" aria-labelledby="list-Estadisticos-list">
									<input type="checkbox" name="EstaVent" value="1" /> Ventas<br />
									<input type="checkbox" name="EstaRepor" value="1" /> Reportes<br />
								</div>

								
							</div>
						</div>
					</div>
				</form>
			</article>
		</article>
	</section>

	<script>
		function seleccionar_todo(element)
		{
			$(element).prop('checked', true);
		}
		function deseleccionar(element)
		{
			$(element).prop('checked', false);
		}
	</script>
