


<body>
	<section id="principal">
		<article id="cuerpo">
			<article id="contenido">
				<form id="form" name="form" action="listar.php" method="post">
					<fieldset class="col-md-6">
						<legend class="ui-widget ui-corner-all">Consultar perfiles</legend>
						<p>
							<label for="nombre">Nombre del Perfil:</label> <!-- CAMPO NOMBRE DEL PERFIL -->
							<input type="text" name="nombre" class="perfil" title="Digite el nombre del perfil" />
						</p>
						<div class="row">
							<div class="col-md-12 col-lg-12">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
							</div>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
	</section>