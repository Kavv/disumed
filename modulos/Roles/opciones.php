<?php
$r = '../../';

?>


<div id="msj"></div>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Agregar</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Buscar</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">  
    <?php require('insertar.php');?>
  </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
      
    <?php require('consultar.php');?>
  </div>
</div>



<script src="<?php echo $r.'incluir/kavv_js/makemsj.js'?>" ></script>

<?php
if (isset($error)) {?>
    <script>make_alert({'type': 'danger', 'message': '<?php echo $error?>'});</script>
<?php }
elseif (isset($mensaje)) 
{ ?>
    <script>make_alert({'message': '<?php echo $mensaje?>'});</script>
<?php } ?>