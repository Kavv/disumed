<?php
$filtro = '&nombre=' . $_GET['nombre'];
$perfil_id = $_GET['id1'];
$row = $db->query("SELECT * FROM perfiles WHERE perid = '$perfil_id'")->fetch(PDO::FETCH_ASSOC);
?>


	<script>
		$(document).ready(function() {
			$("input:checkbox").click(function() {
				var element = $(this).attr("id");
				if ($(this).is(":checked")) {
					$("." + element).removeAttr("disabled");
				} else {
					$("." + element).attr("disabled", "disabled");
				}
			});
			$('#acordeon').accordion({
				heightStyle: "content"
			});
		});
	</script>
	<style type="text/css">

		.permission-list {
			background: #e6e6e6;
			border-radius: 15px;
			padding-top: 10px;
			padding-bottom: 10px;
		}
		input[type="checkbox"]{
			width: 25px!important;
		}
		.btn-k-i{
			background: #63abfc;
		}
		.btn-k-i:hover{
			background: #3b97fe;
		}
		.btn-k-w{
			background: #ffd690;
		}
		.btn-k-w:hover{
			background: #f2c16d;
		}
	</style>

	<section id="principal">
		<article id="cuerpo">
			<article id="contenido">
				<form id="form" name="form" action="listar.php?filtro=<?php echo $filtro ?>" method="post">
					<input type="hidden" name="id" value="<?php echo $row['perid'] ?>" readonly />
					<p>
						<label for="nombre" class="tabx">Nombre del Rol:</label> <!-- CAMPO NOMBRE -->
						<input type="text" name="nombre" value="<?php echo $row['pernombre'] ?>" class="nombre validate[required, text-input" title="Digite el nombre del perfil" style="width: 49%!important;"/>
					</p>
					<div class="row">
						<div class="col-6">
							<div class="list-group" id="list-tab" role="tablist">
								<a class="list-group-item list-group-item-action active" id="list-Gerencia-list" data-toggle="list" href="#list-Gerencia" role="tab" aria-controls="Gerencia">Gerencia</a>
								<a class="list-group-item list-group-item-action" id="list-Ventas-list" data-toggle="list" href="#list-Ventas" role="tab" aria-controls="Ventas">Ventas</a>
								<a class="list-group-item list-group-item-action" id="list-Cobranza-list" data-toggle="list" href="#list-Cobranza" role="tab" aria-controls="Cobranza">Cobranza</a>
								<a class="list-group-item list-group-item-action" id="list-Inventario-list" data-toggle="list" href="#list-Inventario" role="tab" aria-controls="Inventario">Inventario</a>
								<a class="list-group-item list-group-item-action" id="list-PDF-list" data-toggle="list" href="#list-PDF" role="tab" aria-controls="PDF">Reporteria - PDF</a>
								<a class="list-group-item list-group-item-action" id="list-EXCEL-list" data-toggle="list" href="#list-EXCEL" role="tab" aria-controls="EXCEL">Reporteria - EXCEL</a>
							</div>
						</div>
						

						<div class="col-md-6 permission-list">
							<div class="tab-content" id="nav-tabContent" style="height:25em; overflow-y:auto; overflow-x:hidden;">
								<!-- Administracion -->
								<div class="tab-pane fade show active" id="list-Gerencia" role="tabpanel" aria-labelledby="list-Gerencia-list">

									<table class="table table-hover">
										<thead class="thead-dark">
											<tr>
												<th>Acción</th>
												<th></th>
											</tr>
										</thead>
										<tbody>

											<tr>
												<td><label>Modulo Clientes</label></td>
												<td><input type="checkbox" <?php if($row['peradmcli']){?> checked <?php } ?> name="admcli" class="admin" value="1" /></td>
											</tr>
											<tr>
												<td><label>Modulo Trabajadores</label></td>
												<td><input type="checkbox" <?php if($row['peradmusu']){?> checked <?php } ?> name="admusu" class="admin" value="1" /></td>
											</tr>
											<tr>
												<td><label>Modulo Roles</label></td>
												<td><input type="checkbox" <?php if($row['peradmper']){?> checked <?php } ?> name="admper" class="admin" value="1" /></td>
											</tr>
											
											<input type="hidden" name="admpar" class="admin" value="1" />
											<input type="hidden" name="admemp" class="admin" value="1" />
											<input type="hidden" name="admprv" class="admin" value="1" />
											<input type="hidden" name="admcau" class="admin" value="1" />
											<input type="hidden" name="admfpa" class="admin" value="1" />
											<input type="hidden" name="admmde" class="admin" value="1" />
											<input type="hidden" name="admman" class="admin" value="1" />
											<input type="hidden" name="admaut" class="admin" value="1" />
											<input type="hidden" name="admgesdoc" class="admin" value="1" />

											
										</tbody>
									</table>
								</div>

								<!-- Contratos -->
								<div class="tab-pane fade" id="list-Ventas" role="tabpanel" aria-labelledby="list-Ventas-list">
									
									<table class="table table-hover">
										<thead class="thead-dark">
											<tr>
												<th>Acción</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Generar Venta</td>
												<td><input type="checkbox" <?php if($row['percontratoadd']){?> checked <?php } ?> name="contrato_agregar" value="1" /></td>
											</tr>
											<tr>
												<td>Editar Venta</td>
												<td><input type="checkbox" <?php if($row['percontratoedit']){?> checked <?php } ?> name="contrato_editar" value="1" /></td>
											</tr>
											<tr>
												<td>Ver Ventas</td>
												<td><input type="checkbox" <?php if($row['percontrato']){?> checked <?php } ?> name="contrato-ver" value="1" /></td>
											</tr>
											<tr>
												<td>Facturación</td>
												<td><input type="checkbox" <?php if($row['percontratofac']){?> checked <?php } ?> name="bofactura" class="bodega" value="1" /></td>
											</tr>
										</tbody>
									</table>
								</div>

								<!-- Recibos -->
								<div class="tab-pane fade" id="list-Cobranza" role="tabpanel" aria-labelledby="list-Cobranza-list">
																		
									<table class="table table-hover">
										<thead class="thead-dark">
											<tr>
												<th>Acción</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<input type="hidden" name="r_cobrarse_add" value="1" />
											<input type="hidden" name="r_cobrarse_edit" value="1" />
											<tr>
												<td>Agregar recibos cobrados</td>
												<td><input type="checkbox" <?php if($row['perpagadoadd']){?> checked <?php } ?> name="r_pagados_add" value="1" /></td>
											</tr>
											<tr>
												<td>Editar recibos cobrados</td>
												<td><input type="checkbox" <?php if($row['perpagadoedit']){?> checked <?php } ?> name="r_pagados_edit" value="1" /></td>
											</tr>
											<tr>
												<td>Imprimir Recibos</td>
												<td><input type="checkbox" <?php if($row['perimprimirrecibos']){?> checked <?php } ?> name="r_imprimir" value="1" /></td>
											</tr>
										</tbody>
									</table>
								
								</div>
								
								<!-- Consultas -->
								<div class="tab-pane fade" id="list-Inventario" role="tabpanel" aria-labelledby="list-Inventario-list">
																		
									<table class="table table-hover">
										<thead class="thead-dark">
											<tr>
												<th>Acción</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><label>Productos</label></td>
												<td><input type="checkbox" <?php if($row['peradmpro']){?> checked <?php } ?> name="admpro" class="admin" value="1" /></td>
											</tr>
											<tr>
												<td>Documentos</td>
												<td><input type="checkbox" <?php if($row['perboddoc']){?> checked <?php } ?> name="boddoc" class="bodega" value="1" /></td>
											</tr>
											<tr>
												<td>Facturación</td>
												<td><input type="checkbox" <?php if($row['percontratofac']){?> checked <?php } ?> name="bofactura" class="bodega" value="1" /></td>
											</tr>
								

										</tbody>
									</table>

								</div>
								
								<!-- Informes -->
								<div class="tab-pane fade" id="list-PDF" role="tabpanel" aria-labelledby="list-PDF-list">
										
									<div class="row">
										<button type="button" class="btn btn-k-i" onclick="seleccionar_todo('.check-informe');">Seleccionar Todo</button>
										<button type="button" class="btn btn-k-w" onclick="deseleccionar('.check-informe');">Quitar Todo</button>
									</div>
									<table class="table table-hover">
										<thead class="thead-dark">
											<tr>
												<th>Acción</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											
											<?php
												$consultas = $db->query("SELECT * FROM informesdinamicos WHERE ocultar = false ORDER BY idinombre ASC");
												$aux_consulta = $db->query("SELECT reporte FROM perfil_reporte 
												inner join informesdinamicos on idiid = reporte 
												WHERE tipo = 2 and perfil = $perfil_id ORDER BY idinombre ASC")->fetchAll(PDO::FETCH_ASSOC);
												$report_exist = 0;
												$cant_exist = count($aux_consulta);
												while($row_consulta = $consultas->fetch(PDO::FETCH_ASSOC))
												{
													$inf_nombre = $row_consulta['idinombre'];
													$inf_id = $row_consulta['idiid'];
													
													if(isset($aux_consulta[$report_exist]['reporte']))
													{
														if($aux_consulta[$report_exist]['reporte'] == $inf_id)
														{
															echo "<tr>".
																	"<td>$inf_nombre</td>".
																	"<td><input type='checkbox' checked class='check-informe' name='informes[]' value='$inf_id' /></td>".
															"</tr>";
															if($cant_exist != $report_exist + 1)
															$report_exist++;
														}
														else
														echo "<tr>".
																"<td>$inf_nombre</td>".
																"<td><input type='checkbox' class='check-informe' name='informes[]' value='$inf_id' /></td>".
														"</tr>";
													}
													else
													echo "<tr>".
															"<td>$inf_nombre</td>".
															"<td><input type='checkbox' class='check-informe' name='informes[]' value='$inf_id' /></td>".
													"</tr>";
												}
											?>
										</tbody>
									</table>
								</div>
								<!-- CallCenter -->
								<div class="tab-pane fade" id="list-EXCEL" role="tabpanel" aria-labelledby="list-EXCEL-list">
									
									<div class="row">
										<button type="button" class="btn btn-k-i" onclick="seleccionar_todo('.check-consulta');">Seleccionar Todo</button>
										<button type="button" class="btn btn-k-w" onclick="deseleccionar('.check-consulta');">Quitar Todo</button>
									</div>
									<table class="table table-hover">
										<thead class="thead-dark">
											<tr>
												<th>Acción</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											
											<?php
												$consultas = $db->query("SELECT * FROM consultasdinamicas where cdiactiva = true ORDER BY cdinombre ASC");
												$aux_consulta = $db->query("SELECT reporte FROM perfil_reporte 
												inner join consultasdinamicas ON cdiid=reporte 
												WHERE tipo = 1 and perfil = $perfil_id ORDER BY cdinombre ASC")->fetchAll(PDO::FETCH_ASSOC);
												$report_exist = 0;
												$cant_exist = count($aux_consulta);
												while($row_consulta = $consultas->fetch(PDO::FETCH_ASSOC))
												{
													$con_nombre = $row_consulta['cdinombre'];
													$con_id = $row_consulta['cdiid'];
													
													if(isset($aux_consulta[$report_exist]['reporte']))
													{
														if($aux_consulta[$report_exist]['reporte'] == $con_id)
														{
															echo "<tr>".
																	"<td>$con_nombre</td>".
																	"<td><input type='checkbox' checked class='check-consulta' name='consultas[]' value='$con_id' /></td>".
															"</tr>";
															if($cant_exist != $report_exist + 1)
															$report_exist++;
														}
														else
														echo "<tr>".
																"<td>$con_nombre</td>".
																"<td><input type='checkbox' class='check-consulta' name='consultas[]' value='$con_id' /></td>".
														"</tr>";
													}
													else
													echo "<tr>".
															"<td>$con_nombre</td>".
															"<td><input type='checkbox' class='check-consulta' name='consultas[]' value='$con_id' /></td>".
													"</tr>";
												}
											?>
										</tbody>
									</table>
								
								
								
									<input type="hidden" name="callLlamar" class="admin" value="1" /> 
									<input type="hidden" name="CallCampana" class="admin" value="1" /> 
									<input type="hidden" name="CallGeneral" class="admin" value="1" /> 
									<input type="hidden" name="CallCapaci" class="admin" value="1" /> 
									<input type="hidden" name="CallSemaforo" class="admin" value="1" /> 
									<input type="hidden" name="CallRepo" class="admin" value="1" /> 
									<input type="hidden" name="CallViaInser" class="admin" value="1" /> 
									<input type="hidden" name="Callviavalor" class="admin" value="1" /> 
									<input type="hidden" name="CallViaCons" class="admin" value="1" /> 
								</div>

								<!-- Solicitudes -->
								<div class="tab-pane fade" id="list-Solicitudes" role="tabpanel" aria-labelledby="list-Solicitudes-list">
									<input type="checkbox" <?php if($row['persolnor']){?> checked <?php } ?> name="solnor" class="solicitud" value="1" /> Solicitud normal<br />
									<input type="checkbox" <?php if($row['persollib']){?> checked <?php } ?> name="sollib" class="solicitud" value="1" /> Solicitud libranza<br />
									<input type="checkbox" <?php if($row['persolest']){?> checked <?php } ?> name="solest" class="solicitud" value="1" /> Solicitud estimulacion<br />
									<input type="checkbox" <?php if($row['persolmod']){?> checked <?php } ?> name="Solmod" class="solicitud" value="1" /> Modificar solicitud<br />
									<input type="checkbox" <?php if($row['persolrea']){?> checked <?php } ?> name="Solrea" class="solicitud" value="1" /> Cancelar solicitud<br />
									<input type="checkbox" <?php if($row['persolcan']){?> checked <?php } ?> name="Solcan" class="solicitud" value="1" /> Reactivar solicitud<br />
									<input type="checkbox" <?php if($row['persolanu']){?> checked <?php } ?> name="solanu" class="solicitud" value="1" /> Anular solicitud<br />
									<input type="checkbox" <?php if($row['persoleli']){?> checked <?php } ?> name="Soleli" class="solicitud" value="1" /> Eliminar solicitud<br />
									<input type="checkbox" <?php if($row['persolcon']){?> checked <?php } ?> name="solcon" class="solicitud" value="1" /> Consultar solicitud<br />
									<input type="checkbox" <?php if($row['persolscr']){?> checked <?php } ?> name="solscr" class="solicitud" value="1" /> Screen<br />
									<input type="checkbox" <?php if($row['persolche']){?> checked <?php } ?> name="solche" class="solicitud" value="1" /> Check fisico<br />
									<input type="checkbox" <?php if($row['persolsopor']){?> checked <?php } ?> name="Solsopor" class="solicitud" value="1" /> Soporte<br />
									<input type="checkbox" <?php if($row['persolrepor']){?> checked <?php } ?> name="Solrepor" class="solicitud" value="1" /> Reporte<br />
									<input type="checkbox" <?php if($row['persolprop']){?> checked <?php } ?> name="Solprop" class="solicitud" value="1" /> Mis Solicitudes<br />
								</div>

								<!-- Facturacion -->
								<div class="tab-pane fade" id="list-Facturacion" role="tabpanel" aria-labelledby="list-Facturacion-list">
									<input type="checkbox" <?php if($row['perfacfac']){?> checked <?php } ?> name="facfac" class="factura" value="1" /> Facturar<br />
									<input type="checkbox" <?php if($row['perfaccon']){?> checked <?php } ?> name="faccon" class="factura" value="1" /> Consultar<br />
									<input type="checkbox" <?php if($row['perfacrep']){?> checked <?php } ?> name="facrep" class="factura" value="1" /> Reportes<br />
									<input type="checkbox" <?php if($row['perfacrepcall']){?> checked <?php } ?> name="Facrepcall" class="factura" value="1" /> Reportes Call<br />
								</div>

								<!-- Bodega -->
								<div class="tab-pane fade" id="list-Bodega" role="tabpanel" aria-labelledby="list-Bodega-list">
									<input type="checkbox" <?php if($row['perboddoc']){?> checked <?php } ?> name="boddoc" class="bodega" value="1" /> Documentos<br />
									<input type="checkbox" <?php if($row['percontratofac']){?> checked <?php } ?> name="bofactura" class="bodega" value="1" /> Facturar<br />
									<input type="checkbox" <?php if($row['perboddes']){?> checked <?php } ?> name="boddes" class="bodega" value="1" /> Despacho<br />
									<input type="checkbox" <?php if($row['perbodpla']){?> checked <?php } ?> name="bodpla" class="bodega" value="1" /> Planillas<br />
									<input type="checkbox" <?php if($row['perbodrep']){?> checked <?php } ?> name="bodrep" class="bodega" value="1" /> Reportes<br />
								</div>

								<!-- tesoreria -->
								<div class="tab-pane fade" id="list-Tesoreria" role="tabpanel" aria-labelledby="list-Tesoreria-list">
									<input type="checkbox" <?php if($row['pertesoreria']){?> checked <?php } ?> name="tesoreria" class="tesoreria" value="1" /> Tesoreria<br />
								</div>
								
								<!-- Cartera -->
								<div class="tab-pane fade" id="list-Cartera" role="tabpanel" aria-labelledby="list-Cartera-list">
									<input type="checkbox" <?php if($row['percarnot']){?> checked <?php } ?> name="carnot" class="cartera" value="1" /> Notas<br />
									<input type="checkbox" <?php if($row['percarcon']){?> checked <?php } ?> name="carcon" class="cartera" value="1" /> Consultar cartera<br />
									<input type="checkbox" <?php if($row['percarpro']){?> checked <?php } ?> name="carpro" class="cartera" value="1" /> Promotores<br />
									<input type="checkbox" <?php if($row['percarpag']){?> checked <?php } ?> name="carpag" class="cartera" value="1" /> Aplicar pago<br />
									<input type="checkbox" <?php if($row['percarmas']){?> checked <?php } ?> name="carmas" class="cartera" value="1" /> Consulta Masiva<br />
									<input type="checkbox" <?php if($row['percarfec']){?> checked <?php } ?> name="carfec" class="cartera" value="1" /> Cambiar fecha<br />
									<input type="checkbox" <?php if($row['percarcas']){?> checked <?php } ?> name="carcas" class="cartera" value="1" /> Cartera castigada<br />
									<input type="checkbox" <?php if($row['percardat']){?> checked <?php } ?> name="cardat" class="cartera" value="1" /> Datacredito<br />
									<input type="checkbox" <?php if($row['percarrep']){?> checked <?php } ?> name="carrep" class="cartera" value="1" /> Reportes<br />
									<input type="checkbox" <?php if($row['percarpaz']){?> checked <?php } ?> name="carpaz" class="cartera" value="1" /> Paz y Salvo<br />
								</div>

								<!-- Caja -->
								<div class="tab-pane fade" id="list-Caja" role="tabpanel" aria-labelledby="list-Caja-list">
									<input type="checkbox" <?php if($row['percaja']){?> checked <?php } ?> name="caja" value="1" /> Caja<br />
								</div>

								<!-- Comisiones -->
								<div class="tab-pane fade" id="list-Comisiones" role="tabpanel" aria-labelledby="list-Comisiones-list">
									<input type="checkbox" <?php if($row['percominormal']){?> checked <?php } ?> name="Cominormal" value="1" /> Comizion normal<br />
									<input type="checkbox" <?php if($row['percomilibranza']){?> checked <?php } ?> name="Comilibranza" value="1" /> Comizion libranza<br />
									<input type="checkbox" <?php if($row['percomiestimulacion']){?> checked <?php } ?> name="Comiestimulacion" value="1" /> Comision estimulacion<br />
									<input type="checkbox" <?php if($row['percomiprop']){?> checked <?php } ?> name="Comiprop" value="1" /> Mi comision<br />
								</div>

								<!-- Estadisticos -->
								<div class="tab-pane fade" id="list-Estadisticos" role="tabpanel" aria-labelledby="list-Estadisticos-list">
									<input type="checkbox" <?php if($row['perestavent']){?> checked <?php } ?> name="EstaVent" value="1" /> Ventas<br />
									<input type="checkbox" <?php if($row['perestarepor']){?> checked <?php } ?> name="EstaRepor" value="1" /> Reportes<br />
								</div>

							</div>
						</div>
					</div>
					<p class="boton">
						<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">Atras</button>
						<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button>
					</p>


				</form>
			</article>
		</article>
	</section>

<script>
	function seleccionar_todo(element)
	{
		$(element).prop('checked', true);
	}
	function deseleccionar(element)
	{
		$(element).prop('checked', false);
	}
</script>