
	<style>
		.dataTable
				{
						max-height: 30em!important;
				}
	</style>
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css" />

			
	<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			createdt($("#tabla"),{col:3,com: "desc", buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons});
            
            
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '900',
					height: '500',
					title: 'PDF de la solicitud'
				});
			});
		});
        
	</script>

	<section id="principal">
		<div id="msj"></div>
		<article id="cuerpo">
			<article id="contenido">
				<h2>Listado de contratos <?php  ?></h2>
				<div class="row" style="overflow:auto;">
					<table id="tabla" class="table table-hover" style='width:100%;'>
						<thead>
							<tr>
								<?php if ($rowlog['percontratoedit'] == '1') { ?>
									<th></th>
								<?php } ?>
								<th>Contrato</th>
								<th>Estado</th>
								<th>Fecha Contrato</th>
								<th>Fecha Cobro</th>
								<th>Cedula</th>
								<th>Nombre</th>
								<th>Departa</th>
								<th>Ciudad</th>
								<th>Asesor</th>
								<th>Cobrador</th>
								<th>Entregador</th>
							</tr>
						</thead>
						<tbody >
							<?php
							while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							
								$full_name = $row['clinombre'];
								
								$ruta = "solid=".$row['solid'];
							?>
								<tr>
									<?php if ($rowlog['percontratoedit'] == '1') { ?>
										<td>
											<a target="_blank" href="editar.php?solid=<?php echo $row['solid']."&".$filtro?>"  title="Editar"><img src="<?php echo $r?>imagenes/iconos/lapiz.png" class="grayscale"></a>
										</td>
									<?php } ?>
									<td><?php echo $row['solid'] ?></td>
									<td><?php echo $row['carestado'] ?></td>
									<td><?php echo $row['solfecha'] ?></td>
									<td><?php echo $row['solcompromiso'] ?></td>
									<td><?php echo $row['solcliente'] ?></td>
									<td><?php echo $full_name ?></td>
									<td><?php echo $row['depnombre'] ?></td>
									<td><?php echo $row['ciunombre'] ?></td>
									<td><?php echo $row['solasesor'] ?></td>
									<td><?php echo $row['solrelacionista'] ?></td>
									<td><?php echo $row['solenvio'] ?></td>
									
									</td>
								</tr>
							<?php
							}
							?>
						</tbody>
					</table>
				</div>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick=" carga(); location.href = 'index.php'">atras</button>
				</p>
			</article>
		</article>
	</section>
	<div id="modal" style="display:none"></div>
	
	<script src="<?php echo $r.'incluir/kavv_js/makemsj.js'?>" ></script>

	<?php
	if (isset($error)) {?>
		<script>make_alert({'type': 'danger', 'message': '<?php echo $error?>'});</script>
	<?php }
	elseif (isset($mensaje)) 
	{ ?>
		<script>make_alert({'message': '<?php echo $mensaje?>'});</script>
	<?php } ?>

</body>
