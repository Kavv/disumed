<?php 
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
require('funciones_contrato.php');

// Su valor por defecto es el envio por la consulta get
// Pero este puede cambiar con el post de la edición
if(isset($_GET['solid']))
    $contrato = $_GET['solid'];
if(isset($_POST['contrato-original']))
{
    // Cliente
    $cedula = strupperEsp(str_replace("'", "", $_POST['cedula']??''));
    $nombre = strupperEsp(str_replace("'", "", $_POST['nombre']??''));
    $telefono1 = str_replace("'", "", $_POST['telefono1']??'');
    $telefono2 = str_replace("'", "", $_POST['telefono2']??'');
    $departamento = strupperEsp(str_replace("'", "", $_POST['departamento']??''));
    $full_ciudad = str_replace("'", "", $_POST['ciudad']??'');
    $ciudad = substr($full_ciudad,-2);
    $trabajo = strupperEsp(str_replace("'", "", $_POST['trabajo']??''));
    $direccion = strupperEsp(str_replace("'", "", $_POST['direccion']??''));
    $conyugue = strupperEsp(str_replace("'", "", $_POST['conyugue']??''));
    $conyugue_telefono = str_replace("'", "", $_POST['conyugue_telefono']??'');
    $conyugue_trabajo = strupperEsp(str_replace("'", "", $_POST['conyugue_trabajo']??''));
    $referencia1 = strupperEsp(str_replace("'", "", $_POST['referencia1']??''));
    $ref1_tel1 = str_replace("'", "", $_POST['ref1_tel1']??'');
    $ref1_tel2 = str_replace("'", "", $_POST['ref1_tel2']??'');
    $referencia2 = strupperEsp(str_replace("'", "", $_POST['referencia2']??''));
    $ref2_tel1 = str_replace("'", "", $_POST['ref2_tel1']??'');
    $ref2_tel2 = str_replace("'", "", $_POST['ref2_tel2']??'');

    // Solicitud
    $emp = $db->query("SELECT * FROM empresas")->fetch(PDO::FETCH_OBJ);
    $empresa = $emp->empid;
    $contrato = strupperEsp(str_replace("'", "", $_POST['contrato']??''));
    $fecha_contrato = str_replace("'", "", $_POST['fecha-contrato']??'');
    $estudiante = strupperEsp(str_replace("'", "", $_POST['estudiante']??''));
    $escuela = strupperEsp(str_replace("'", "", $_POST['escuela']??''));
    $grado = strupperEsp(str_replace("'", "", $_POST['grado']??''));
    $observaciones = strupperEsp(str_replace(["'","<",">"], "", $_POST['observaciones']??''));
    
    $razonamientos = [];
    if(isset($_POST['razonamientos']))
    $razonamientos = $_POST['razonamientos'];
    $cantidad_razonamiento = count($razonamientos);

    $id_razonamientos = [];
    if(isset($_POST['id-razonamientos']))
    $id_razonamientos = $_POST['id-razonamientos'];
    $cant_id_razon = count($id_razonamientos);
    
    
    $asesor = $_POST['asesor'];
    $entregador = strupperEsp($_POST['entregador']);
    $cobrador = $_POST['cobrador'];
    // Por defecto
    $clase = 1;
    
    $digitador = $_SESSION['id'];

    // solicitud-detalle
    $producto_codigo = [];
    $producto_nombre = [];
    $producto_precio = [];
    $producto_cantidad = [];
    
    if(isset($_POST['producto-codigo']))
        $producto_codigo = $_POST['producto-codigo'];
    if(isset($_POST['producto-nombre']))
        $producto_nombre = $_POST['producto-nombre'];
    if(isset($_POST['producto-precio']))
        $producto_precio = $_POST['producto-precio'];
    if(isset($_POST['producto-cantidad']))
        $producto_cantidad = $_POST['producto-cantidad'];
        
    $cantidad_productos = count($producto_codigo);

    $tel_aux = "";
    if($telefono1 != "")
        $tel_aux .= $telefono1;
    if($telefono2 != "")
    {
        if($tel_aux != "")
            $tel_aux .= " / ".$telefono2;
        else
            $tel_aux .= $telefono2;
    }

    // Ajuste del nombre
    $nombre1 = $nombre;

    // Agregar el cliente en caso de haber modificado la cedula y ser una cedula nueva
    $aux_cliente = $db->query("SELECT * FROM clientes WHERE cliid = '$cedula'")->fetch(PDO::FETCH_ASSOC);
    
    if(!$aux_cliente)
    {
        // Generamos la insercion del cliente
        $db->query("INSERT INTO clientes(
        clase, cliid, clinombre,
        clicelular, clitelresidencia, clidepresidencia, cliciuresidencia, 
        cliempresa, clidirresidencia, clinomfamiliar, clitelfamiliar, 
        cliempfamiliar, clirefnombre1, clireftelefono1, clirefcelular1, 
        clirefnombre2, clireftelefono2 ,clirefcelular2) 
        VALUES ($clase, '$cedula', '$nombre1',
        '$telefono1', '$telefono2', '$departamento', '$ciudad', 
        '$trabajo', '$direccion', '$conyugue', '$conyugue_telefono', 
        '$conyugue_trabajo', '$referencia1', '$ref1_tel1', '$ref1_tel2', 
        '$referencia2', '$ref2_tel1', '$ref2_tel2' ); ") or die($db->errorInfo()[2]);
    }
    else
    {
        // De lo contrario actualizamos
        actualizar_cliente($aux_cliente);
    }

    
    // Datos de la cartera a agenerarse
    $contrato_original = $_POST['contrato-original'];
    $entrega = $_POST['entrega'];
    $fecha_cobro = $_POST['fecha-cobro'];
    $cobro = $_POST['cobro'];
    $monto_contrato = floatval($_POST['monto-contrato']);
    $prima = floatval($_POST['prima']);
    $cuenta = $_POST['cuenta'];
    if($cuenta == "")
        $cuenta = 1;
    $salida = $_POST['salida'];
    if($salida == "")
        $salida = 0;

    $neto = round(floatval($_POST['neto']), 2);
    $descuento = round(floatval($_POST['descuento']), 2);
    $ncuotas = $_POST['ncuotas'];
    $cuota = round(floatval($_POST['cuota']), 2);
    $saldo = round($neto - $descuento, 2);

    // Datos de lo pagos abonados
    $detalle_original = [];
    $detalle_recibo = [];
    $detalle_fpago = [];
    $detalle_monto = [];
    $detalle_observaciones = [];
    $detalle_cobrador = [];

    if(isset($_POST['detalle-original']))
        $detalle_original = $_POST['detalle-original'];
    if(isset($_POST['detalle-recibo']))
        $detalle_recibo = $_POST['detalle-recibo'];
    if(isset($_POST['detalle-fpago']))
        $detalle_fpago = $_POST['detalle-fpago'];
    if(isset($_POST['detalle-monto']))
        $detalle_monto = $_POST['detalle-monto'];
    if(isset($_POST['detalle-observaciones']))
        $detalle_observaciones = $_POST['detalle-observaciones'];
    if(isset($_POST['detalle-cobrador']))
        $detalle_cobrador = $_POST['detalle-cobrador'];
    
    $aux_solicitud = $db->query("SELECT solfactura FROM solicitudes WHERE solid='$contrato_original';")->fetch(PDO::FETCH_ASSOC);
    $factura  = $aux_solicitud['solfactura'];

    $db->query("UPDATE solicitudes SET 
    
    solid = '$contrato', solfecha = '$fecha_contrato', 
    solcliente = '$cedula', solasesor = '$asesor', solrelacionista = '$cobrador', 
    soldepentrega = '$departamento', solciuentrega = '$ciudad', solentrega = '$direccion', soltelentrega = '$telefono1', 
    soldepcobro = '$departamento', solciucobro = '$ciudad', solcobro = '$direccion', soltelcobro = '$telefono2', 
    solenvio = '$entregador', solobservacion = '$observaciones', 
    estudiante = '$estudiante', escuela = '$escuela', grado = '$grado',

    soltotal = $monto_contrato, solbase = $monto_contrato, solcuota = $prima, solncuota = $ncuotas, 
    solcompromiso = '$fecha_cobro', cobro = '$cobro', solestado = 'ENTREGADO', 
    solfechreg = NOW(), solfechdespacho = '$entrega', solfechdescarga = '$entrega', 
    cuenta = '$cuenta', salida = '$salida'
    WHERE solempresa = '$empresa' AND solid = '$contrato_original'")
    or die($db->errorInfo()[2]);


    // Si existe algun razonamiento
    if($cantidad_razonamiento > 0)
    {
        
        if($cant_id_razon > 0)
        {
            // En caso de que el usuario ha eliminado algun razonamiento que estubiese en la BD
            // La siguiente linea de codigo se encargar de eliminarlo definitivamente
            $db->query("DELETE FROM hissolicitudes WHERE 
            hsosolicitud = '$contrato_original' AND hsoempresa = '$empresa'
            AND hsoid not in (" . implode(',', $id_razonamientos) . ");");
        }

        // Recorremos cada razonamiento existente
        for($i = 0; $i < $cantidad_razonamiento; $i++)
        {
            // Si el razonamiento actual es inferior a la cantidad de id de razonamientos existentes
            // Significa que este razonamiento ya existe por lo que debe ser actualizado y no eliminado
            if($i < $cant_id_razon)
            {
                $new_razonamiento = strupperEsp(str_replace(["'","<",">"], "", $razonamientos[$i]??''));
                $or_razonamiento = $db->query("SELECT hsonota, hsosolicitud FROM hissolicitudes WHERE hsoid = ".$id_razonamientos[$i])->fetch(PDO::FETCH_ASSOC);
                // Si el razonamiento original es diferente al nuevo razonamiento entonces 
                if($or_razonamiento['hsonota'] != $new_razonamiento || $or_razonamiento['hsosolicitud'] != $contrato)
                {
                    // Actualizamos el razonamiento y el digitador que modifico el original
                    $db->query("UPDATE hissolicitudes SET  
                    hsonota = '$new_razonamiento', hsousuario = '$digitador', hsosolicitud = '$contrato'
                    WHERE hsoid = ".$id_razonamientos[$i]);
                }
            }
            else
            {
                // Si la cantidad de id de razonamientos es inferior entonces es un nuevo razonamiento
                $aux_razonamiento = strupperEsp(str_replace(["'","<",">"], "", $razonamientos[$i]??''));
                
                $db->query("INSERT INTO hissolicitudes(hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) 
                VALUES ('$empresa', '$contrato', NOW(), '$digitador', '$aux_razonamiento');") or die($db->errorInfo()[2]);
            }
        }
    }
    else
    {
        //Eliminamos los razonamientos existentes
        $db->query("DELETE FROM hissolicitudes WHERE hsoempresa = '$empresa' AND hsosolicitud = '$contrato_original'");
    }

    // Actualizar Movimiento de factura
    $db->query("UPDATE movimientos SET 
    movtercero = '$cedula', movestado = 'FACTURADO',
    movvalor = '$monto_contrato', movdescuento = '$descuento', movsaldo = '$saldo'  
    WHERE movempresa = '$empresa' AND movdocumento = '$factura' AND movprefijo = 'FV';")
    or die($db->errorInfo()[2]);

    
    // Actualzar Detalle movimiento
    // Con detsolicitud esta logica funciona perfectamente (borrar y volver a crear)
    // En cambio detmovimiento como gestiona datos importantes en el inventario lo correcto seria aplicar una logica como en la edición razonamiento
    $db->query("DELETE FROM detsolicitudes WHERE detempresa = '$empresa' AND detsolicitud = '$contrato_original'");
    if($cantidad_productos > 0)
    {
        for($i = 0; $i < $cantidad_productos; $i++)
        {
            $aux_codigo = $producto_codigo[$i];
            $aux_cantidad = $producto_cantidad[$i];
            $aux_precio = $producto_precio[$i];
            $aux_costo = $db->query("SELECT procosto from productos WHERE proid = '$aux_codigo'");
            $aux_costo = $aux_costo->fetch(PDO::FETCH_ASSOC);
            if($aux_costo)
                $aux_costo = $aux_costo['procosto'];
            else
                $aux_costo = 0;
                
            $total_costo = $aux_costo * $aux_cantidad;
            $total_precio = $aux_precio * $aux_cantidad;

            $db->query("INSERT INTO detsolicitudes (detsolicitud, detempresa, detproducto, detcantidad, detprecio) 
            VALUES ('$contrato', '$empresa', '$aux_codigo', $aux_cantidad, $aux_precio);") or die($db->errorInfo()[2]);
            // aquí cambiamos mov_numero por factura porque igual sigue siendo el mismo dato
        }
    }

    // SEGUNDA PARTE

    // Creación de cartera
    // En caso de que el # de cuotas sea mayor a 0 entonces se genera una cartera
    if($ncuotas > 0)
    {
        $estado = $_POST['cartera-estado'];
        $cartera = $db->query("SELECT * FROM carteras WHERE carempresa = '$empresa' AND carfactura = '$factura'")->fetch(PDO::FETCH_ASSOC);
        if($cartera)
        {
            $db->query("UPDATE carteras SET
            carcliente = '$cedula', cartotal = '$neto', carcuota = '$cuota', 
            carsaldo = '$saldo', carncuota = '$ncuotas', carestado = '$estado'
            WHERE carempresa = '$empresa' AND carfactura = '$factura'")
            or die($db->errorInfo()[2]);

            $log_usuario = $_SESSION['id'];
            $log_ip = $_SERVER['REMOTE_ADDR'];
            $a_cliente_org = $cartera['carcliente'];
            $a_neto_org = $cartera['cartotal'];
            $a_cuota_org = $cartera['carcuota'];
            $a_saldo_org = $cartera['carsaldo'];
            $a_ncuotas_org = $cartera['carncuota'];
            $a_estado_org = $cartera['carestado'];

            $log_msj = "INICIO - EDICIÓN DEL CONTRATO $contrato, MODIFCIACIONES: ";

            if($a_cliente_org != $cedula )
                $log_msj .= ", CLIENTE $a_cliente_org por $cedula";
            if($a_neto_org != $neto )
                $log_msj .= ", NETO $a_neto_org por $neto";
            if($a_cuota_org != $cuota )
                $log_msj .= ", CUOTA $a_cuota_org por $cuota";
            if($a_saldo_org != $saldo )
                $log_msj .= ", SALDO $a_saldo_org por $saldo";
            if($a_ncuotas_org != $ncuotas )
                $log_msj .= ", #CUOTAS $a_ncuotas_org por $ncuotas";
            if($a_estado_org != $estado)
                $log_msj .= ", ESTADO $a_estado_org por $estado";

            $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) 
            VALUES ('$log_usuario', '$log_ip' , '$log_msj' , NOW() );"); 

        }
        else
        {
            $db->query("INSERT INTO carteras(carempresa, carfactura, carcliente, cartotal, carcuota, carsaldo, carncuota, carfecha, carestado, created_at)
            VALUES ('$empresa', '$factura', '$cedula', $neto, $cuota, $saldo, $ncuotas, NOW(), '$estado', NOW() );")
            or die($db->errorInfo()[2]);

            
            $log_usuario = $_SESSION['id'];
            $log_ip = $_SERVER['REMOTE_ADDR'];
            $log_msj = "-ACTUALIZACIÓN DEL CONTRATO $contrato NO EXISTIA SU CARTERA - SE LE CREO UNA";
            $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) 
            VALUES ('$log_usuario', '$log_ip' , '$log_msj' , NOW() );"); 
        }
        
        $fechacom = $fecha_cobro;
        // Eliminamos el detalle de la cartera existente
        $db->query("DELETE FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$factura'");

        // Creación de detalle cartera
        for($i = 1; $i <= $ncuotas; $i++)
        {
            $db->query("INSERT INTO detcarteras (dcaempresa, dcafactura, dcacuota, dcafecha, dcaestado) 
            VALUES ('$empresa', '$factura', $i, '$fechacom', 'ACTIVA')")
            or die($db->errorInfo()[2]);
            $fechacom = date('Y-m-d', strtotime($fechacom.' next month'));
        }

        // Actualizamos el movimiento de recibos
        // Define la cantidad de pagos que ya se habian realizado
        $originales = count($detalle_original);
        // Define la cantidad total de pagos existentes desde interfaz
        $cantidad_pagos = count($detalle_monto);
        $aux_saldo = $saldo;
        $comision = true;


        for($i = 0; $i < $cantidad_pagos; $i++)
        {
            $movcobrador = strtoupper($detalle_cobrador[$i]);
            if($movcobrador == '')
                $movcobrador = 'PERDIDO';
                
            // Numero del recibo
            $num_recibo = $detalle_recibo[$i];
            if($num_recibo == "" || $num_recibo == 0)
            {
                // Obtenemos el ultimo recibo con numero negativo para poder asignar a todos aquellos pagos sin numero de recibo
                $recibo_automatico = $db->query("select nextval('sec_factura_negativa') as factura")->fetch(PDO::FETCH_ASSOC);
                if($recibo_automatico)
                    $recibo_automatico = $recibo_automatico['factura'];
                else 
                    $recibo_automatico = -1;
                    
                $num_recibo = $recibo_automatico;
            }


            if($i < $originales)
            {
                $original_aux = $detalle_original[$i];
                $new_recibo = $detalle_recibo[$i];
                $movvalor = $detalle_monto[$i];
                // En base al (neto - descuento) lo cual es equivalende al primer saldo del contrato (sin incluir algun pago)
                // Esto definira el nuevo saldo en cada movimiento ya existente, ya que el monto puede ser modificado
                $aux_saldo -= $movvalor;
                $fecha_de_pago = $detalle_fpago[$i];
                $db->query("UPDATE movimientos 
                SET movnumero = '$new_recibo', movtercero = '$cedula', movvalor = '$movvalor', 
                movsaldo = '$aux_saldo', movfecha = '$fecha_de_pago', movcobrador = '$movcobrador'
                WHERE movprefijo='RC' AND movempresa='$empresa' AND movnumero='$original_aux'")
                or die($db->errorInfo()[2]);
                
                // Trabajamos el detalle de la cartera pero sin crear un movimiento nuevo
                $movimiento_nuevo = false;
                pagar($db, $contrato, $empresa, $factura, $cedula,
                $cuota, 1, $detalle_monto[$i], 0, $detalle_fpago[$i], 1, $movcobrador, 
                $nombre, $num_recibo, null,
                $movimiento_nuevo, $comision);
            }
            else
            {
                // Creamos el nuevo pago
                $movimiento_nuevo = true;


                $digitador = $_SESSION['id'];
                $comentario = "";
                $db->query("INSERT INTO infoextra 
                (comentario, digitador)
                VALUES ('$comentario', '$digitador');") or die($db->errorInfo()[2]);
                
                $id_info = $db->lastInsertId();

                pagar($db, $contrato, $empresa, $factura, $cedula,
                $cuota, 1, $detalle_monto[$i], 0, $detalle_fpago[$i], 1, $movcobrador, 
                $nombre, $num_recibo, $id_info,
                $movimiento_nuevo, $comision);
            }
            // Se cambia a falso ya que solo en la primer iteración nos intereza que se realice la funcion de comision.
            $comision = false;
        }
    }

    
    $qrylogsregister = $db->query("SELECT * FROM usuarios WHERE usuid = '$digitador'"); //verificacion usuario por ID de sesion
    $rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);	
    // Log de la acción realizada
    $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) 
    VALUES ( '".$rowlogsregister['usunombre']."', '$digitador', '".$_SERVER['REMOTE_ADDR']."' , 'FIN - SE EDITO EL CONTRATO: $contrato' , NOW() );"); 

    $mensaje = "¡Contrato $contrato modificado exitosamente!";
    header("location:editar.php?solid=$contrato&mensaje=$mensaje");
    exit();
}

$data = $db->query("SELECT * FROM solicitudes 
INNER JOIN clientes ON cliid = solcliente 
LEFT JOIN departamentos ON depid = soldepentrega 
LEFT JOIN ciudades ON (ciuid = solciuentrega AND ciudepto = soldepentrega)
LEFT JOIN movimientos on (movdocumento = solfactura AND movprefijo = 'FV') 
LEFT JOIN carteras on carfactura = solfactura 
WHERE solid = '$contrato'; ")->fetch(PDO::FETCH_ASSOC);

$full_name = $data['clinombre'];
if( $data['clinom2'] != "" )
    $full_name .= " " . $data['clinom2'];
if( $data['cliape1'] != "" )
    $full_name .= " " . $data['cliape1'];
if( $data['cliape2'] != "" )
    $full_name .= " " . $data['cliape2'];

// Cliente
$cedula = strupperEsp(str_replace("'", "", $data['solcliente']??''));
$nombre = strupperEsp(str_replace("'", "", $full_name??''));
$telefono1 = str_replace("'", "", $data['soltelentrega']??'');
$telefono2 = str_replace("'", "", $data['soltelcobro']??'');
$departamento = strupperEsp(str_replace("'", "", $data['soldepentrega']??''));

$ciudad = $data['solciuentrega'];
$full_ciudad = str_replace("'", "", $data['soldepentrega'].$ciudad??'');

$trabajo = strupperEsp(str_replace("'", "", $data['cliempresa']??''));
$direccion = strupperEsp(str_replace("'", "", $data['solentrega']??''));
$conyugue = strupperEsp(str_replace("'", "", $data['clinomfamiliar']??''));
$conyugue_telefono = str_replace("'", "", $data['clitelfamiliar']??'');
$conyugue_trabajo = strupperEsp(str_replace("'", "", $data['cliempfamiliar']??''));
$referencia1 = strupperEsp(str_replace("'", "", $data['clirefnombre1']??''));
$ref1_tel1 = str_replace("'", "", $data['clireftelefono1']??'');
$ref1_tel2 = str_replace("'", "", $data['clirefcelular1']??'');
$referencia2 = strupperEsp(str_replace("'", "", $data['clirefnombre2']??''));
$ref2_tel1 = str_replace("'", "", $data['clireftelefono2']??'');
$ref2_tel2 = str_replace("'", "", $data['clirefcelular2']??'');

// Solicitud
$emp = $db->query("SELECT * FROM empresas")->fetch(PDO::FETCH_OBJ);
$empresa = $emp->empid;
//$contrato = strupperEsp(str_replace("'", "", $data['solid']));
$fecha_contrato = str_replace("'", "", $data['solfecha']??'');
$estudiante = strupperEsp(str_replace("'", "", $data['estudiante']??''));
$escuela = strupperEsp(str_replace("'", "", $data['escuela']??''));
$grado = strupperEsp(str_replace("'", "", $data['grado']??''));
$observaciones = strupperEsp(str_replace(["'","<",">"], "", $data['solobservacion']??''));

$razonamientos = $db->query("SELECT hissolicitudes.* FROM solicitudes INNER JOIN hissolicitudes ON hsosolicitud = solid WHERE solid='$contrato' ORDER BY hsoid ASC");

if(isset($data['razonamientos']))
$razonamientos = $data['razonamientos'];
$cantidad_razonamiento = $razonamientos->rowCount();


$asesor = $data['solasesor'];
$entregador = $data['solenvio'];
$cobrador = $data['solrelacionista'];

// Por defecto
$clase = $data['clase'];
$salida = $data['salida'];
$cuenta = $data['cuenta'];
$digitador = $data['digitador'];

// solicitud-detalle

$productos = $db->query("SELECT detproducto, detcantidad, detprecio, pronombre FROM solicitudes INNER JOIN detsolicitudes ON detsolicitud = solid INNER JOIN productos on proid = detproducto WHERE solid = '$contrato';");
$cantidad_productos = $productos->rowCount();

// Información de pago
$fentrega = $data['solfechdescarga'];
$fcobro = $data['solcompromiso'];
$cobrar = $data['cobro'];
$monto_contrato = $data['soltotal'];
$prima = $data['solcuota'];
$neto = $data['cartotal'];
$descuento = $data['movdescuento'];
$ncuotas = $data['carncuota'];
$cuota = $data['carcuota'];
$saldo = $data['carsaldo'];
$factura = $data['solfactura'];
$cartera_estado = $data['carestado'];

$vista = 'Factura/editar_contrato.php';
$titulo = "MODIFICAR CONTRATO";
require($r. 'incluir/src/menu_new.php');
?>