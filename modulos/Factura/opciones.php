<div id="msj"></div>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Nueva venta</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-factura-tab" data-toggle="pill" href="#pills-factura" role="tab" aria-controls="pills-factura" aria-selected="false">Facturar venta</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Buscar ventas</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-contrato-tab" data-toggle="pill" href="#pills-contrato" role="tab" aria-controls="pills-contrato" aria-selected="false">Buscaqueda veloz</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
    <?php require('insertar.php'); ?>
  </div>

  <div class="tab-pane fade" id="pills-factura" role="tabpanel" aria-labelledby="pills-factura-tab">
    <?php require('buscar_factura.php'); ?>

  </div>
  
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
    <?php require('buscar_contratos.php'); ?>
  </div>
  
  <div class="tab-pane fade" id="pills-contrato" role="tabpanel" aria-labelledby="pills-contrato-tab">
    <?php require('ver_contrato.php'); ?>
  </div>
</div>


<script src="<?php echo $r . 'incluir/kavv_js/makemsj.js' ?>"></script>

<?php
if (isset($_GET['msj'])) { ?>
  <script>
    make_alert({
      'message': '<?php echo $_GET['msj'] ?>'
    });
  </script>
<?php } ?>