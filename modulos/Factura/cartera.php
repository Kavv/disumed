<?php
$r = '../../';

    if(isset($_POST['cedula']))
    {
        require($r . 'incluir/session.php');
        require($r . 'incluir/connection.php');
        require($r . 'incluir/funciones.php');
        require('funciones_contrato.php');
        // Cliente
        $cedula = strupperEsp(str_replace("'", "", $_POST['cedula']??''));
        $nombre = strupperEsp(str_replace("'", "", $_POST['nombre']??''));
        $telefono1 = str_replace("'", "", $_POST['telefono1']??'');
        $telefono2 = str_replace("'", "", $_POST['telefono2']??'');
        $departamento = strupperEsp(str_replace("'", "", $_POST['departamento']??''));
        $full_ciudad = str_replace("'", "", $_POST['ciudad']??'');
        $ciudad = substr($full_ciudad,-2);
        $trabajo = strupperEsp(str_replace("'", "", $_POST['trabajo']??''));
        $direccion = strupperEsp(str_replace("'", "", $_POST['direccion']??''));
        $conyugue = strupperEsp(str_replace("'", "", $_POST['conyugue']??''));
        $conyugue_telefono = str_replace("'", "", $_POST['conyugue_telefono']??'');
        $conyugue_trabajo = strupperEsp(str_replace("'", "", $_POST['conyugue_trabajo']??''));
        $referencia1 = strupperEsp(str_replace("'", "", $_POST['referencia1']??''));
        $ref1_tel1 = str_replace("'", "", $_POST['ref1_tel1']??'');
        $ref1_tel2 = str_replace("'", "", $_POST['ref1_tel2']??'');
        $referencia2 = strupperEsp(str_replace("'", "", $_POST['referencia2']??''));
        $ref2_tel1 = str_replace("'", "", $_POST['ref2_tel1']??'');
        $ref2_tel2 = str_replace("'", "", $_POST['ref2_tel2']??'');
    
        // Solicitud
        $emp = $db->query("SELECT * FROM empresas")->fetch(PDO::FETCH_OBJ);
        $empresa = $emp->empid;
        $contrato = strupperEsp(str_replace("'", "", $_POST['contrato']??''));
        $fecha_contrato = str_replace("'", "", $_POST['fecha-contrato']??'');
        $observaciones = strupperEsp(str_replace(["'","<",">"], "", $_POST['observaciones']??''));
        $razonamientos = [];
        if(isset($_POST['razonamientos']))
        $razonamientos = $_POST['razonamientos'];
        $cantidad_razonamiento = count($razonamientos);
        $asesor = $_POST['asesor'];
        $entregador = strupperEsp($_POST['entregador']);
        $cobrador = $_POST['cobrador'];
        // Por defecto
        $clase = 1;
        $cuenta = 1;
        $digitador = $_SESSION['id'];
    
        // solicitud-detalle
        $producto_codigo = [];
        $producto_nombre = [];
        $producto_precio = [];
        $producto_cantidad = [];
        
        if(isset($_POST['producto-codigo']))
            $producto_codigo = $_POST['producto-codigo'];
        if(isset($_POST['producto-nombre']))
            $producto_nombre = $_POST['producto-nombre'];
        if(isset($_POST['producto-precio']))
            $producto_precio = $_POST['producto-precio'];
        if(isset($_POST['producto-cantidad']))
            $producto_cantidad = $_POST['producto-cantidad'];
            
        $cantidad_productos = count($producto_codigo);
    
        $tel_aux = "";
        if($telefono1 != "")
            $tel_aux .= $telefono1;
        if($telefono2 != "")
        {
            if($tel_aux != "")
                $tel_aux .= " / ".$telefono2;
            else
                $tel_aux .= $telefono2;
        }
    
        // Ajuste del nombre
        $nombre1 = $nombre;
        
    
        if(isset($_POST['guardar-contrato']))
        {
        
            $aux_cliente = $db->query("SELECT * FROM clientes WHERE cliid = '$cedula'")->fetch(PDO::FETCH_ASSOC);
            // Si no hay ningun cliente con el numero de cedula entonces
            if(!$aux_cliente)
            {
                // Generamos la insercion del cliente
                $db->query("INSERT INTO clientes(
                clase, cliid, clinombre,
                clicelular, clitelresidencia, clidepresidencia, cliciuresidencia, 
                cliempresa, clidirresidencia, clinomfamiliar, clitelfamiliar, 
                cliempfamiliar, clirefnombre1, clireftelefono1, clirefcelular1, 
                clirefnombre2, clireftelefono2 ,clirefcelular2) 
                VALUES ($clase, '$cedula', '$nombre1',
                '$telefono1', '$telefono2', '$departamento', '$ciudad', 
                '$trabajo', '$direccion', '$conyugue', '$conyugue_telefono', 
                '$conyugue_trabajo', '$referencia1', '$ref1_tel1', '$ref1_tel2', 
                '$referencia2', '$ref2_tel1', '$ref2_tel2' ); ") or die($db->errorInfo()[2]);
            }
            else
            {
                actualizar_cliente($aux_cliente);
            }
    
    
            
            $aux_solicitud = $db->query("SELECT solid FROM solicitudes WHERE solid='$contrato';")->fetch(PDO::FETCH_ASSOC);
            if(!$aux_solicitud)
            {
                $db->query("INSERT INTO solicitudes
                (solempresa, solid, soltipo, solfecha, solcliente, solasesor, solrelacionista, 
                soldepentrega, solciuentrega, solentrega, soltelentrega, 
                soldepcobro, solciucobro, solcobro, soltelcobro, 
                solestado, solfechreg, solfechafac, 
                solenvio, solfisico, solobservacion, digitador, cuenta)
                VALUES 
                ('$empresa', '$contrato', 'NORMAL', '$fecha_contrato', '$cedula', '$asesor', '$cobrador', 
                '$departamento', '$ciudad', '$direccion', '$telefono1', 
                '$departamento', '$ciudad', '$direccion', '$telefono2', 
                'BORRADOR', NOW(), NOW(), 
                '$entregador', 0, '$observaciones', '$digitador', '$cuenta');") or die($db->errorInfo()[2]);
    
                $ultima_factura = $db->query("SELECT solfactura FROM solicitudes where solid = '$contrato'")->fetch(PDO::FETCH_ASSOC);
                if($ultima_factura)
                {
                    $factura = $ultima_factura['solfactura'];
                    $mov_numero = $factura;
                }
                else
                {
                    echo 'Error critico al crear el nuevo contrato contactar inmediatamente a Kevin Valverde <a href="tel:50582449347">8244-9347</a>';
                    exit();
                }
        
                
                // En caso de existir razonamiento se agrega como historial de la solicitud
                if($cantidad_razonamiento > 0)
                {
                    for($i = 0; $i < $cantidad_razonamiento; $i++)
                    {
                        $aux_razonamiento = strupperEsp(str_replace(["'","<",">"], "", $razonamientos[$i]??''));
                        
                        $db->query("INSERT INTO hissolicitudes(hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) 
                        VALUES ('$empresa', '$contrato', NOW(), '$digitador', '$aux_razonamiento');") or die($db->errorInfo()[2]);
                    }
                }
    
                $db->query("INSERT INTO movimientos(movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movestado) 
                VALUES ('$empresa', 'FV', '$mov_numero', '$cedula', '$factura', NOW(), 'PROCESO');") or die($db->errorInfo()[2]);
            
                
                //$db->query("DELETE FROM detsolicitudes WHERE detempresa='$empresa' AND detsolicitud='$contrato'");
                //$db->query("DELETE FROM detmovimientos WHERE dmoempresa='$empresa' AND dmoprefijo = 'FV' AND dmonumero='$numero'");
                
                if($cantidad_productos > 0)
                {
                    for($i = 0; $i < $cantidad_productos; $i++)
                    {
                        $aux_codigo = $producto_codigo[$i];
                        $aux_cantidad = $producto_cantidad[$i];
                        $aux_precio = $producto_precio[$i];
                        $aux_costo = $db->query("SELECT procosto from productos WHERE proid = '$aux_codigo'");
                        $aux_costo = $aux_costo->fetch(PDO::FETCH_ASSOC);
                        if($aux_costo)
                            $aux_costo = $aux_costo['procosto'];
                        else
                            $aux_costo = 0;
                            
                        $total_costo = $aux_costo * $aux_cantidad;
                        $total_precio = $aux_precio * $aux_cantidad;
            
                        $db->query("INSERT INTO detsolicitudes (detsolicitud, detempresa, detproducto, detcantidad, detprecio) 
                        VALUES ('$contrato', '$empresa', '$aux_codigo', $aux_cantidad, $aux_precio);") or die($db->errorInfo()[2]);
                        
                    }
                }
                
                $qrylogsregister = $db->query("SELECT * FROM usuarios WHERE usuid = '".$_SESSION['id']."'"); //verificacion usuario por ID de sesion
                $rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);	
                // Log de la acción realizada
                $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '".$rowlogsregister['usunombre']."', '".$_SESSION['id']."', '".$_SERVER['REMOTE_ADDR']."' , 'SE CREO LA 1ER PARTE DEL CONTRATO: $contrato' , NOW() );"); 
            
                
                $db->query("INSERT INTO carteras(carempresa, carfactura, carcliente, cartotal, carcuota, carsaldo, carncuota, carfecha, carestado, created_at)
                VALUES ('$empresa', '$factura', '$cedula', 0, 0, 0, 1, NOW(), 'ACTIVA', NOW() );");
            }
    
        
        }
    
        if(isset($_POST['informacion-pago']))
        {
    
            // Agregar el cliente en caso de haber modificado la cedula y ser una cedula nueva
            $aux_cliente = $db->query("SELECT * FROM clientes WHERE cliid = '$cedula'")->fetch(PDO::FETCH_ASSOC);
            // Si no hay ningun cliente con el numero de cedula entonces
            if(!$aux_cliente)
            {
                // Generamos la insercion del cliente
                $db->query("INSERT INTO clientes(clase, cliid, clinombre, clinom2, cliape1, cliape2, clicelular, clitelresidencia, clidepresidencia, cliciuresidencia, cliempresa, clidirresidencia, clinomfamiliar, clitelfamiliar, cliempfamiliar, clirefnombre1, clireftelefono1, clirefcelular1, clirefnombre2, clireftelefono2 ,clirefcelular2) 
                VALUES ($clase, '$cedula', '$nombre1', '$nombre2', '$apellido1', '$apellido2', '$telefono1', '$telefono2', '$departamento', '$ciudad', '$trabajo', '$direccion', '$conyugue', '$conyugue_telefono', '$conyugue_trabajo', '$referencia1', '$ref1_tel1', '$ref1_tel2', '$referencia2', '$ref2_tel1', '$ref2_tel2' ); ") or die($db->errorInfo()[2]);
            }
            else
            {
                actualizar_cliente($aux_cliente);
            }
    
            // Datos de la cartera a agenerarse
            $contrato_original = $_POST['contrato-original'];
            $entrega = $_POST['entrega'];
            $fecha_cobro = $_POST['fecha-cobro'];
            $cobro = $_POST['cobro'];
            $monto_contrato = floatval($_POST['monto-contrato']);
            $prima = floatval($_POST['prima']);
            $cuenta = $_POST['cuenta'];
            if($cuenta == "")
                $cuenta = 1;
            $salida = $_POST['salida'];
            if($salida == "")
                $salida = 0;
    
            $neto = floatval($_POST['neto']);
            $descuento = floatval($_POST['descuento']);
            $ncuotas = $_POST['ncuotas'];
            $cuota = floatval($_POST['cuota']);
            $saldo = round($neto - $descuento, 2);
    
            // Datos de lo pagos abonados
            $detalle_recibo = [];
            $detalle_fpago = [];
            $detalle_monto = [];
            $detalle_observaciones = [];
            if(isset($_POST['detalle-recibo']))
                $detalle_recibo = $_POST['detalle-recibo'];
            if(isset($_POST['detalle-fpago']))
                $detalle_fpago = $_POST['detalle-fpago'];
            if(isset($_POST['detalle-monto']))
                $detalle_monto = $_POST['detalle-monto'];
            if(isset($_POST['detalle-observaciones']))
                $detalle_observaciones = $_POST['detalle-observaciones'];
            if(isset($_POST['detalle-cobrador']))
                $detalle_cobrador = $_POST['detalle-cobrador'];
    
            
            $aux_solicitud = $db->query("SELECT solfactura FROM solicitudes WHERE solid='$contrato_original';")->fetch(PDO::FETCH_ASSOC);
            $factura  = $aux_solicitud['solfactura'];
    
            $db->query("UPDATE solicitudes SET 
            
            solid = '$contrato', solfecha = '$fecha_contrato', 
            solcliente = '$cedula', solasesor = '$asesor', solrelacionista = '$cobrador', 
            soldepentrega = '$departamento', solciuentrega = '$ciudad', solentrega = '$direccion', soltelentrega = '$telefono1', 
            soldepcobro = '$departamento', solciucobro = '$ciudad', solcobro = '$direccion', soltelcobro = '$telefono2', 
            solenvio = '$entregador', solobservacion = '$observaciones',
    
            soltotal = $monto_contrato, solbase = $monto_contrato, solcuota = $prima, solncuota = $ncuotas, 
            solcompromiso = '$fecha_cobro', cobro = '$cobro', solestado = 'ENTREGADO', 
            solfechafac = NOW(), solfechdespacho = '$entrega', solfechdescarga = '$entrega', 
            cuenta = '$cuenta', salida = '$salida'
            WHERE solempresa = '$empresa' AND solid = '$contrato_original'")
            or die($db->errorInfo()[2]);
    
    
            //Eliminamos los razonamientos existentes
            $db->query("DELETE FROM hissolicitudes WHERE hsoempresa = '$empresa' AND hsosolicitud = '$contrato_original'");
    
            // En caso de existir razonamiento se agrega como historial de la solicitud
            if($cantidad_razonamiento > 0)
            {
                for($i = 0; $i < $cantidad_razonamiento; $i++)
                {
                    $aux_razonamiento = strupperEsp(str_replace(["'","<",">"], "", $razonamientos[$i]??''));
                    
                    $db->query("INSERT INTO hissolicitudes(hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) 
                    VALUES ('$empresa', '$contrato', NOW(), '$digitador', '$aux_razonamiento');") or die($db->errorInfo()[2]);
                }
            }
    
            // Movimiento
            $db->query("UPDATE movimientos SET 
            movtercero = '$cedula', movestado = 'FACTURADO',
            movvalor = '$monto_contrato', movdescuento = '$descuento', movsaldo = '$saldo'  
            WHERE movempresa = '$empresa' AND movdocumento = '$factura' AND movprefijo = 'FV';")
            or die($db->errorInfo()[2]);
    
            // Detalle movimiento
            $db->query("DELETE FROM detsolicitudes WHERE detempresa = '$empresa' AND detsolicitud = '$contrato_original'");
            if($cantidad_productos > 0)
            {
                for($i = 0; $i < $cantidad_productos; $i++)
                {
                    $aux_codigo = $producto_codigo[$i];
                    $aux_cantidad = $producto_cantidad[$i];
                    $aux_precio = $producto_precio[$i];
                    $aux_costo = $db->query("SELECT procosto from productos WHERE proid = '$aux_codigo'");
                    $aux_costo = $aux_costo->fetch(PDO::FETCH_ASSOC);
                    if($aux_costo)
                        $aux_costo = $aux_costo['procosto'];
                    else
                        $aux_costo = 0;
                        
                    $total_costo = $aux_costo * $aux_cantidad;
                    $total_precio = $aux_precio * $aux_cantidad;
        
                    $db->query("INSERT INTO detsolicitudes (detsolicitud, detempresa, detproducto, detcantidad, detprecio) 
                    VALUES ('$contrato', '$empresa', '$aux_codigo', $aux_cantidad, $aux_precio);") or die($db->errorInfo()[2]);
                    
                }
            }
    
            // SEGUNDA PARTE
    
            // Creación de cartera
            // En caso de que el # de cuotas sea mayor a 0 entonces se genera una cartera
            if($ncuotas > 0)
            {
                $aux_cartera = $db->query("SELECT * FROM carteras WHERE carempresa='$empresa' AND carfactura='$factura'")->fetch(PDO::FETCH_ASSOC);
                if($aux_cartera)
                {
                    $db->query("DELETE FROM carteras WHERE carempresa='$empresa' AND carfactura='$factura'");
                    $db->query("DELETE FROM detcarteras WHERE dcaempresa='$empresa' AND dcafactura='$factura'");
                }
    
                $estado = "ACTIVA";
                $db->query("INSERT INTO carteras(carempresa, carfactura, carcliente, cartotal, carcuota, carsaldo, carncuota, carfecha, carestado, created_at)
                VALUES ('$empresa', '$factura', '$cedula', $neto, $cuota, $saldo, $ncuotas, NOW(), '$estado', NOW() );")
                or die($db->errorInfo()[2]);
            
                
                $fechacom = $fecha_cobro;
                // Creación de detalle cartera
                for($i = 1; $i <= $ncuotas; $i++)
                {
                    $db->query("INSERT INTO detcarteras (dcaempresa, dcafactura, dcacuota, dcafecha, dcaestado) 
                    VALUES ('$empresa', '$factura', $i, '$fechacom', 'ACTIVA')")
                    or die($db->errorInfo()[2]);
                    $fechacom = date('Y-m-d', strtotime($fechacom.' next month'));
                }
    
                // Aplicamos los pagos en caso de existir
                $cantidad_pagos = count($detalle_monto);
                $movimiento_nuevo = true;
                $comision = true;
    
                
                
                
                for($i = 0; $i < $cantidad_pagos; $i++)
                {
                    // Numero del recibo
                    $num_recibo = $detalle_recibo[$i];
                    if($num_recibo == "" || $num_recibo == 0)
                    {
                        // Obtenemos el ultimo recibo con numero negativo para poder asignar a todos aquellos pagos sin numero de recibo
                        $recibo_automatico = $db->query("select nextval('sec_factura_negativa') as factura")->fetch(PDO::FETCH_ASSOC);
                        if($recibo_automatico)
                            $recibo_automatico = $recibo_automatico['factura'];
                        else 
                            $recibo_automatico = -1;
                            
                        $num_recibo = $recibo_automatico;
                    }
    
                    $digitador = $_SESSION['id'];
                    $comentario = "";
                    $db->query("INSERT INTO infoextra 
                    (comentario, digitador)
                    VALUES ('$comentario', '$digitador');") or die($db->errorInfo()[2]);
                    
                    $id_info = $db->lastInsertId();
    
                    $aux_cobrador = strtoupper($detalle_cobrador[$i]);
                    pagar($db, $contrato, $empresa, $factura, $cedula,
                    $cuota, 1, $detalle_monto[$i], 0, $detalle_fpago[$i], 1, $aux_cobrador, 
                    $nombre, $num_recibo, $id_info,
                    $movimiento_nuevo, $comision);
                    
                    $comision = false;
                }
            }
            $msj = "¡Contrato ".$contrato." creado exitosamente!";
            header("location:index.php?msj=$msj");
            exit();
        }
    }
    




$vista = 'Factura/informacion_de_pago.php';
$titulo = "CARTERA";
require($r. 'incluir/src/menu_new.php');

?>