
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<section id="principal">
        <section class='form-style'>
            <div class="" id="msj">

            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-md-5 text-center">
                    <label for="contrato" class="default text-center bold">Ingrese el codigo</label> 
                    <div class="input-group mb-3">
                        <input autocomplete='off' style="border: 3px solid #006dad !important;" type="text" name="contrato" id="contrato_veloz" 
                        class="form-control contrato not-w uppercase" title="Digite # de contrato" />
                        <div class="input-group-append">
                            <button type="button" id="btnbuscar_veloz" class="btn btn-primary" name="consultar">BUSCAR</button> <!-- BOTON CONSULTAR -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="show-contrato" style="min-height:20em">
            

        </section>
	</section>
	
</body>
<script>
    $("#btnbuscar_veloz").click(function(){
        var contrato = $("#contrato_veloz").val();
        carga();
        var ruta = "ajax/all_info_contrato.php?contrato="+contrato;
        $.get(ruta, function(res){
            $("#show-contrato").empty();
            $("#show-contrato").append(res);
            
            ocultarCarga();
        });
    });

    function msj(tipo, texto)
    {
        var html = 
            '<div class="alert alert-' + tipo + ' alert-dismissible fade show" role="alert">'+
                texto +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                    '<span aria-hidden="true">&times;</span>'+
               ' </button>'+
           ' </div>';
        
        
        
        '<div class="alert alert-' + tipo + '" role="alert">' + texto + '</div>';
        $("#msj").empty();
        $("#msj").append(html);
    }

    $("#contrato_veloz").keypress(function(e) {
        if (e.which == 13) {
            $("#btnbuscar_veloz").click();
            return false;
        }
    });
</script>
</html>