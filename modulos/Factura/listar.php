<?php
$r = '../../';


    require($r . 'incluir/session.php');
    require($r . 'incluir/connection.php');
    $numero = $cliente = $fecha1 = $fecha2 = $estado = "";
    if (isset($_POST['consultar'])) {
        $contrato = strupperEsp(str_replace("'", "", $_POST['contrato']??''));
        $cliente = strupperEsp(str_replace("'", "", $_POST['cliente']??''));
        $fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
		$estado = $_POST['estado'];
		$nombre = strupperEsp($_POST['nombre']);

		$dep = $_POST['departamento'];
		$ciu = $_POST['ciudad'];
		$asesor = $_POST['asesor'];
		$cobrador = $_POST['cobrador'];
		$entregador = $_POST['entregador'];
		$telefono = $_POST['telefono'];
		$direccion = strupperEsp(str_replace("'", "", $_POST['direccion']??''));
		$referencia = strupperEsp(str_replace("'", "", $_POST['referencia']??''));
		$fecha_cobro = $_POST['cobro'];

    } elseif(isset($_GET['contrato'])) {
        @$contrato = strupperEsp(str_replace("'", "", $_GET['contrato']??''));
        @$cliente = strupperEsp(str_replace("'", "", $_GET['cliente']??''));
        @$fecha1 = $_GET['fecha1'];
        @$fecha2 = $_GET['fecha2'];
		@$estado = $_GET['estado'];
		@$nombre = strupperEsp($_GET['nombre']);
		@$dep = $_GET['departamento'];
		@$ciu = $_GET['ciudad'];
		@$asesor = $_GET['asesor'];
		@$cobrador = $_GET['cobrador'];
		@$entregador = $_GET['entregador'];
		@$telefono = $_GET['telefono'];
		@$direccion = strupperEsp(str_replace("'", "", $_GET['direccion']??''));
		@$referencia = strupperEsp(str_replace("'", "", $_GET['referencia']??''));
		@$fecha_cobro = $_GET['cobro'];
    }

    $filtro = 'contrato=' . $contrato . '&cliente=' . $cliente . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&estado=' . $estado . 
	'&departamento=' . $dep . '&ciudad=' . $ciu . '&asesor=' . $asesor . '&cobrador=' . $cobrador . '&entregador=' . $entregador.
	'&telefono='. $telefono. '&direccion=' . $direccion . '&referencia=' . $referencia  . '&cobro=' . $fecha_cobro ;
    
	$con = 'SELECT * FROM solicitudes INNER JOIN clientes ON cliid = solcliente LEFT JOIN departamentos ON depid = soldepentrega LEFT JOIN ciudades ON (ciuid = solciuentrega AND ciudepto = soldepentrega) LEFT JOIN carteras ON carfactura = solfactura';
    $ord = ' ORDER BY solfecha';

    /* Los parametros de la consulta sql se genera dinamicamente 
    en base a los datos recibidos para delimitar los resultados */
    $parameters = [];
    if($contrato != "")
        array_push($parameters, "solid LIKE '%$contrato%'" );
    if($cliente != "")
        array_push($parameters, "solcliente LIKE '%$cliente%'" );
    if($fecha1 != "")
        array_push($parameters, "solfecha BETWEEN '$fecha1' AND '$fecha2'" );
	if($estado != "")
		array_push($parameters, "carestado = '$estado'" );
	if($nombre != "")
		array_push($parameters, "upper(concat(cliNombre, ' ', cliNom2, ' ', cliApe1, ' ', cliApe2)) LIKE '%$nombre%'" );
	
	if($dep != "")
		array_push($parameters, "soldepentrega = '$dep'" );
	if($ciu != "")
		array_push($parameters, "solciuentrega = '$ciu'" );
	if($asesor != "")
		array_push($parameters, "solasesor = '$asesor'" );
	if($cobrador != "")
		array_push($parameters, "solrelacionista = '$cobrador'" );
	if($entregador != "")
		array_push($parameters, "solenvio = '$entregador'" );
	
	if($telefono != "")
		array_push($parameters, "soltelentrega iLIKE '%$telefono%' OR soltelcobro iLIKE '%$telefono%'" );
	
	if($direccion != "")
		array_push($parameters, "solentrega like '%$direccion%'" );
	
	if($referencia != "")
		array_push($parameters, "clirefnombre1 ILIKE '%$referencia%' OR clirefnombre2 ILIKE '%$referencia%'" );
	
	if($fecha_cobro != "")
		array_push($parameters, "cobro = $fecha_cobro" );
	
	

	
    // Consulta base
    $sql = $con;
    foreach ($parameters as $index => $parameter) {
        // Se agregan los parametros del WHERE
        if($index == 0)
            $sql .= " WHERE " . $parameter;
        else
            $sql .= " AND " . $parameter;
    }
    // Se completa la consulta
	$sql .= $ord;
	
$qry = $db->query($sql);
$cantidad_contrato = $qry->rowCount();


$vista = 'Factura/listar_contratos.php';
$titulo = "BUSCAR CONTRATOS";
require($r. 'incluir/src/menu_new.php');

?>