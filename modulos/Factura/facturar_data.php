

    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #ffa81a;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
        .input-disabled {
            background :#e0e0e0;
            pointer-events: none;
        }
        
        #monto-total{
            font-size: 20px!important;
            font-weight: bold;
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">
        show_carga_modal = false;
        var first_time = true;
        var $select_cobradores = "";
		$(document).ready(function() {
            input_fecha();
            $("input").prop("autocomplete","off");

            $('#form').validationEngine({
                onValidationComplete: function(form, status) {
                    if (status) {
                        return true;
                    }
                }
            });
            
            var $ciudades = [];
            actualizar_saldo();
            first_time = false;
            info_extra();
            $("#principal").css("visibility", "visible");

            
            var ruta = 'ajax/cobradores_asesores.php';
            $select_cobradores += "<select class='sin-inicializar detalle-cobrador form-control validate[required] selectpicker' name='detalle-cobrador[]'  data-live-search='true' title='Seleccione un cobrador' >";
            $select_cobradores += "<option value=''></option>";
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){
                    nombre = "";
                    if(data.usunombre != "")
                        nombre += "/" + data.usunombre;
                    $select_cobradores += "<option value='"+ data.usuid +"'> "+ data.usuid + nombre +"</option>";
                });
                $select_cobradores += "</select>";
                agregar_select_cobradores();
            });
        
            function agregar_select_cobradores()
            {
                var cantidad = $(".codigo-cobrador-existente").length;
                for(var i = 0; i < cantidad; i++)
                {
                    $(".cobrador-existente").eq(i).append($select_cobradores);
                }
                $(".sin-inicializar").selectpicker("refresh");
                ajustar_cobrador();
            }
            function ajustar_cobrador()
            {
                var cantidad = $(".codigo-cobrador-existente").length;
                var codigos = $(".codigo-cobrador-existente");
                var $select_aux = $("select.detalle-cobrador");
                for(var i = 0; i < cantidad; i++)
                {
                    cobrador = codigos.eq(i).val();
                    $select_aux.eq(i).val(cobrador);
                    if($select_aux.eq(i).val() == "")
                    {
                        var html = "<option selected value='" + cobrador + "'>" + cobrador + "</option>";
                        $select_aux.eq(i).append(html);
                    }
                }
                $(".sin-inicializar").selectpicker("refresh");
                $(".sin-inicializar").removeClass("sin-inicializar");
            }
            calcular_monto_productos();

            
            $("input").prop("autocomplete","off");
            
            // Almacenar las ciudades
            const ruta_ciudad = '../Carteras/ciudades.php';
            var first_time_change = true;
            $.get(ruta_ciudad, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){

                    var dep = data.ciudepto;
                    var ciu = data.ciuid;
                    var nombre = data.ciunombre;
                    // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                    if($ciudades[dep] != null)
                    {
                        $ciudades[dep].push([ciu, nombre]);
                    }
                    else
                    {
                        // Si es la primera vez se asigna el primer dato como un arreglo
                        $ciudades[dep] = [[ciu, nombre]];
                    }
                });
                $("#departamento").change();
            });
            contrato_ciudad = "<?php echo $ciudad?>";
            $("#departamento").change(function(){
                console.log("entra");
                $("#ciudad").empty();
                // Por defecto
                var html = '<option value=""></option>';
                $("#ciudad").append(html);

                dep = $(this).val();
                if(dep == "")
                    cant = 0;
                else
                cant = $ciudades[dep].length;
                for(var i = 0; i < cant; i++)
                {
                    if(contrato_ciudad != $ciudades[dep][i][0])
                        html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
                    else
                        html = "<option selected value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
                    $("#ciudad").append(html);
                }
                $('#ciudad').selectpicker('refresh');
                if(first_time_change)
                {
                    console.log('entra2')
                    $("#ciudad").val(contrato_ciudad);
                    first_time_change = false;
                    $('#ciudad').selectpicker('refresh');
                }
            });
        });
        function input_fecha()
        {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });
        }
        
        
	</script>

	<section id="principal" style="visibility:hidden;">
		<article id="cuerpo">
			<article id="contenido">
				<form id="form" name="form-detalle" action="facturar.php" method="post">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="false" >
                        <div class="carousel-inner">
                            <div class="carousel-item active" id="carousel1">
                                <?php require('facturar_carousel1.php') ?>

                                <div class="col-md-12 mb-3 text-center" id="border-monto">
                                    <p class="font-weight-bold" style="font-size:20px">MONTO TOTAL</p>
                                    <div class="input-group mb-3 justify-content-center d-flex">
                                        <input type="text" id="monto-total" class="form-control col-md-4 text-center"  readonly>
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-info" onclick="calcular_monto_productos();"><i class="fas fa-calculator"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row d-flex justify-content-center mt-5">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="CambioPag();">INFORMACIÓN DE PAGO</button>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item" id="carousel2">
                                <?php require('editar_carousel2.php') ?>
                                <div class="ro">
                                    <div class="col-md-12">
                                        <label for="">COMENTARIO PARA LA FACTURACIÓN</label>
                                        <input type="hidden" name="infoextra" value="<?php echo $infoextra?>">
                                        <textarea type="text" name="comentario-factura" value="" class="form-control validate[maxSize[400]]"><?php echo $comentario_factura?></textarea>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-info btn-block" onclick="CambioPag();">REGRESAR A CONTRATO</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-success btn-block" name="informacion-pago">GUARDAR E IMPRIMIR</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- Modal para ver razonamiento-->
                    <div class="modal fade" id="ver-razonamiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Ver Razonamientos</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-hover">
                                        <thead class="text-center">
                                            <th></th>
                                            <th></th>
                                        </thead>
                                        <tbody id="t-razonamiento">
                                            <?php 
                                                while($razonamiento = $razonamientos->fetch(PDO::FETCH_ASSOC))
                                                {
                                                    echo 
                                                    "<tr>
                                                        <td>
                                                            <input type='hidden' value=". $razonamiento['hsoid'] ." name='id-razonamientos[]'>
                                                            <textarea type='text' name=razonamientos[] class='form-control'>".$razonamiento['hsonota']."</textarea>
                                                        </td>
                                                        <td><button type='button' class='btn btn-danger' onclick='remove_razonamiento(this);'>Eliminar</button></td>
                                                    </tr>";
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal para agregar razonamiento-->
                    <div class="modal fade" id="add-razonamiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Agregar Razonamiento</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <label for="">Describa el razonamiento</label>
                                    <textarea id="razonamiento" class="form-control not-w" rows="3"></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success" data-dismiss="modal" onclick="add_razonamiento();">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </article>
        </article>
    </section>
    
<script>
    var prueba;

    function check(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8) {
            return true;
        }

        // Patron de entrada, en este caso solo acepta numeros y letras
        patron = /[A-Za-z0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }


    function CambioPag()
    {
        if($("#carousel1").hasClass('active'))
        {
            if(!first_step_validation())
            {
                buscar_contrato();
            }
        }
        else
        {
            $("#carousel2").removeClass("active");
            $("#carousel1").addClass("active");
        }
    }

    $("#form").submit(function(e){
        
        if(incompleto == true)
        {
            $("#modal-recibos").modal("show");
            e.preventDefault();
        }
        else
        {
            if($("#form").validationEngine('validate'))
                carga();
        }
        
    });
    
    function buscar_contrato()
    {
        var original = $("#contrato-original").val();
        var contrato = $("#contrato").val();
        ruta = "ajax/contrato.php?contrato="+contrato+"&original="+original;
        $.get(ruta, function(res){
            if(JSON.parse(res))
            {
                update_total();
                $("#carousel1").removeClass("active");
                $("#carousel2").addClass("active");
            }
            else
            {
                $("#contrato").validationEngine('validate')
                $("#contrato").focus();
            }
        });
    }

    function first_step_validation()
    {
        var inputs = ["#cedula", "#fecha-factura", "#fecha-contrato", "#nombre", "#asesor", ".producto-cantidad", ".producto-precio", "#departamento", "#ciudad"];
        var inputs_ajax = ["#contrato"];
        for(var i = 0; i < inputs.length; i++)
        {
            cant_inputs = $(inputs[i]).length;
            //Si la cantidad de inputs es igual a 1 entonces
            if(cant_inputs == 1)
            {
                // ejecutamos la validacion individual
                if( $(inputs[i]).validationEngine('validate') )
                {
                    $(inputs[i]).focus();
                    return true;
                }
            }
            else
            {
                // Significa que hay mas de 1 input del mismo tipo
                for(j = 0; j < cant_inputs; j++)
                {
                    
                    if( $(inputs[i]).eq(j).validationEngine('validate') )
                    {
                        $(inputs[i]).eq(j).focus();
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    
    function update_total()
    {
        var cantidad_productos = $('.producto-condigo').length;
        var total = 0;
        for(var i = 0; i < cantidad_productos; i++)
        {
            var precio = $(".producto-precio").eq(i).val();
            var cantidad = $(".producto-cantidad").eq(i).val();
            total += (precio*cantidad);
        }
        //$("#monto-contrato").val(total);
        //calcular();
    }
    
    $('#add-razonamiento').on('show.bs.modal', function (e) {
        $("#razonamiento").val("");
    });
    $('#add-razonamiento').on('shown.bs.modal', function (e) {
        $("#razonamiento").focus();
    });

    function calcular_monto_productos()
    {
        var precios = $(".producto-precio");
        var cantidad = $(".producto-cantidad");

        var cant_productos = precios.length;
        var total_productos = 0;
        var temp_total = 0;
        for(var i = 0; i < cant_productos; i++)
        {
            temp_total = parseFloat(precios.eq(i).val()) * parseInt(cantidad.eq(i).val());
            total_productos += temp_total;
        }
        $("#monto-total").val(total_productos.toFixed(2));
    }
</script>