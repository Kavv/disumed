
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style type="text/css">

		#form label {
			display: inline-block;
			width: 100px;
			text-align: right;
			margin: 0.3em 2% 0 0
		}

		#form p {
			margin: 5px 0
		}
	</style>
	<section id="principal">
		<article id="cuerpo">
			<article id="contenido">
				<form id="form2" class="form-style" name="form" action="listar.php" method="post">
                    
					<fieldset class="col-md-12">
						<legend class="ui-widget ui-corner-all">Consultar Venta</legend>
						<p>
							<div class="row">
								<div class="col-md-4">
									<label for="contrato">Contrato:</label> 
									<input type="text" name="contrato" id="contrato_buscar" class="form-control contrato" title="Digite # de contrato" />
								</div>
								<div class="col-md-4">
									<label for="fechas">Fecha de contrato - Desde</label>
									<input type="text" class="fecha validate[custom[date]]" id="fecha1" name="fecha1" /> 
								</div>
								<div class="col-md-4">
									<label for="">Fecha de contrato - Hasta</label>
									<input type="text" id="fecha2" class="fecha validate[custom[date]]" name="fecha2" />
								</div>
							</div>
						</p>
						
						<p>
							<div class="row">
								<div class="col-md-4">
									<label for="contrato">Cedula cliente:</label> 
									<input type="text" name="cliente" id="cliente" class="form-control cliente" title="Digite cedula del cliente"  onkeypress="return check(event)"/>
								</div>
								<div class="col-md-4">
									<label for="contrato">Nombre cliente:</label> 
									<input type="text" name="nombre" id="nombre_buscar" class="form-control" title="Digite el nombre del cliente"  />
								</div>
								<div class="col-md-4">
									<label for="Estado">Estado:</label>
									<select name="estado" id="estado">
									<option value="">TODOS</option>
									<?php
										$estados = $db->query("SELECT * FROM estadoscartera ORDER BY estaid");
										while($estado = $estados->fetch(PDO::FETCH_ASSOC))
										{
											$descripcion = $estado['estadescripcion'];
											echo "<option value='$descripcion'>$descripcion</option>"; 
										}
									?>
									</select> 
								</div>
							</div>
                        </p>
						<p>
							<div class="row">
								<div class="col-md-4">
									<label for="">Fecha de Cobro</label>
									<input type="text" class="form-control" value="" name="cobro" placeholder="Especifique un valor entre 1 y 31">
								</div>
								<div class="col-md-4">
									<label for="Departamento">Departamento:</label>
									<select name="departamento" id="departamento_buscar" class="selectpicker" data-live-search="true" title="TODOS" data-width="100%">
									<option value="">TODOS</option>
									<?php
										$departamentos = $db->query("SELECT * FROM departamentos ORDER BY depnombre");
										while($departamento = $departamentos->fetch(PDO::FETCH_ASSOC))
										{
											$nombre_dep = $departamento['depnombre'];
											$id_dep = $departamento['depid'];
											echo "<option value='$id_dep'>$nombre_dep</option>"; 
										}
									?>
									</select> 
								</div>
								<div class="col-md-4">
									<label for="Ciudad">Ciudad:</label>
									<select name="ciudad" id="ciudad_buscar" class="selectpicker" data-live-search="true" title="TODOS" data-width="100%">
									<option value="">TODOS</option>
									</select> 
								</div>
							</div>
                        </p>
						<p>
							<div class="row">
								<div class="col-md-4">
									<label for="asesor">Asesor:</label>
									<select name="asesor" id="asesor_buscar" class="selectpicker" data-live-search="true" title="TODOS" data-width="100%">
									<option value="">TODOS</option>
									<?php
										$asesores = $db->query("SELECT * FROM usuarios WHERE usuasesor = 1 ORDER BY usunombre ASC");
										while($asesor = $asesores->fetch(PDO::FETCH_ASSOC))
										{
											$nombre_asesor = $asesor['usunombre'];
											$id_asesor = $asesor['usuid'];
											echo "<option value='$id_asesor'>$id_asesor / $nombre_asesor</option>"; 
										}
									?>
									</select> 
								</div>
								<div class="col-md-4">
									<label for="cobrador">Cobrador:</label>
									<select name="cobrador" id="cobrador_buscar" class="selectpicker" data-live-search="true" title="TODOS" data-width="100%">
									<option value="">TODOS</option>
									<?php
										$cobradores = $db->query("SELECT * FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre ASC");
										while($cobrador = $cobradores->fetch(PDO::FETCH_ASSOC))
										{
											$nombre_cobrador = $cobrador['usunombre'];
											$id_cobrador = $cobrador['usuid'];
											echo "<option value='$id_cobrador'>$id_cobrador / $nombre_cobrador</option>"; 
										}
									?>
									</select> 
								</div>
								<div class="col-md-4">
									<label for="entregador">Entregador:</label>
									<select name="entregador" id="entregador_buscar" class="selectpicker" data-live-search="true" title="TODOS" data-width="100%">
									<option value="">TODOS</option>
									<?php
										$entregadores = $db->query("SELECT * FROM mdespachos ORDER BY mdenombre ASC");
										while($entregador = $entregadores->fetch(PDO::FETCH_ASSOC))
										{
											$nombre_entregador = $entregador['mdenombre'];
											$id_entregador = $entregador['mdeid'];
											echo "<option value='$id_entregador'>$id_entregador / $nombre_entregador</option>"; 
										}
									?>
									</select> 
								</div>
							</div>
                        </p>
						<p>
							<div class="row">
								<div class="col-md-4">
									<label for="">Teléfono</label>
									<input type="text" class="form-control" value="" name="telefono">
								</div>
								<div class="col-md-4">
									<label for="">Dirección del cliente</label>
									<input type="text" class="form-control" value="" name="direccion">
								</div>
								<div class="col-md-4">
									<label for="">Nombre Referencia</label>
									<input type="text" class="form-control" value="" name="referencia">
								</div>
							</div>
						</p>

						<div class="row">
							<div class="col-md-12 col-lg-12">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar">BUSCAR</button> <!-- BOTON CONSULTAR -->
							</div>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
	</section>
	
<script>
    $(document).ready(function(){

		
		$('#form').validationEngine({
			onValidationComplete: function(form, status) {
				if (status) {
					return true;
				}
			}
		});
		
        // Almacenar las ciudades
        ruta = "<?php echo $r . 'modulos/Carteras/ciudades.php';?>" ;
        $ciudades = [];
        $.get(ruta, function(res){
            res = JSON.parse(res);
            res.forEach(function(data){

                var dep = data.ciudepto;
                var ciu = data.ciuid;
                var nombre = data.ciunombre;
                // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                if($ciudades[dep] != null)
                {
                    $ciudades[dep].push([ciu, nombre]);
                }
                else
                {
                    // Si es la primera vez se asigna el primer dato como un arreglo
                    $ciudades[dep] = [[ciu, nombre]];
                }
            });
        });
	});

    $('#fecha1').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        onClose: function(selectedDate) {
			if(!$("#fecha1").validationEngine('validate') && !$("#fecha2").validationEngine('validate'))
            	$('#fecha2').datepicker('option', 'minDate', selectedDate);
        }
    })
    $('#fecha2').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        onClose: function(selectedDate) {
			if(!$("#fecha1").validationEngine('validate') && !$("#fecha2").validationEngine('validate'))
            $('#fecha1').datepicker('option', 'maxDate', selectedDate);
        }
    })

    function both_date() {
        if ($('#fecha2').val() != "" || $('#fecha1').val() != "") {
            $('#fecha1').attr("required", true);
            $('#fecha2').attr("required", true);
        } else {
            $('#fecha1').removeAttr("required", false);
            $('#fecha2').removeAttr("required", false);
        }
    }
    
    function check(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8) {
            return true;
        }

        // Patron de entrada, en este caso solo acepta numeros y letras
        patron = /[A-Za-z0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }
	
    $("#departamento_buscar").change(function(){
        $("#ciudad_buscar").empty();
        // Por defecto
        var html = '';
        $("#ciudad_buscar").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
		// Defecto
		html += "<option value=''>TODOS</option>";
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad_buscar").append(html);
        }
        $('#ciudad_buscar').selectpicker('refresh');
    });
</script>