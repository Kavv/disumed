

	<section id="principal">
		<article id="cuerpo">
			<article id="contenido">
				<div id="msj2"></div>
				<form id="form3" class="form-style" name="form" action="facturar.php" method="get">
                    
					<fieldset class="col-md-12">
						<legend class="ui-widget ui-corner-all">Buscar venta a facturar</legend>
							<label for="">Código</label>
							<div class="row">
									<div class="col-md-6">
											<input type="text" name="solid" id="codigo" class='form-control'>
									</div>
									<div class="col-md-3">
											<button class="btn btn-block btn-primary" type='button' onclick="buscar_venta()">FACTURAR</button>
									</div>
									<div class="col-md-3">
											<button class="btn btn-block btn-primary pdf" type='button'>FACTURA ACTUAL</button>
									</div>
							</div>
					</fieldset>
				</form>
			</article>
		</article>
	</section>

	<div id="modal" style="display:none"></div>
	
<script>
	$(document).ready(function(){
		$('#form3').validationEngine({
			onValidationComplete: function(form, status) {
				if (status) {
					return true;
				}
				ocultarCarga();
			}
		});
	});

	function buscar_venta()
	{
			const codigo = $("#codigo").val();
			if (codigo == "") {
				make_alert({'message':'Es necesario que especifique el código', 'type': 'danger'});
				return;
			}
			const ruta = "ajax/contrato.php?contrato="+codigo+"&original=";
			carga();
			$.get(ruta, function(res) {
				if(JSON.parse(res))
				{
					make_alert({'message':'La venta con codigo ' + codigo + ' no existe!', 'type': 'danger'});
					ocultarCarga();
				} else {
					$('#form3').submit();
				}
			});
	}

	$('.pdf').click(function() {
		const codigo = $("#codigo").val();
		if (codigo == "") {
			make_alert({'message':'Es necesario que especifique el código', 'type': 'danger'});
			return;
		}
		$('#modal').html("<iframe src='pdf.php?solid=" + codigo + "' width='100%' height='100%'></iframe>");
		$('#modal').dialog({
			modal: true,
			width: '800',
			height: '900',
			title: 'PDF de la factura'
		});
	});
</script>