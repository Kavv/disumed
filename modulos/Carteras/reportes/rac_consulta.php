
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
        tr td {
            position: relative;
        }
        .f-size-15 {
            font-size: 15px;
        }
        .nota-importante{
            border: #000 solid 2px;
            padding: 10px;
            border-radius: 18px;
            text-align: center;
            background: #00205f;
            color: white;
        }
        .div-fixed-start {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 10px;
        }
        .div-fixed-end {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 50px;
        }
        .excluidos {
            background: #ffe2fd;
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>


	<script type="text/javascript">
        var $ciudades = [];
		$(document).ready(function() {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });
            
            $("input").prop("autocomplete","off"); 

            $("#form-contratos").keypress(function(e) {
                if (e.which == 13) {
                    contratos_a_cobrarse();
                    return false;
                }
            });

            // Almacenar las ciudades
            ruta = 'ciudades.php';
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){

                    var dep = data.ciudepto;
                    var ciu = data.ciuid;
                    var nombre = data.ciunombre;
                    // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                    if($ciudades[dep] != null)
                    {
                        $ciudades[dep].push([ciu, nombre]);
                    }
                    else
                    {
                        // Si es la primera vez se asigna el primer dato como un arreglo
                        $ciudades[dep] = [[ciu, nombre]];
                    }
                });
            });
		});
	</script>
</head>

<body>
	<section id="principal">
		<article id="cuerpo">
			<article id="contenido">
            <div class="row">
                <div class="col-md-6">
                    <form id="form-contratos" class="form-style" name="form" action="#" method="post">
                        <fieldset class="col-md-12">
                            <legend class="ui-widget ui-corner-all">Contratos a cobrarse</legend>
                            <label for="Departamento">Departamento</label>
                            <select name="dep" id="departamento-excel" class="form-control validate[required] selectpicker" data-live-search="true" data-width="100%">
                                <option value=""></option>
                                <?php
                                $departamentos = $db->query("select  * from departamentos order by depnombre;");
                                while ($dep = $departamentos->fetch(PDO::FETCH_ASSOC)) { ?>
                                    <option value="<?php echo $dep['depid'] ?>"><?php echo $dep['depnombre'] ?></option>
                                <?php } ?>
                            </select>
                            <label for="">Ciudades a excluir</label>
                            <div class="input-group mb-3 pl-0">
                                <select id="ciudad-excel" name="ciudad" class="selectpicker col-md-8 px-0" data-live-search="true" title="Selecciona una ciudad">
                                    <option value=""></option>
                                </select>
                                <div class="input-group-append col-md-4 px-0">
                                    <button id="btn-excluir-excel" class="btn btn-block btn-danger" type="button" onclick="excluir_ciudad()">Excluir</button>
                                </div>
                            </div>
                            <div id="ciudades-excluidas-excel">

                            </div>
                            <p>
                                <label for="CobroDesde">Cobro Desde</label>
                                <input id="cobro_desde-excel" name="cd" type="number" class="form-control validate[required]">
                            </p>
                            <p>
                                <label for="CobroHasta">Cobro Hasta</label>
                                <input id="cobro_hasta-excel" name="ch" type="number" class="form-control validate[required]">
                            </p>
                            <button onclick="contratos_a_cobrarse();" type="button" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">
                                Consultar
                            </button>
                        </fieldset>
                    </form>
                </div>
                <div class="col-md-6">
                </div>
            </div>
            </article>
        </article>
    </section>

<script>

    
    function contratos_a_cobrarse()
    {
        var dep = $("#departamento-excel").val();
        var dep_nombre = $("#departamento-excel option:selected").text();
        var cobro_desde = $("#cobro_desde-excel").val();
        var cobro_hasta = $("#cobro_hasta-excel").val();

        var form = $("#form-contratos").validationEngine('validate');


        if(form && dep != "" &&  cobro_desde != "" &&  cobro_hasta != ""  )
        {
            //ruta = "recivos_a_cobrarse_v2.php?dep="+ dep +"&desde="+ desde +"&hasta="+ hasta +"&cd="+ cobro_desde +"&ch="+ cobro_hasta +"&moneda="+ moneda;
            var ruta = "reportes/excel.php?";
            ruta += $("#form-contratos").serialize();
            var pdf_name = "Contrato a cobrarse " + dep_nombre +" cobro desde "+cobro_desde+" hasta "+ cobro_hasta + ".xlsx";

            $("#loading").css('display', 'block');
            //
            var xhr = new XMLHttpRequest();
            xhr.open('GET', ruta , true);
            xhr.responseType = 'blob';

            xhr.onload = function(e) {
            if (this.status == 200) {
                    var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = pdf_name;
                    link.click(); 
                    $("#loading").css('display', 'none');     
                }
                else
                    $("#loading").css('display', 'none');   
                
            };

            xhr.send();
        }
    }
    $("#departamento-excel").change(function(){
        $("#ciudad-excel").empty();
        // Por defecto
        var html = '<option value=""></option>';
        $("#ciudad-excel").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad-excel").append(html);
        }
        $('#ciudad-excel').selectpicker('refresh');
        // Limpiamos las ciudades excluidas del dep anterior
        $("#ciudades-excluidas-excel").empty();
    });
    function excluir_ciudad()
    {
        var codigo = $("#ciudad-excel").val();
        var nombre = $("#ciudad-excel option:selected").text();
        if($("#ciudad-excel-"+codigo).length < 1)
        {
            var html = '<div class="input-group mb-3 pl-0" id="ciudad-excel-' + codigo + '">'+
                '<input type="hidden" value="' + codigo + '" name="ciu-excluidas[]">' +
                '<label class="not-w col-md-8 px-0 mx-0 excluidos">' + nombre + '</label>' +
                '<div class="input-group-append col-md-4 mx-0 px-0">' +
                    '<button class="btn btn-block btn-danger" type="button" onclick="remover_ciudad_excluida(this)">Remover</button>' +
                '</div>' +
            '</div>';
            $("#ciudades-excluidas-excel").append(html);
        }
    }
    var test; 
</script>