<div id="msj"></div>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Aplicar pagos</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Imprimir recibos</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-contratos-tab" data-toggle="pill" href="#pills-contratos" role="tab" aria-controls="pills-contratos" aria-selected="false">Contratos a cobrarse</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">  
    <?php require('recibos_cobrados.php');?>
  </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
    <?php require('reportes/parametros_rac.php');?>
  </div>
  <div class="tab-pane fade" id="pills-contratos" role="tabpanel" aria-labelledby="pills-contratos-tab">
    <?php require('reportes/rac_consulta.php');?>
  </div>
</div>

<?php require($r . 'incluir/src/loading.php'); ?>

<script src="<?php echo $r.'incluir/kavv_js/makemsj.js'?>" ></script>

<?php
if (isset($error)) {?>
    <script>make_alert({'type': 'danger', 'message':  <?php echo $error ?>}});</script>
<?php }
elseif (isset($mensaje)) 
{ ?>
    <script>make_alert({'message': <?php echo $mensaje ?>});</script>
<?php } ?>