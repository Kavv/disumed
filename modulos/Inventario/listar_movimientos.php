<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

$empresa = $_GET['empresa'];
$prefijo = $_GET['prefijo'];
$numero = $_GET['numero'];
$estado = $_GET['estado'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];

$filtro = 'empresa=' . $empresa . '&prefijo=' . $prefijo . '&numero=' . $numero . '&estado=' . $estado . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
$con = 'SELECT * FROM (movinventario INNER JOIN empresas ON movempresa = empid) 
INNER JOIN tipdocumentos ON tipid = movprefijo
LEFT JOIN infoextra ON infoextra.id = movinfo
LEFT JOIN departamentos ON infoextra.departamento = depid
LEFT JOIN ciudades ON (ciuid = infoextra.ciudad AND ciudepto = infoextra.departamento)';
$ord = 'ORDER BY movfecha DESC, movnumero DESC';

$siempreVerdadero = true;

$sql = crearConsulta($con, $ord,
    array($siempreVerdadero, "(tiptipo = 'ENTINV' OR tiptipo = 'SALINV')"),
    array($empresa, "movempresa = '$empresa'  "),
    array($prefijo, "movprefijo = '$prefijo'"),
    array($numero, "movnumero = $numero"),
    array($estado, "movestado = '$estado'"),
    array($fecha1, "movfecha BETWEEN '$fecha1' AND '$fecha2'")
);
$qry = $db->query($sql);

$vista = 'Inventario/lista_movimientos.php';
$titulo = "LISTAR MOVIMIENTOS";
require($r. 'incluir/src/menu_new.php');
?>
