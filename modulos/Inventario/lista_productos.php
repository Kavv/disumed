
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css" />

    
	<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
	<script>
		$(document).ready(function() {
			createdt($("#tabla"),{col:1, buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons});

			$('.confirmar').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el producto?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});
		});
	</script>

	<section id="principal">
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article id="contenido">
				<h2>Listado de productos</h2>
				<div class="reporte">

				</div>
					<table id="tabla" class="table table-hover" style='width:100%;'>
					<div id="msj"></div>
					<thead>
						<tr>
							<th>Codigo</th>
							<th>Nombre</th>
							<th>Descripcion</th>
							<th>Cantidad</th>
							<th>Activo</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA
						?>
							<tr>
								<td><?php echo $row['proid'] ?></td>
								<td><?php echo $row['pronombre'] ?></td>
								<td><?php echo $row['prodescr'] ?></td>
								<td align="center"><?php echo $row['procantidad'] ?></td>
								<td align="center"><?php if ($row['prodesactivado'] == '0') echo '<img src="' . $r . 'imagenes/iconos/accept.png" />';
													else echo '<img src="' . $r . 'imagenes/iconos/cancel_.png" />' ?></td>
								<td align="center"><a href="modificar_producto.php?<?php echo 'id1=' . $row['proid'] . '&' . $filtro ?>"  title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td> <!-- ENVIA POR GET LAS VARIABLES NECESARIAS PARA MODIFICAR UN PRODUCTO -->
								<td align="center"><a href="listar_productos.php?<?php echo 'id1=' . $row['proid'] . '&' . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td> <!-- ENVIA LAS VARIABLES NECESARIAS PARA ELIMINAR UN PRODUCTO JUNTO CON VENTANA MODAL PARA CONFIRMAR -->
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='index.php'">Atras</button> <!-- BOTON PARA VOLVER A CONSULTAR -->
				</p>
			</article>
		</article>
	</section>
	<script src="<?php echo $r.'incluir/kavv_js/makemsj.js'?>" ></script>
