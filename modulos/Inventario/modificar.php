
	<section id="principal">
		<article id="cuerpo">
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar_productos.php?<?php echo $filtro ?>" method="post">
						<!-- FORMULARIO ENVIADO POR POST A LISTAR.PHP JUNTO CON EL FILTRO GENERAL  -->
						<fieldset class="col-md-6">
							<legend class="ui-widget ui-corner-all">Modificar producto</legend>
							<p>
								<label for="id">Codigo:</label> <!-- CAMPO CODIGO -->
								<input type="text" name="id" class="referen" value="<?php echo $row['proid'] ?>" readonly />
							</p>
							<p>
								<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
								<input type="text" name="nombre" class="nombre validate[required] text-input" value="<?php echo $row['pronombre'] ?>" title="Digite el nombre del producto" />
							</p>
							<p>
								<label for="desc">Descripcion:</label> <!-- CAMPO DESCRIPCION -->
								<input type="text" name="desc" value="<?php echo $row['prodescr'] ?>" />
							</p>
							<div class="row">
								<div class="col-4">
									<label for="desactivar" class="not-w" style="width:auto!important;">Desactivar:</label> <!-- CAMPO DESACTIVAR -->
								</div>
								<div class="col-4 text-left">
									<input type="checkbox" class="not-w" name="desactivar" value="1" <?php if ($row['prodesactivado'] == 1) { ?> checked <?php } ?> style="width:25px!important;" />
								</div>
							</div>
							
							<input type="hidden" value="0" name="precio" class="valor validate[required, min[0], custom[onlyNumberSp]] text-input" title="Digite el precio del producto" style="text-align:left!important; padding-left:2.5em;" />
							<input type="hidden" name="categoria" value='-1'>
							<input type="hidden" name="editorial" class="validate[maxSize[40]]" style="text-transform: uppercase" title="Digite la editorial" />
							<input type="hidden" class="validate[required]" name="tipo" value="PT">
							<input type="hidden" name="modificar">
							<p class="boton">
								<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar_productos.php?<?php echo $filtro ?>'">Atras</button>
								<button type="submit" class="btn btn-primary btnmodificar" >Modificar</button>
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
	</section>