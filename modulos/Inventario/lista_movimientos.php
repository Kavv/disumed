<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css" />

    
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
    <script>
        $(document).ready(function() {
        createdt($("#tabla"),{col:1, buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons});
            
            $('.pdf').click(function () {
                newSrc = $(this).attr('data-rel');
                $('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
                $('#modal').dialog({modal: true, width: '1200', height: '600', title: 'PDF de la factura'});
            });
            
        });
    </script>
</head>
<body>
<section id="principal">
    <article id="cuerpo">
        <article id="contenido">
            <h2>Listado de documentos</h2>
            <table id="tabla" style="width:100%;">
                <thead>
                <th>Orden</th>
                <th>Tipo</th>
                <th>Departamento</th>
                <th>Ciudad</th>
                <th>Entregado</th>
                <th style="min-width:135px;">Fecha de orden</th>
                <th>Estado</th>
                <th></th>
                <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                    <tr>
                        <td align="center"><?php echo $row['movnumero'] ?></td>
                        <td align="center"><?php echo $row['movprefijo'] ?></td>
                        <td><?php echo $row['depnombre'] ?></td>
                        <td><?php echo $row['ciunombre'] ?></td>
                        <td><?php echo $row['entregado'] ?></td>
                        <td align="center"><?php echo $row['movfecha'] ?></td>
                        <td><?php echo $row['movestado'] ?></td>

                        <?php
                        if($row['tiptipo'] == 'SALINV')
                        {
                            $ruta_edit = "salida/salida.php?";
                            $ruta_pdf = 'salida/pdf.php?empresa=' . $row['movempresa'] . '&prefijo=' . $row['movprefijo'] . '&numero=' . $row['movnumero'];
                        }
                        else
                        {
                            $ruta_edit = "entrada/entrada.php?";
                            $ruta_pdf = 'entrada/pdf.php?empresa=' . $row['movempresa'] . '&prefijo=' . $row['movprefijo'] . '&numero=' . $row['movnumero'];
                        }

                        if ($row['movestado'] == 'PROCESO') 
                            $ruta_edit .= 'empresa=' . $row['movempresa'] . '&prefijo=' . $row['movprefijo'] . '&numero=' . $row['movnumero'];
                        else
                            $ruta_edit .= 'hide=1&empresa=' . $row['movempresa'] . '&prefijo=' . $row['movprefijo'] . '&numero=' . $row['movnumero'];

                        echo '<td align="center"><a target="_blank" href="' . $ruta_edit . '" title="modificar"><img src="' . $r . 'imagenes/iconos/lapiz.png" class="grayscale" /></a></td>';
                        echo '<td align="center"><img title="Movimiento" src="' . $r . 'imagenes/iconos/pdf.png" class="pdf" data-rel="'.$ruta_pdf.'" /></td>';

                            
                        ?>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <p class="boton">
                <button type="button" class="btn btn-primary btnatras"
                        onClick="carga(); location.href = 'index.php?hash=pills-movimientos-tab'">
                    Atras
                </button>
            </p>
        </article>
    </article>
</section>
<div id="modal" style="display:none"></div>
</body>
</html>