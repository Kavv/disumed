		<article id="cuerpo">
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar_productos.php" method="post">
						<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
						<fieldset class="col-md-6">
							<legend class="ui-widget ui-corner-all">Consultar productos</legend>
							<p>
								<label for="id">Codigo:</label> <!-- CAMPO CODIGO -->
								<input type="text" name="id" class="referen" title="Digite el codigo" />
							</p>
							<p>
								<label for="nombre">Nombre:</label> <!-- CAMPO  NOMBRE -->
								<input type="text" name="nombre" class="nombre" title="Digite el nombre del producto" />
							</p>
							<p>
								<label for="activo">Activo:</label> <!-- CAMPO  NOMBRE -->
								<select class="" name="activo">
									<option value="">TODOS</option>
									<option value="0">Sí</option>
									<option value="1">No</option>
								</select>							
							</p>
							<input type="hidden" name="categoria">
							<input type="hidden" name="editorial">
							<input type="hidden" name="tipo">
							<div class="row">
								<div class="col-md-12 col-lg-12">
									<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">Consultar</button> <!-- BOTON CONSULTAR -->
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
	</section>
