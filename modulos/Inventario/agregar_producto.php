

	<section id="principal">
		<article id="cuerpo">
			<article id="contenido">
				<div class="ui-widget">
					<form id="form1" name="form" action="Controlador/inventarioControlador.php" class="form-style" method="post">
						<!-- FORMULARIO ENVIADO POR POST A SI MISMO -->
						<fieldset class="col-md-6">
							<legend class="ui-widget ui-corner-all">Insertar producto</legend>
							<p>
								<label for="id">Codigo:</label><!-- CAMPO CODIGO -->
								<input type="text" name="id" id="codigo" class="referen text-input" title="Digite el codigo" />
							</p>
							<p>
								<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
								<input type="text" name="nombre" class="uppercase nombre validate[required, maxSize[50]] text-input" title="Digite el nombre del producto" />
							</p>
							<p>
								<label for="desc">Descripcion:</label> <!-- CAMPO DESCRIPCION -->
								<input type="text" name="desc" class="validate[maxSize[100]]" style="text-transform: uppercase" title="Digite descripcion del producto" />
							</p>
								
							<input type="hidden" value="0" name="precio" class="valor validate[required, min[0]] text-input" title="Digite el precio del producto" style="text-align:left!important; padding-left:2.5em;" />
							<input type="hidden" name="categoria" value='-1'>
							<input type="hidden" name="editorial" class="validate[maxSize[40]]" style="text-transform: uppercase" title="Digite la editorial" />
							<input type="hidden" class="validate[required]" name="tipo" value="PT">
							<input type="hidden" name="insertar">
							<p class="boton">
								<button type="button" class="btn btn-block btn-primary btninsertar" onclick="enviarForm()">Insertar</button>
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
	</section>
</body>

</html>
<script>
	$(document).ready(function(){
		$('#form1').validationEngine({
			onValidationComplete: function(form, status) {
				if (status) {
					return true;
				}
				ocultarCarga();
			}
		});
	});

	function enviarForm() {
		const codigo = $("#codigo").val();
		if (codigo == "") {
			make_alert({'message':'Es necesario que especifique el código', 'type': 'danger'});
			return;
		}
		const ruta = "Controlador/inventarioControlador.php?getProduct="+codigo;
		carga();
		$("#msj").empty();
		$.get(ruta, function(res) {
			if(JSON.parse(res))
			{
				make_alert({'message':'El codigo ' + codigo + ' ya se encuentra en uso!', 'type': 'danger'});
				ocultarCarga();
			} else {
				$('#form1').submit();
			}
		});
	}
</script>