
<section id="principal">
    <article id="cuerpo">
        <article id="contenido">
            <form id="form" name="form" action="salida/salida.php" method="post">
                <fieldset class="col-md-6">
                    <legend class="ui-widget ui-corner-all">Tipo de salida</legend>
                    <p>
                        <label for="documento">Tipo de orden:</label>
                        <select id="prefijo" name="prefijo" class="validate[required]">
                            <?php
                            $qry = $db->query("SELECT * FROM tipdocumentos WHERE tiptipo = 'SALINV' ORDER BY tipnombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row['tipid'] . '>' . $row['tipid'] . '/'  . $row['tipnombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <?php
                            $qry = $db->query("SELECT empid FROM empresas ORDER BY empnombre")->fetch(PDO::FETCH_OBJ);
                            echo "<input type='hidden' name='empresa' value='$qry->empid'>";
                        ?>
                    </p>
                    <p class="boton">
                        <button type="submit" class="btn btn-block btn-primary" name="ingresar" value="ingresar">Crear orden</button>
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
</section>