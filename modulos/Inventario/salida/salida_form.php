
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
	
    <style>

        form legend {
            font-weight: bold;
            padding: 5px;
            text-align: center;
        }

        form fieldset {
            padding: 10px;
            display: block;
            margin: 20px auto;
            width: 100% !important;
        }

        .index_producto {
            background: #0087be;
            color: #fff;
            font-weight: bold;
            border-radius: 10px;
            text-align: center!important;
        }
        #list-product{
            height: 35em;
            overflow-x: hidden;
            overflow-y: overlay;
            padding-right: 20px;
        }
    </style>
    <script type="text/javascript">
        function checkCantidad(field, rules, i, options) {
            var cantidad = parseInt(field.val());
            var inventario = parseInt($("#inventario").val());
            if (cantidad > inventario) {
                return 'Esta cantidad es mayor a la del inventario';
            }
        }


        $(document).ready(function () {
            $('#producto').selectpicker();
            input_fecha();
            function input_fecha()
            {
                $(".fecha").datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true,
                });
            }
        

            $(".confirmar").click(function (e) {
                e.preventDefault();
                var targetUrl = $(this).attr("href");
                var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-trash' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea eliminar el producto del documento?</p>").dialog({
                        title: 'Porfavor confirmar',
                        buttons: {
                            "Si": function () {
                                window.location.href = targetUrl;
                            },
                            "No": function () {
                                $(this).dialog("close");
                            }
                        },
                        modal: true,
                        width: 'auto',
                        height: 135
                    }
                );
                $dialog_link_follow_confirm.dialog("open");
            });

            // Almacenar las ciudades
            ruta = "<?php echo $r . 'modulos/Carteras/ciudades.php';?>" ;
            $ciudades = [];
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){

                    var dep = data.ciudepto;
                    var ciu = data.ciuid;
                    var nombre = data.ciunombre;
                    // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                    if($ciudades[dep] != null)
                    {
                        $ciudades[dep].push([ciu, nombre]);
                    }
                    else
                    {
                        // Si es la primera vez se asigna el primer dato como un arreglo
                        $ciudades[dep] = [[ciu, nombre]];
                    }
                });
            });
            
            $('#form').validationEngine({
                showOneMessage: true,
                onValidationComplete: function(form, status) {
                    if (status) {
                        return true;
                    }
                }
            });

            $('#form2').validationEngine({
                showOneMessage: true,
                onValidationComplete: function(form, status) {
                    if (status) {
                        carga();
                        return true;
                    }
                }
            });
        });
    </script>
<section id="principal">
    <article id="cuerpo">
        <article id="contenido"class="row">
            <div class="col-md-7">
                <form id="form" name="form" action="salida.php" method="post" onkeypress="is_enter_key(event);" >
                    <fieldset id="field" class="col-md-12 mx-auto">
                        <legend class="ui-widget ui-corner-all">Saldos iniciales</legend>
                        <div class="row">
                        
                            <div class="col-md-12">
                                <label for="">Movimiento: <strong><?php echo $row['tipnombre'] ?></strong></label>
                                <input id="h-prefijo" type="hidden" name="prefijo" class="form-control documento"
                                    value="<?php echo $prefijo ?>"
                                    readonly/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <label for="">Orden #: <strong><?php echo $numero ?></strong></label>
                                <input id="h-numero" type="hidden" name="numero" class="form-control consecutivo" value="<?php echo $numero ?>" readonly/>
                                <input id="h-empresa" type="hidden" name="empresa"
                                    value="<?php echo $empresa ?>"/>
                            </div>
                            <div class="col-md-6 col-lg-6">

                                <?php if($row['movprefijo'] == "FD"){ ?>
                                    <label for=""><strong>Fecha de Factura </strong></label>
                                    <input id="f-movimiento" type="text" name="fecha" class="fecha validate[custom[date], required]" value="<?php echo $row['movfecha'] ?>" />
                                <?php } else { ?>
                                    <label for="">Fecha <strong><?php echo $row['movfecha'] ?></strong> </label>
                                    <input id="f-movimiento"  type="hidden" name="fecha" class="fecha input-disabled" value="<?php echo $row['movfecha'] ?>" />
                                <?php } ?>
                            </div>
                        </div>
                        <fieldset class="">
                            <legend class="ui-widget ui-corner-all">Datos productos</legend>
                            <div id="msj"></div>
                            <div id="data-productos" style="position:relative;">
                                <div>
                                    <label for="producto">Producto</label>
                                    <select id="producto" name="producto" class="form-control validate[required] text-input" onchange="update_total(); select_product(event);" data-live-search="true" style="left:0">
                                        <option value="">SELECCIONE</option>
                                        <?php
                                        $qry = $db->query("SELECT * FROM productos WHERE procantidad > 0 AND prodesactivado = 0 AND proid NOT IN (SELECT dmoproducto FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero') ORDER BY pronombre");
                                        //echo "SELECT * FROM productos WHERE procantidad > 0 AND prodesactivado = 0 AND proid NOT IN (SELECT dmoproducto FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero') ORDER BY pronombre";
                                        while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                            if ($row2['proestado'] == 'OK')
                                                echo '<option value="' . $row2['proid'] . '">C' . $row2['proid'] . ' / ' . $row2['pronombre'] . '</option>';
                                            else echo '<option value="' . $row2['proid'] . '" style="background-color:RED" disabled>C' . $row2['proid'] . ' / ' . $row2['pronombre'] . '</option>';
                                        }
                                        ?>
                                    </select>

                                    <label for="cantidad">Cantidad en inventario</label>
                                    <input type="text" class="cantidad" disabled id="inventario" name="inventario" value=""/>
                                    <label for="cantidad">Cantidad</label>
                                    <input onkeyup="update_total();" type="text" id="cantidad" name="cantidad" class="cantidad validate[required, custom[onlyNumberSp], funcCall[checkCantidad], min[1]]" value="1" autocomplete='off'/>
                                    
                                    <?php if($row['movprefijo'] != "FD") {?>
                                        <input type="hidden" id="vunitario" name="vunitario" class="valor validate[required, custom[onlyNumberSp]]" value="0"/>
                                        <input type="hidden" id="tunitario" name="tunitario" class="form-control not-w valor validate[required, custom[onlyNumberSp]]"/>
                                        <button type="button" class="btn btn-block btn-success" name="insproducto" value="insproducto" onclick="add_product();">
                                            AGREGAR
                                        </button>
                                    <?php } else {?>
                                        <label for="vunitario">Valor unitario</label>
                                        <input onkeyup="update_total();" type="text" id="vunitario" name="vunitario" class="valor validate[required, custom[onlyNumberSp]]" value="0" autocomplete='off'/>
                                        <label for="tunitario">Sub total</label>
                                        <div class="input-group">
                                            <input type="text" id="tunitario" name="tunitario" class="form-control not-w valor validate[required, custom[onlyNumberSp]]" readonly/>
                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-success" name="insproducto" value="insproducto" onclick="add_product();">
                                                    <i class="fas fa-plus-square"></i>
                                                </button>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div id="list-product">
                                    <label for="" class="col-md-12" style="text-align:center!important;">
                                        <strong>Productos Agregados</strong>
                                    </label>
                                    <?php if($row['movprefijo'] == "FD") {?>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text font-weight-bold">Total de la salida</label>
                                            </div>
                                            <input type="text" class="valor form-control not-w mt-1" value="<?php echo $row['movvalor'] ?>" readonly /></td>
                                        </div>
                                    <?php } 
                                    
                                    $qry = $db->query("SELECT * FROM detmovimientos INNER JOIN productos ON dmoproducto = proid WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero'");
                                    $num = $qry->rowCount();
                                    $hide_input = $num;
                                    $index_producto = 0;
                                    while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    $index_producto++; ?>
                                    
                                        <p>
                                            <label class="index_producto default">#<?php echo $index_producto; ?></label>
                                        </p>
                                        <p align="center">
                                            <label>  <?php echo $row2['proid'] . " / " . $row2['pronombre'] ?> </label>
                                            
                                            <?php if($row['movprefijo'] == "FD") {?>
                                                <div class="row">
                                                    <div class="input-group mb-1 col-md-6">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Cantidad</span>
                                                        </div>
                                                        <input type="text" class="form-control not-w cantidad edit-cantidad"
                                                            value="<?php echo $row2['dmocantidad'] ?>"
                                                            />
                                                    </div>
                                                    <div class="input-group mb-1 col-md-6">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Valor unitario</span>
                                                        </div>
                                                        <input type="text" class="form-control not-w cantidad  edit-vunitario"
                                                            value="<?php echo $row2['dmounitario'] ?>"
                                                            />
                                                    </div>
                                                    <div class="input-group mb-1 col-md-12">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Sub total</span>
                                                        </div>
                                                        <input type="text" class="form-control not-w cantidad"
                                                            value="<?php echo $row2['dmototal'] ?>"
                                                            readonly/>
                                                        <div class="input-group-append">
                                                            <div class="">
                                                                <button type="button" class="btn btn-block btn-warning"
                                                                    onclick="edit_product_FD('<?php echo $row2['dmoproducto']; ?>', this);"  >
                                                                    <i class="fas fa-pencil-alt"></i>
                                                                </button>
                                                            </div>
                                                            <div class="">
                                                                <button type="button" class="btn btn-block btn-danger"
                                                                    onclick="remove_product('<?php echo $row2['dmoproducto']; ?>');"  >
                                                                    <i class="fas fa-trash"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="row">
                                                    <div class="input-group mb-1 col-md-6">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Cantidad</span>
                                                        </div>
                                                        <input type="text" class="form-control not-w cantidad edit-cantidad"
                                                            value="<?php echo $row2['dmocantidad'] ?>"/>
                                                    </div>
                                                    
                                                    <div class="col-md-2">
                                                        <button type="button" class="btn btn-block btn-warning"
                                                            onclick="edit_product('<?php echo $row2['dmoproducto']; ?>', this);"  >
                                                            <i class="fas fa-pencil-alt"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button type="button" class="btn btn-block btn-danger"
                                                            onclick="remove_product('<?php echo $row2['dmoproducto']; ?>');"  >
                                                            <i class="fas fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </div>

                                            <?php } ?>

                                        </p>
                                    <?php } ?>
                                
                                </div>
                                
                            </div>
                        </fieldset>
                    </fieldset>
                </form>
            </div>
            <div class="col-md-5">
                <form id="form2" name="form2" action="finalizar.php" method="post">
                    <fieldset class="col-md-12 fieldset-espejo">
                        <legend class="ui-widget ui-corner-all">Datos Generales</legend>
                        
                        <input type="hidden" name="empresa" value="<?php echo $empresa ?>" />
                        <input type="hidden" name="prefijo" value="<?php echo $prefijo ?>" />
                        <input type="hidden" name="numero" value="<?php echo $numero ?>" />
                        <input type="hidden" name="infoextra" value="<?php echo $id_info ?>" />
                        <input id="fecha-factura" type="hidden" name="fecha" value="" />
                        <p style="position: relative;">
                            <label for=""><span style="color:red">*</span>Departamento</label>
                            <select name="departamento" id="departamento" class="form-control validate[required] selectpicker">
                            <option value=""></option>
                            <?php 
                                $departamentos = $db->query("SELECT * FROM departamentos ORDER BY depnombre ASC");
                                while ($d = $departamentos->fetch(PDO::FETCH_ASSOC)) {
                                    if($infoextra != "")
                                    {
                                        if($infoextra['departamento'] == $d['depid'])
                                            echo "<option selected value='" . $d['depid'] . "'>" . $d['depnombre'] . "</option>";
                                        else
                                            echo "<option value='" . $d['depid'] . "'>" . $d['depnombre'] . "</option>";
                                    }
                                    else
                                        echo "<option value='" . $d['depid'] . "'>" . $d['depnombre'] . "</option>";
                                }
                            ?>
                            </select>
                            <label for=""><span style="color:red">*</span>Ciudad</label>
                            <select name="ciudad" id="ciudad" class="form-control validate[required]">
                                <?php 
                                    if($infoextra != "")
                                    {
                                        echo "<option selected value='" . $infoextra['ciudad'] . "'>" . $infoextra['ciunombre'] . "</option>";
                                    }
                                ?>
                            </select>
                            <?php if($row['movprefijo'] == "FD") {?>
                                <label for=""><span style="color:red">*</span>Distribuidor</label>
                                <input type="text" class="form-control validate[required, maxSize[50]] uppercase" value="<?php echo $distribuidor?>" name="distribuidor">
                                <label for="">Moneda</label>
                                <select name="simbolo" id="simbolo" class="form-control validate['required']">
                                    <?php 
                                        $monedas = $db->query("SELECT * FROM monedas ORDER BY nombre ASC");
                                        while ($moneda = $monedas->fetch(PDO::FETCH_ASSOC)) {
                                            $moneda_distribuidor = "es-NI";
                                            if($moneda['currency'] == $moneda_distribuidor)
                                            echo "<option selected value='" . $moneda['simbolo'] . "'>" . $moneda['nombre']. "(" . $moneda['simbolo'].")</option>";
                                            else
                                            echo "<option value='" . $moneda['simbolo'] . "'>" . $moneda['nombre']. "(" . $moneda['simbolo'] . ")</option>";

                                        }
                                    ?>
                                </select>
                                <label for="">Dirección</label>
                                <textarea type="text" class="form-control validate[maxSize[200] uppercase" name="direccion"><?php echo $direccion?></textarea>
                            <?php } else {?>
                                <input type="hidden" class="" value="" name="distribuidor">
                                <input type="hidden" class="" value="<?php echo $simbolo_defecto; ?>" name="simbolo">
                                <input type="hidden" class="" value="" name="direccion">
                            <?php } ?>

                            <label for="">Entregado</label>
                            <input type="text" class="form-control validate[maxSize[40]] uppercase" value="<?php echo $entregado?>" name="entregado">
                            <label for="">Bodeguero</label>
                            <input type="text" class="form-control validate[maxSize[40]] uppercase" value="<?php echo $bodeguero?>" name="bodeguero">
                            <label for="">Telefono 1</label>
                            <input type="text" class="form-control validate[maxSize[15]]" value="<?php echo $telefono1?>" name="telefono1">
                            <label for="">Telefono 2</label>
                            <input type="text" class="form-control validate[maxSize[15]]" value="<?php echo $telefono2?>" name="telefono2">
                        </p>
                        <p style="position:relative;">
                            <label>Comentario:</label> 
                            <textarea name="texto" style="text-transform: uppercase" cols="100" rows="3" class="form-control validate[maxSize[400]]"><?php echo $comentario ?></textarea>
                        </p>
                                        
                        <div class="row d-flex justify-content-center">
                            <p class="boton mt-1"> 
                                <?php 
                                    $text_button = "Finalizar";
                                    if($hide != 0)
                                        $text_button = "Actualizar"; 

                                    if($num == 0)
                                    {
                                        $style_finalizar = "style='display:none;' ";
                                        $style_cancelar = "style='display:block;' ";
                                    }
                                    else
                                    {
                                        $style_cancelar = "style='display:none;' ";
                                        $style_finalizar = "style='display:block;' ";
                                    }
                                    
                                    echo "
                                        <button id='btn-cancelar' type='button' class='btn btn-block btn-danger' $style_cancelar".
                                        "onClick=\"carga(); location.href='cancelar.php?empresa=$empresa&prefijo=$prefijo&numero=$numero' \">".
                                            "Cancelar".
                                        "</button>".
                                        "<button  id='btn-finalizar' $style_finalizar type='button' class='btn btn-block btn-primary' name='finalizar' value='finalizar' onclick='finalizar_proceso()'>$text_button</button>";


                                ?>
                            </p>
                        </div>
                    </fieldset>
                    
                </form>
            </div>
        </article>
    </article>
</section>
<script>
    function update_total()
    {
        var tunitario = $('#cantidad').val() * $('#vunitario').val();

        if (tunitario == null || isNaN(tunitario)) {
            tunitario = 0;
        }

        $('#tunitario').val(tunitario.toFixed(2));
    }
    function add_product()
    {
        var vp = $("#producto").validationEngine('validate');
        var vc = $("#cantidad").validationEngine('validate');
        var vu = $("#vunitario").validationEngine('validate');
        var vt = $("#tunitario").validationEngine('validate');
        ocultarCarga();
        if(!vp && !vc && !vu && !vt)
        {
            var url = "../template/salida_add_remove.php?hide="+ <?php echo $hide; ?>;
            carga();
            $.ajax({
                type: "POST",
                url: url,
                data: $("#form").serialize(),
                success:function(res){ 
                    $("#data-productos").empty();
                    $("#data-productos").append(res);
                    $("#producto").selectpicker("refresh");

                    ocultarCarga()
                    $("#btn-finalizar").css('display', 'inline-block');
                    $("#btn-cancelar").css('display', 'none');
                    
                }, fail: function(){
                    ocultarCarga()
                    var item_cant = $(".product-item").length;
                    if(item_cant < 1)
                    {
                        $("#btn-finalizar").css('display', 'none');
                        $("#btn-cancelar").css('display', 'block');
                    }
                }
            });
        }
    }

    function remove_product(producto)
    {
        var empresa = $("#h-empresa").val();
        var prefijo = $("#h-prefijo").val();
        var numero = $("#h-numero").val();
        var url = "../template/salida_add_remove.php?empresa="+empresa+"&prefijo="+prefijo+"&numero="+numero+"&producto="+producto;
        carga();
        $.get(url, function(res){
            $("#data-productos").empty();
            $("#data-productos").append(res);
            $("#producto").selectpicker("refresh");
            ocultarCarga()
            var item_cant = $(".product-item").length;
            if(item_cant < 1)
            {
                $("#btn-finalizar").css('display', 'none');
                $("#btn-cancelar").css('display', 'block');
            }
        });

    }

    function finalizar_proceso()
    {
        if(!$("#f-movimiento").validationEngine('validate'))
        {
            var fecha_factura = $("#f-movimiento").val();
            $("#fecha-factura").val(fecha_factura);
            $("#form2").submit();
        }
    }
    function is_enter_key(e)
    {
        if (e.which == 13) {
            add_product();
            return false;
        }
    }
    function select_product(event){
        carga();
        $.getJSON("<?php echo $r ?>incluir/carga/costo.php", {"id": $("#producto").find(':selected').val()}, datos);

        function datos(data) {
            $("#vunitario").val(parseFloat(data.costo).toFixed(2));
            $("#inventario").val(data.inventario);
            ocultarCarga();
        }
    }
    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
    });

    
    function edit_product(producto, element)
    {

        var new_cantidad = $(element).parent().parent().find(".edit-cantidad").val()

        var empresa = $("#h-empresa").val();
        var prefijo = $("#h-prefijo").val();
        var numero = $("#h-numero").val();
        var url = "../template/salida_editar_p.php?empresa="+empresa+"&prefijo="+prefijo+"&numero="+numero+"&producto="+producto+"&cantidad="+new_cantidad;
        carga();

        $.ajax({
            type: "GET",
            url: url,
            success:function(res){ 
                res = JSON.parse(res);
                if(res.codigo == 0)
                    make_alert({'object': $("#msj"), 'type':'info', 'message':res.msj});
                else
                    make_alert({'object': $("#msj"), 'type':'danger', 'message':res.msj});

                ocultarCarga()
            }, fail: function(){
                make_alert({'object': $("#msj"), 'type':'danger', 'message':'NO se edito la cantidad del producto, intentelo nuevamente o solicite soporte tecnico'});
                ocultarCarga()
            }
        });

    }
    var  prueba;
    function edit_product_FD(producto, element)
    {
        prueba = element;
        var fila_editar = $(element).parents(".row").eq(0);
        var new_cantidad = fila_editar.find(".edit-cantidad").val();
        var new_vunitario = fila_editar.find(".edit-vunitario").val();

        var empresa = $("#h-empresa").val();
        var prefijo = $("#h-prefijo").val();
        var numero = $("#h-numero").val();
        var url = "../template/salida_editar_FD.php?empresa="+empresa+"&prefijo="+prefijo+"&numero="+numero+"&producto="+producto+"&cantidad="+new_cantidad+"&vunitario="+new_vunitario;
        carga();

        $.ajax({
            type: "GET",
            url: url,
            success:function(res){ 
                res = JSON.parse(res);
                if(res.code == 0)
                    make_alert({'object': $("#msj"), 'type':'info', 'message':res.msj});
                else
                    make_alert({'object': $("#msj"), 'type':'info', 'message':res.msj});

                ocultarCarga()
            }, fail: function(){
                make_alert({'object': $("#msj"), 'type':'danger', 'message':'NO se edito la cantidad del producto, intentelo nuevamente o solicite soporte tecnico'});
                ocultarCarga()
            }
        });

    }
    function make_alert({object = $('#msj'), type = 'success', message = ''})
    {
        object.slideUp();
        object.empty();

        var alerta = 
        '<div class="alert alert-'+ type +' alert-dismissible fade show" role="alert">'+
            message +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                '<span aria-hidden="true">&times;</span>'+
            '</button>'+
        '</div>';

        object.append(alerta);
        object.slideDown();
    }

</script>