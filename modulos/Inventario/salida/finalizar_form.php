
    <style>
    @media print {
        iframe {
            width: 100%;
            height: 100%;
        }
    }
    </style>
    
<section id="principal">
    <article id="cuerpo">
        <article id="contenido">
            <p align="center">
                <iframe src="<?php echo $pdf . '.php?empresa='. $empresa . '&prefijo=' . $prefijo . '&numero=' . $numero . '&simbolo=' . $simbolo ?>"
                        width="800" height="550"></iframe>
            </p>
            <p class="text-center">
                <a class="btn btn-warning ml-1" href="<?php echo "salida.php?hide=1&empresa=".$empresa."&prefijo=".$prefijo."&numero=".$numero; ?>">
                    Editar este movimiento
                </a>
                <button type="button" class="btn btn-primary btnatras"
                        onClick="carga(); location.href = '../index.php?hash=pills-salida-tab'">
                    Agregar nuevo movimiento de salida
                </button>
            </p>
        </article>
    </article>
</section>