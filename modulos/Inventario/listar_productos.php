<?php 
$r = '../../';

require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
if (isset($_POST['modificar'])) { //VALIDAMOS SI POST MODIFICAR ES TRUE
	$id = $_POST['id'];
	$nombre = strtoupper(trim($_POST['nombre']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$num = $db->query("SELECT * FROM productos WHERE proid <> '$id' AND pronombre = '$nombre'")->rowCount(); // REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
	if ($num < 1) { // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		$desc = trim(strtoupper($_POST['desc']));
		$precio = $_POST['precio'];
		$tipo = $_POST['tipo'];
		$desactivar = 0;
		$categoria = $_POST['categoria'];
		$editorial = $_POST['editorial'];
		if(isset($_POST['desactivar']))
			$desactivar = $_POST['desactivar'];
		$qry = $db->query("UPDATE productos SET 
		pronombre = '$nombre', 
		prodescr = '$desc', 
		proprecio = $precio, 
		protipo = '$tipo', 
		prodesactivado = '$desactivar', 
		categoria = $categoria,
		editorial = '$editorial'		
		WHERE proid = '$id'"); // ACTUALIZAMOS LA INFORMACION CON LOS NUEVOS DATOS RECIBIDOS
		if ($qry) $mensaje = 'Se actualizo el producto';  // MENSAJE EXITOSO
		else $error = 'No se actualizo el producto'; // MENSAJE ERROR
	} else $error = 'El nombre del producto ya existe';
	$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre']. '&categoria=' . $_GET['id']. '&editorial=' . $_GET['editorial']. '&tipo=' . $_GET['tipo']. '&activo=' . $_GET['activo']; // FILTRO GENERAL PARA ENVIAR POR GET
	if(isset($mensaje))
		$url = 'listar_productos.php?mensaje='.$mensaje.'&'.$filtro;
	else
		$url = 'listar_productos.php?error='.$error.'&'.$filtro;
	header("location:$url");
    exit();

}
// Eliminar 
if (isset($_GET['id1'])) { //VALIDAMOS SI GET ID1 ES TRUE 
	$id1 = $_GET['id1'];
	$num = $db->query("SELECT * FROM detsolicitudes WHERE detproducto = '$id1'")->rowCount();
	if ($num < 1) { // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		$num = $db->query("SELECT * FROM detmovimientos WHERE dmoproducto = '$id1'")->rowCount();
		if ($num < 1) {
			$qry = $db->query("DELETE FROM productos WHERE proid = '$id1'"); //ELIMINAMOS EL PRODUCTO
			if ($qry) $mensaje = 'Se elimino el producto';
			else $error = 'No se pudo eliminar el producto';
		} else $error = 'El producto tiene movimiento';
	} else $error = 'El producto esta asociado a solicitudes';
	
	$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre']. '&categoria=' . $_GET['id']. '&editorial=' . $_GET['editorial']. '&tipo=' . $_GET['tipo']. '&activo=' . $_GET['activo']; // FILTRO GENERAL PARA ENVIAR POR GET
	if(isset($mensaje))
		$url = 'listar_productos.php?mensaje='.$mensaje.'&'.$filtro;
	else
		$url = 'listar_productos.php?error='.$error.'&'.$filtro;
	header("location:$url");
    exit();
}
$id = $nombre = "";
if (isset($_POST['consultar'])) { //VALIDAMOS SI POST CONSULTAR ES TRUE
	$id = $_POST['id'];
	$nombre = strtoupper(trim($_POST['nombre']));
	$categoria = $_POST['categoria'];
	$editorial = strtoupper(trim($_POST['editorial']));
	$tipo = $_POST['tipo'];
	$activo = $_POST['activo'];
} else if (isset($_GET['id'])) { // SI LO RECIBIDO POR POST CONSULTAR NO ES TRUE
	$id = $_GET['id'];
	$nombre = strtoupper(trim($_GET['nombre']));
	$categoria = $_GET['categoria'];
	$editorial = strtoupper(trim($_GET['editorial']));
	$tipo = $_GET['tipo'];
	$activo = $_GET['activo'];
}
$filtro = 'id=' . $id . '&nombre=' . $nombre. '&categoria=' . $categoria. '&editorial=' . $editorial. '&tipo=' . $tipo. '&activo=' . $activo; // FILTRO GENERAL PARA ENVIAR POR GET

$con = "SELECT *
	FROM productos
	LEFT JOIN categorias ON productos.categoria = categorias.id";
$ord = 'ORDER BY pronombre ASC';
$sql = crearConsulta($con, $ord,
	array($id, "proid = '$id'"),
	array($nombre, "pronombre like '%$nombre%'"),
	array($categoria, "categoria = '$categoria'  "),
	array($editorial, "editorial = '$editorial'"),
	array($tipo, "protipo = '$tipo'"),
	array($activo, "prodesactivado = '$activo'"),
);

$qry = $db->query($sql);


$vista = 'Inventario/lista_productos.php';
$titulo = "LISTAR INVENTARIO";
require($r. 'incluir/src/menu_new.php');
?>