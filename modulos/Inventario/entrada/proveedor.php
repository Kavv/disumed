<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>

<p>
    <label for="tercero">Proveedor:</label>
    <select id="tercero" data-live-search="true" name="tercero" class="form-control validate[required]">
        <option value="">SELECCIONE</option>
        <?php
        $qry = $db->query("SELECT * FROM proveedores ORDER BY pvdnombre");
        while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
            echo '<option value=' . $row['pvdid'] . '>' . $row['pvdnombre'] . '</option>';
        }
        ?>
    </select>
</p>
<p>
    <label for="documento_prov">Documento del proveedor: </label><input type="text" name="documento_prov"
                                                                   class="consecutivo validate[required]"
                                                                   style="text-transform:uppercase"/>
</p>
