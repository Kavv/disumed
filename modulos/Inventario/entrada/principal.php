

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#empresa, #documento').change(function (event) {
                var id1 = $('#documento').val();
                if (id1 == 'FC') {
                    carga();
                    $('#adicional').load('proveedor.php', function () {
                        $("#tercero").selectpicker();
                        $("#cargando").hide()
                    });
                } else {
                    $('#adicional').load('blank.php');
                }

            });
        });
    </script>
<section id="principal">
    <article id="cuerpo">
        <article id="contenido">
            <form id="form" name="form" action="entrada/entrada.php" method="post">
                <fieldset class="col-md-6">
                    <legend class="ui-widget ui-corner-all">Tipo de entrada</legend>
                    <p>
                        <label for="documento">Documento:</label>
                        <select id="documento" name="prefijo" class="validate[required]">
                            <?php
                            $qry = $db->query("SELECT * FROM tipdocumentos WHERE tiptipo = 'ENTINV' ORDER BY tipnombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row['tipid'] . '>' .$row['tipid'] . '/' . $row['tipnombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <?php
                            $qry = $db->query("SELECT empid FROM empresas ORDER BY empnombre")->fetch(PDO::FETCH_OBJ);
                            echo "<input type='hidden' name='empresa' value='$qry->empid'>";
                        ?>
                    </p>
                    <p id="adicional"></p>
                    <p class="boton">
                        <button type="submit" class="btn btn-block btn-primary mt-1" name="ingresar" value="ingresar">Crear
                        </button>
                    </p>


                </fieldset>
            </form>
        </article>
    </article>
</section>
</body>
</html>