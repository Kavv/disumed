<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$hide = 0;
if (isset($_POST['ingresar'])) {

    $empresa = $_POST['empresa'];
    $prefijo = $_POST['prefijo'];
    // Obtenemos el # de movimiento consecuente en base al prefijo
    $rowmax = $db->query("SELECT MAX(movnumero)+1 AS ultimo FROM movinventario WHERE movempresa = '$empresa' AND movprefijo = '$prefijo'")->fetch(PDO::FETCH_ASSOC);
    if($rowmax['ultimo'] == '')
        $numero = 1;
    else
        $numero = $rowmax['ultimo'];

    $documento = isset($_POST['documento_prov']) ? $_POST['documento_prov'] : '';
    $tercero = isset($_POST['tercero']) ? $_POST['tercero'] : '';

    $num = $db->query("SELECT * FROM movinventario
    INNER JOIN tipdocumentos ON tipid = movprefijo
    WHERE (tiptipo = 'ENTINV' OR tiptipo = 'SALINV')
    AND movestado = 'PROCESO' AND tipid = '$prefijo';")->rowCount();

    if ($num > 0) {
        $error = 'Hay un documento que no se ha finalizado, debe finalizarlo o cancelarlo para proceder';
        header('Location:../index.php?hash=pills-entrada-tab&error=' . $error);
        exit();
    }
    if ($tercero == '') {
        $qry = $db->query("INSERT INTO movinventario(movempresa, movprefijo, movnumero, movfecha, movestado) VALUES ('$empresa', '$prefijo', '$numero', NOW(), 'PROCESO')");
    } else {
        $qry = $db->query("INSERT INTO movinventario(movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movestado) VALUES ('$empresa', '$prefijo', $numero, '$tercero', '$documento', NOW(), 'PROCESO')");
    }

    $url = "empresa=$empresa&prefijo=$prefijo&numero=$numero";

    header('Location:entrada.php?' . $url);
} else {
    $empresa = $_GET['empresa'];
    $prefijo = $_GET['prefijo'];
    $numero = $_GET['numero'];
    if(isset($_GET['hide']))
        $hide = $_GET['hide'];
    
}

$row = $db->query("SELECT movinventario.*, empresas.*, tipnombre FROM movinventario 
LEFT JOIN empresas ON movempresa = empid 
LEFT JOIN tipdocumentos ON tipid = '$prefijo'
WHERE movempresa = '$empresa' 
AND movprefijo = '$prefijo' 
AND movnumero = $numero")->fetch(PDO::FETCH_ASSOC);
$infoextra = $entregado = $bodeguero = $comentario = $entregado = $telefono1 = $telefono2 = "";
if($row)
{
    $id_info = $row['movinfo'];
    if($id_info != "")
    {
        $infoextra = $db->query("SELECT infoextra.*, ciunombre FROM infoextra INNER JOIN ciudades ON (ciuid = ciudad AND ciudepto = departamento) WHERE id = '$id_info'")->fetch(PDO::FETCH_ASSOC);
        $entregado = $infoextra['entregado'];
        $bodeguero = $infoextra['bodeguero'];
        $comentario = $infoextra['comentario'];
        $telefono1 = $infoextra['telefono1'];
        $telefono2 = $infoextra['telefono2'];
        $id_info = $infoextra['id'];
    }

}
$hide_input = "";

$vista = 'Inventario/entrada/entrada_form.php';
$titulo = "AGREGAR ENTRADA";
require($r. 'incluir/src/menu_new.php');

?>