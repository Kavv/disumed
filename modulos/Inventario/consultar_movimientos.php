
    <script type="text/javascript">
        $(document).ready(function () {
            $('#form').validationEngine({
                showOneMessage: true,
                onValidationComplete: function (form, status) {
                    if (status) {
                        carga();
                        return true;
                    }
                }
            });
            $('#fecha1').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                onClose: function (selectedDate) {
                    $('#fecha2').datepicker('option', 'minDate', selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });
            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                onClose: function (selectedDate) {
                    $('#fecha1').datepicker('option', 'maxDate', selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });
            $('.btnconsulta').button({icons: {primary: 'ui-icon ui-icon-search'}});
            $("#dialog-message").dialog({
                height: 'auto',
                width: 'auto',
                modal: true
            });
        });
    </script>
<section id="principal">
    <article id="cuerpo">
        <article id="contenido">
            <form id="form" name="form" action="listar_movimientos.php" method="GET">
                <fieldset class="col-md-6">
                    <legend class="ui-widget ui-corner-all">Consulta de Ordenes</legend>
                    <p>
                        <?php
                            $qry = $db->query("SELECT empid FROM empresas ORDER BY empnombre")->fetch(PDO::FETCH_OBJ);
                            echo "<input type='hidden' name='empresa' value='$qry->empid'>";
                        ?>
                    </p>
                    <p>
                        <label for="prefijo">Documento:</label>
                        <select id="prefijo" name="prefijo">
                            <option value="">TODOS</option>
                            <?php
                            $qry = $db->query("SELECT * FROM tipdocumentos WHERE tiptipo = 'ENTINV' OR tiptipo = 'SALINV' ORDER BY tipnombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row['tipid'] . '>' . $row['tipnombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="numero">Numero de orden:</label>
                        <input type="text" name="numero" class="consecutivo"/>
                    </p>
                    <p>
                        <label for="estado">Estado:</label>
                        <select name="estado">
                            <option value="">TODOS</option>
                            <option value="PROCESO">PROCESO</option>
                            <option value="FINALIZADO">FINALIZADO</option>
                        </select>
                    </p>
                    <p>
                        <label for="fecha">Fechas:</label>
                        <input type="text" id="fecha1" name="fecha1" class="fecha"/> <input type="text" id="fecha2"
                                                                                            name="fecha2"
                                                                                            class="fecha"/>
                    </p>
                    <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar"
                            value="Buscar">consultar
                    </button>
                </fieldset>
            </form>
        </article>
    </article>
</section>