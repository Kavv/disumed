<div id="msj"></div>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Agregar productos</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Buscar producto</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-entrada-tab" data-toggle="pill" href="#pills-entrada" role="tab" aria-controls="pills-entrada" aria-selected="false">Movimiento de entrada</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-salida-tab" data-toggle="pill" href="#pills-salida" role="tab" aria-controls="pills-salida" aria-selected="false">Movimiento de salida</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-movimientos-tab" data-toggle="pill" href="#pills-movimientos" role="tab" aria-controls="pills-movimientos" aria-selected="false">Buscar movimientos</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">  
    <?php require('agregar_producto.php');?>
  </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
    <?php require('buscar_productos.php');?>
  </div>
  <div class="tab-pane fade" id="pills-entrada" role="tabpanel" aria-labelledby="pills-entrada-tab">
    <?php require('entrada/principal.php');?>
  </div>
  <div class="tab-pane fade" id="pills-salida" role="tabpanel" aria-labelledby="pills-salida-tab">
    <?php require('salida/principal.php');?>
  </div>
  <div class="tab-pane fade" id="pills-movimientos" role="tabpanel" aria-labelledby="pills-movimientos-tab">
    <?php require('consultar_movimientos.php');?>
  </div>
</div>

<?php require($r . 'incluir/src/loading.php'); ?>
<script src="<?php echo $r.'incluir/kavv_js/makemsj.js'?>" ></script>

<?php
if (isset($error)) {?>
    <script>make_alert({'type': 'danger', 'message': <?php echo $error ?>});</script>
<?php }
elseif (isset($mensaje)) 
{ ?>
    <script>make_alert({'message': <?php echo $mensaje ?>});</script>
<?php } ?>

<script>
  let hash = "<?php if(isset($_GET['hash'])){echo $_GET['hash'];} else {echo "";}?>";
  if(hash == "")
    $("#pills-tab .nav-link").eq(0).click();
  else
    $("#"+hash).click();
</script>