
    function make_alert({object = $('#msj'), type = 'success', message = ''})
    {
        object.slideUp();
        object.empty();

        var alerta = 
        '<div class="alert alert-'+ type +' alert-dismissible fade show" role="alert">'+
            message +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                '<span aria-hidden="true">&times;</span>'+
            '</button>'+
        '</div>';

        object.append(alerta);
        object.slideDown();
    }

    function notificacion({object = $('#msj'), error = '', exito = ''})
    {
        if (!error && !exito) {
            return;
        }
        object.slideUp();
        object.empty();

        tipo = error ? 'danger' : 'success';
        console.log('x');
        message = error ? error : exito;
        var alerta = 
        '<div class="alert alert-'+ tipo +' alert-dismissible fade show" role="alert">'+
            message +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                '<span aria-hidden="true">&times;</span>'+
            '</button>'+
        '</div>';

        object.append(alerta);
        object.slideDown();
    }