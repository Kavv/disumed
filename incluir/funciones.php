<?php

/**
 * Genera una consulta sql según los parametros dados
 *
 * @param $sql Consulta sql
 * @param $ord Consulta order by
 * @param mixed ...$condiciones conjunto de elementos tipo array el primero debe contener la variable que no debe ser empty y la segunda la condicion que se concatenara con la consulta
 * @return string Consutla sql
 * @example crearConsulta("select * from Usuarios","order by nombre", array($apellido,"apellido = '". $apellido."'"))
 *
 */
function crearConsulta($sql, $ord, ...$condiciones)
{
    $bandera = 0;

    foreach ($condiciones as $item) {
        if (!empty($item[0])) {

            $sql .= " " . ($bandera == 0 ? 'where ' : "and ") . $item[1];

            $bandera++;
        }
    }
    return $sql . " " . $ord;
}

function delDir($dir)
{
    foreach (glob($dir . "/*") as $archive_dir) {
        if (is_dir($archive_dir)) {
            delDir($archive_dir);
        } else {
            unlink($archive_dir);
        }
    }
    if (rmdir($dir)) return true;
    else return false;
}

function zerofill($entero, $largo)
{
    $entero = (int)$entero;
    $largo = (int)$largo;
    $relleno = '';
    if (strlen($entero) < $largo)
        $relleno = str_repeat('0', $largo - strlen($entero));
    return $relleno . $entero;
}

function fechaDif($start, $end)
{
    $start_ts = strtotime($start);
    $end_ts = strtotime($end);
    $diff = $end_ts - $start_ts;
    return round($diff / 86400);
}

function izqrell($cadena, $largo, $reemplazo)
{
    $relleno = '';
    if (strlen($cadena) < $largo)
        $relleno = str_repeat($reemplazo, $largo - strlen($cadena));
    $final = substr($relleno . $cadena, 0, $largo);
    return $final;
}

function derrell($cadena, $largo, $reemplazo)
{
    $relleno = '';
    if (strlen($cadena) < $largo)
        $relleno = str_repeat($reemplazo, $largo - strlen($cadena));
    $final = substr($cadena . $relleno, 0, $largo);
    return $final;
}

function array_envia($array)
{
    $tmp = serialize($array);
    $tmp = urlencode($tmp);
    return $tmp;
}

function array_recibe($url_array)
{
    $tmp = stripslashes($url_array);
    $tmp = urldecode($tmp);
    $tmp = unserialize($tmp);
    return $tmp;
}

function noCache()
{
    header("Expires: Tue, 01 Jul 2001 06:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
}

function caracteres_especiales($s)
{
    $s = str_replace("ñ", "N", $s);
    $s = str_replace("Ñ", "N", $s);
    return $s;
}

/**
 * Obtiene el nombre de la columna de Excel según el número dado
 *
 * @link https://stackoverflow.com/questions/3302857/algorithm-to-get-the-excel-like-column-name-of-a-number
 * @param $num
 * @return string
 */
function obtenerNombreDeColumnaExcel($num)
{
    $numeric = $num % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval($num / 26);
    if ($num2 > 0) {
        return obtenerNombreDeColumnaExcel($num2 - 1) . $letter;
    } else {
        return $letter;
    }
}

function nombreAlaeotorioDeArchivo($length, $directory = '', $extension = '')
{
    // default to this files directory if empty...
    $dir = !empty($directory) && is_dir($directory) ? $directory : dirname(__FILE__);

    do {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
    } while (file_exists($dir . '/' . $key . (!empty($extension) ? '.' . $extension : '')));

    return $key . (!empty($extension) ? '.' . $extension : '');
}


?>
