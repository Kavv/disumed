<?php 
    if(!isset($_SESSION['id']))
    require($r . 'incluir/session.php');
    if(!isset($db))
    require($r . 'incluir/connection.php');

    $qrylog = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = '".$_SESSION['id']."'");
    $rowlog = $qrylog->fetch(PDO::FETCH_ASSOC);
    
    // Puede ver consultas?
    $can_see_consult = $db->query("SELECT cdiid FROM perfil_reporte INNER JOIN consultasdinamicas ON reporte = cdiid WHERE perfil = ".$rowlog['perid']." AND tipo = 1")->fetch(PDO::FETCH_ASSOC);
    
    // Puede ver informes?
    $can_see_report = $db->query("SELECT idiid FROM perfil_reporte INNER JOIN informesdinamicos ON reporte = idiid WHERE ocultar = false AND perfil = ".$rowlog['perid']." AND tipo = 2")->fetch(PDO::FETCH_ASSOC);
    

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <title><?php echo $titulo;?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="shortcut icon" href="<?php echo $r?>imagenes/fav-disumed.png">
    <link href="<?php echo $r ?>incluir/dashboard/css/style.min.css" rel="stylesheet">
    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>
    
    <?php require($r . 'incluir/src/head.php');?>
    <?php require($r . 'incluir/src/head-form.php');?>

    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $r.'incluir/kavv_js/kavvdt.js'?>" ></script>


    
    <style>
        #cargando {
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            background: #00000059;
            width: 100%;
            z-index: 2000;
            display: none;
        }
        .centrar-todo {
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            height: 100%
        }
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin6">
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="<?php echo $r ?>home.php">
                        <!-- Logo text -->
                        <span class="logo-text">
                            <!-- dark Logo text -->
                            <img class=" img-fluid" src="<?php echo $r. "imagenes/Disumed-logo.jpg"?>" alt="homepage" />
                        </span>
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <a class="nav-toggler waves-effect waves-light text-dark d-block d-md-none"
                        href="javascript:void(0)"><i class="fas fa-bars"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                   
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav ms-auto d-flex align-items-center px-3">

                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li>
                            <a class="profile-pic" href="#"><?php echo $rowlog['usunombre']?> </a>
                        </li>
                        
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class=" in">
                            <a id="close" class="close msj_sesion text-light" href="<?php echo $r ?>incluir/logout.php" title="cerrar sesion"> 
                                <i class="fas fa-times-circle"></i>
                            </a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <!-- User Profile-->
                        <li class="sidebar-item pt-2">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo $r?>modulos/Clientes/index.php"
                                aria-expanded="false">
                                <i class="fas fa-user-check"></i>
                                <span class="hide-menu ml-1">Clientes</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo $r?>modulos/Trabajadores/index.php"
                                aria-expanded="false">
                                <i class="fa fa-table" aria-hidden="true"></i>
                                <span class="hide-menu">Trabajadores</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo $r?>modulos/Roles/index.php"
                                aria-expanded="false">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span class="hide-menu">Roles</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo $r?>modulos/Factura/index.php"
                                aria-expanded="false">
                                <i class="fa fa-font" aria-hidden="true"></i>
                                <span class="hide-menu">Facturación</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo $r?>modulos/Carteras/index.php"
                                aria-expanded="false">
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                <span class="hide-menu">Cartera</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo $r?>modulos/Inventario/index.php"
                                aria-expanded="false">
                                <i class="fa fa-columns" aria-hidden="true"></i>
                                <span class="hide-menu">Inventario</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo $r?>modulos/Reporteria/index.php"
                                aria-expanded="false">
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                <span class="hide-menu">Reporteria</span>
                            </a>
                        </li>
                    </ul>

                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" id="contenido-principal">
                <div id="msj"></div>

                <?php 
                    if($vista != '')
                        require($r.'modulos/'.$vista)
                ?>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center"> 2021 © Team
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo $r ?>incluir/dashboard/dist/js/bootstrap.bundle.min.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo $r ?>incluir/dashboard/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo $r ?>incluir/dashboard/js/custom.js"></script>

    <script src="<?php echo $r.'incluir/kavv_js/makemsj.js'?>" ></script>

    <!--This page JavaScript -->
    <script>
        function carga(){
            $("#cargando").css('display', 'block');
        }
        function ocultarCarga(){
            $("#cargando").css('display', 'none');
        }
        
        // NOTIFICACIONES
        $(document).ready(function(){
            notificacion({'error': '<?php echo $_GET['error'] ?? '' ?>', 'exito': '<?php echo $_GET['mensaje'] ?? '' ?>'});
        });
    </script>

    <!-- PANTALLA DE CARGA -->
    <div id="cargando">
        <div class="centrar-todo">
            <div class="spinner-border" role="status">
                <span class="sr-only"></span>
            </div>
        </div>
    </div>

</body>

</html>