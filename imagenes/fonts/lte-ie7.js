/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'iconos\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-home' : '&#x21;',
			'icon-home-2' : '&#x22;',
			'icon-home-3' : '&#x23;',
			'icon-office' : '&#x24;',
			'icon-newspaper' : '&#x25;',
			'icon-pencil' : '&#x26;',
			'icon-pencil-2' : '&#x27;',
			'icon-quill' : '&#x28;',
			'icon-pen' : '&#x29;',
			'icon-blog' : '&#x2a;',
			'icon-droplet' : '&#x2b;',
			'icon-paint-format' : '&#x2c;',
			'icon-image' : '&#x2d;',
			'icon-image-2' : '&#x2e;',
			'icon-images' : '&#x2f;',
			'icon-camera' : '&#x30;',
			'icon-music' : '&#x31;',
			'icon-headphones' : '&#x32;',
			'icon-play' : '&#x33;',
			'icon-film' : '&#x34;',
			'icon-camera-2' : '&#x35;',
			'icon-dice' : '&#x36;',
			'icon-pacman' : '&#x37;',
			'icon-spades' : '&#x38;',
			'icon-folder-open' : '&#x39;',
			'icon-folder' : '&#x3a;',
			'icon-stack' : '&#x3b;',
			'icon-paste' : '&#x3c;',
			'icon-paste-2' : '&#x3d;',
			'icon-paste-3' : '&#x3e;',
			'icon-copy' : '&#x3f;',
			'icon-copy-2' : '&#x40;',
			'icon-copy-3' : '&#x41;',
			'icon-file' : '&#x42;',
			'icon-file-2' : '&#x43;',
			'icon-file-3' : '&#x44;',
			'icon-profile' : '&#x45;',
			'icon-file-4' : '&#x46;',
			'icon-library' : '&#x47;',
			'icon-books' : '&#x48;',
			'icon-book' : '&#x49;',
			'icon-feed' : '&#x4a;',
			'icon-connection' : '&#x4b;',
			'icon-podcast' : '&#x4c;',
			'icon-bullhorn' : '&#x4d;',
			'icon-pawn' : '&#x4e;',
			'icon-diamonds' : '&#x4f;',
			'icon-clubs' : '&#x50;',
			'icon-tag' : '&#x51;',
			'icon-tags' : '&#x52;',
			'icon-barcode' : '&#x53;',
			'icon-qrcode' : '&#x54;',
			'icon-ticket' : '&#x55;',
			'icon-cart' : '&#x56;',
			'icon-cart-2' : '&#x57;',
			'icon-cart-3' : '&#x58;',
			'icon-coin' : '&#x59;',
			'icon-credit' : '&#x5a;',
			'icon-calculate' : '&#x5b;',
			'icon-support' : '&#x5c;',
			'icon-phone' : '&#x5d;',
			'icon-address-book' : '&#x5e;',
			'icon-phone-hang-up' : '&#x5f;',
			'icon-notebook' : '&#x60;',
			'icon-envelop' : '&#x61;',
			'icon-pushpin' : '&#x62;',
			'icon-location' : '&#x63;',
			'icon-location-2' : '&#x64;',
			'icon-compass' : '&#x65;',
			'icon-map' : '&#x66;',
			'icon-map-2' : '&#x67;',
			'icon-history' : '&#x68;',
			'icon-download' : '&#x69;',
			'icon-box-remove' : '&#x6a;',
			'icon-box-add' : '&#x6b;',
			'icon-drawer' : '&#x6c;',
			'icon-drawer-2' : '&#x6d;',
			'icon-drawer-3' : '&#x6e;',
			'icon-cabinet' : '&#x6f;',
			'icon-tv' : '&#x70;',
			'icon-tablet' : '&#x71;',
			'icon-mobile' : '&#x72;',
			'icon-mobile-2' : '&#x73;',
			'icon-laptop' : '&#x74;',
			'icon-screen' : '&#x75;',
			'icon-keyboard' : '&#x76;',
			'icon-print' : '&#x77;',
			'icon-calendar' : '&#x78;',
			'icon-calendar-2' : '&#x79;',
			'icon-stopwatch' : '&#x7a;',
			'icon-bell' : '&#x7b;',
			'icon-alarm' : '&#x7c;',
			'icon-alarm-2' : '&#x7d;',
			'icon-clock' : '&#x7e;',
			'icon-clock-2' : '&#xe000;',
			'icon-disk' : '&#xe001;',
			'icon-storage' : '&#xe002;',
			'icon-undo' : '&#xe003;',
			'icon-redo' : '&#xe004;',
			'icon-flip' : '&#xe005;',
			'icon-flip-2' : '&#xe006;',
			'icon-forward' : '&#xe007;',
			'icon-undo-2' : '&#xe008;',
			'icon-redo-2' : '&#xe009;',
			'icon-reply' : '&#xe00a;',
			'icon-bubble' : '&#xe00b;',
			'icon-bubbles' : '&#xe00c;',
			'icon-bubbles-2' : '&#xe00d;',
			'icon-bubble-2' : '&#xe00e;',
			'icon-bubbles-3' : '&#xe00f;',
			'icon-bubbles-4' : '&#xe010;',
			'icon-users' : '&#xe011;',
			'icon-user' : '&#xe012;',
			'icon-user-2' : '&#xe013;',
			'icon-users-2' : '&#xe014;',
			'icon-user-3' : '&#xe015;',
			'icon-quotes-left' : '&#xe016;',
			'icon-user-4' : '&#xe017;',
			'icon-busy' : '&#xe018;',
			'icon-upload' : '&#xe019;',
			'icon-cogs' : '&#xe01a;',
			'icon-cog' : '&#xe01b;',
			'icon-equalizer' : '&#xe01c;',
			'icon-settings' : '&#xe01d;',
			'icon-wrench' : '&#xe01e;',
			'icon-unlocked' : '&#xe01f;',
			'icon-lock' : '&#xe020;',
			'icon-lock-2' : '&#xe021;',
			'icon-key' : '&#xe022;',
			'icon-key-2' : '&#xe023;',
			'icon-contract' : '&#xe024;',
			'icon-expand' : '&#xe025;',
			'icon-contract-2' : '&#xe026;',
			'icon-zoom-out' : '&#xe027;',
			'icon-expand-2' : '&#xe028;',
			'icon-zoom-in' : '&#xe029;',
			'icon-binoculars' : '&#xe02a;',
			'icon-search' : '&#xe02b;',
			'icon-spinner' : '&#xe02c;',
			'icon-spinner-2' : '&#xe02d;',
			'icon-spinner-3' : '&#xe02e;',
			'icon-spinner-4' : '&#xe02f;',
			'icon-spinner-5' : '&#xe030;',
			'icon-spinner-6' : '&#xe031;',
			'icon-cog-2' : '&#xe032;',
			'icon-wand' : '&#xe033;',
			'icon-hammer' : '&#xe034;',
			'icon-aid' : '&#xe035;',
			'icon-bug' : '&#xe036;',
			'icon-pie' : '&#xe037;',
			'icon-stats' : '&#xe038;',
			'icon-bars' : '&#xe039;',
			'icon-bars-2' : '&#xe03a;',
			'icon-gift' : '&#xe03b;',
			'icon-trophy' : '&#xe03c;',
			'icon-glass' : '&#xe03d;',
			'icon-mug' : '&#xe03e;',
			'icon-food' : '&#xe03f;',
			'icon-leaf' : '&#xe040;',
			'icon-rocket' : '&#xe041;',
			'icon-meter' : '&#xe042;',
			'icon-hammer-2' : '&#xe043;',
			'icon-dashboard' : '&#xe044;',
			'icon-meter2' : '&#xe045;',
			'icon-fire' : '&#xe046;',
			'icon-lab' : '&#xe047;',
			'icon-magnet' : '&#xe048;',
			'icon-remove' : '&#xe049;',
			'icon-download-2' : '&#xe04a;',
			'icon-upload-2' : '&#xe04b;',
			'icon-download-3' : '&#xe04c;',
			'icon-cloud-upload' : '&#xe04d;',
			'icon-cloud-download' : '&#xe04e;',
			'icon-menu' : '&#xe04f;',
			'icon-tree' : '&#xe050;',
			'icon-numbered-list' : '&#xe051;',
			'icon-list' : '&#xe052;',
			'icon-list-2' : '&#xe053;',
			'icon-signup' : '&#xe054;',
			'icon-power-cord' : '&#xe055;',
			'icon-switch' : '&#xe056;',
			'icon-lightning' : '&#xe057;',
			'icon-target' : '&#xe058;',
			'icon-shield' : '&#xe059;',
			'icon-accessibility' : '&#xe05a;',
			'icon-truck' : '&#xe05b;',
			'icon-road' : '&#xe05c;',
			'icon-airplane' : '&#xe05d;',
			'icon-briefcase' : '&#xe05e;',
			'icon-remove-2' : '&#xe05f;',
			'icon-upload-3' : '&#xe060;',
			'icon-globe' : '&#xe061;',
			'icon-earth' : '&#xe062;',
			'icon-flag' : '&#xe063;',
			'icon-link' : '&#xe064;',
			'icon-attachment' : '&#xe065;',
			'icon-eye' : '&#xe066;',
			'icon-eye-blocked' : '&#xe067;',
			'icon-eye-2' : '&#xe068;',
			'icon-bookmark' : '&#xe069;',
			'icon-bookmarks' : '&#xe06a;',
			'icon-brightness-medium' : '&#xe06b;',
			'icon-brightness-contrast' : '&#xe06c;',
			'icon-star' : '&#xe06d;',
			'icon-star-2' : '&#xe06e;',
			'icon-contrast' : '&#xe06f;',
			'icon-star-3' : '&#xe070;',
			'icon-menu-2' : '&#xe071;',
			'icon-cloud' : '&#xe072;',
			'icon-heart' : '&#xe073;',
			'icon-heart-2' : '&#xe074;',
			'icon-heart-broken' : '&#xe075;',
			'icon-thumbs-up' : '&#xe076;',
			'icon-thumbs-up-2' : '&#xe077;',
			'icon-happy' : '&#xe078;',
			'icon-happy-2' : '&#xe079;',
			'icon-wondering' : '&#xe07a;',
			'icon-neutral' : '&#xe07b;',
			'icon-neutral-2' : '&#xe07c;',
			'icon-wondering-2' : '&#xe07d;',
			'icon-confused' : '&#xe07e;',
			'icon-confused-2' : '&#xe07f;',
			'icon-shocked' : '&#xe080;',
			'icon-shocked-2' : '&#xe081;',
			'icon-evil' : '&#xe082;',
			'icon-evil-2' : '&#xe083;',
			'icon-angry' : '&#xe084;',
			'icon-angry-2' : '&#xe085;',
			'icon-cool' : '&#xe086;',
			'icon-cool-2' : '&#xe087;',
			'icon-grin' : '&#xe088;',
			'icon-grin-2' : '&#xe089;',
			'icon-wink' : '&#xe08a;',
			'icon-wink-2' : '&#xe08b;',
			'icon-sad' : '&#xe08c;',
			'icon-sad-2' : '&#xe08d;',
			'icon-tongue' : '&#xe08e;',
			'icon-tongue-2' : '&#xe08f;',
			'icon-smiley' : '&#xe090;',
			'icon-smiley-2' : '&#xe091;',
			'icon-point-up' : '&#xe092;',
			'icon-point-right' : '&#xe093;',
			'icon-point-down' : '&#xe094;',
			'icon-point-left' : '&#xe095;',
			'icon-warning' : '&#xe096;',
			'icon-question' : '&#xe097;',
			'icon-notification' : '&#xe098;',
			'icon-info' : '&#xe099;',
			'icon-info-2' : '&#xe09a;',
			'icon-cancel-circle' : '&#xe09b;',
			'icon-blocked' : '&#xe09c;',
			'icon-checkmark-circle' : '&#xe09d;',
			'icon-spam' : '&#xe09e;',
			'icon-close' : '&#xe09f;',
			'icon-checkmark' : '&#xe0a0;',
			'icon-checkmark-2' : '&#xe0a1;',
			'icon-spell-check' : '&#xe0a2;',
			'icon-minus' : '&#xe0a3;',
			'icon-plus' : '&#xe0a4;',
			'icon-enter' : '&#xe0a5;',
			'icon-exit' : '&#xe0a6;',
			'icon-play-2' : '&#xe0a7;',
			'icon-pause' : '&#xe0a8;',
			'icon-stop' : '&#xe0a9;',
			'icon-arrow-up-left' : '&#xe0aa;',
			'icon-shuffle' : '&#xe0ab;',
			'icon-loop' : '&#xe0ac;',
			'icon-loop-2' : '&#xe0ad;',
			'icon-loop-3' : '&#xe0ae;',
			'icon-volume-decrease' : '&#xe0af;',
			'icon-volume-increase' : '&#xe0b0;',
			'icon-volume-mute' : '&#xe0b1;',
			'icon-volume-mute-2' : '&#xe0b2;',
			'icon-volume-low' : '&#xe0b3;',
			'icon-backward' : '&#xe0b4;',
			'icon-forward-2' : '&#xe0b5;',
			'icon-play-3' : '&#xe0b6;',
			'icon-pause-2' : '&#xe0b7;',
			'icon-stop-2' : '&#xe0b8;',
			'icon-backward-2' : '&#xe0b9;',
			'icon-forward-3' : '&#xe0ba;',
			'icon-first' : '&#xe0bb;',
			'icon-last' : '&#xe0bc;',
			'icon-previous' : '&#xe0bd;',
			'icon-eject' : '&#xe0be;',
			'icon-volume-high' : '&#xe0bf;',
			'icon-next' : '&#xe0c0;',
			'icon-volume-medium' : '&#xe0c1;',
			'icon-arrow-up' : '&#xe0c2;',
			'icon-arrow-up-right' : '&#xe0c3;',
			'icon-arrow-right' : '&#xe0c4;',
			'icon-arrow-down-right' : '&#xe0c5;',
			'icon-arrow-down' : '&#xe0c6;',
			'icon-arrow-down-left' : '&#xe0c7;',
			'icon-arrow-left' : '&#xe0c8;',
			'icon-arrow-up-left-2' : '&#xe0c9;',
			'icon-arrow-up-2' : '&#xe0ca;',
			'icon-arrow-up-right-2' : '&#xe0cb;',
			'icon-arrow-right-2' : '&#xe0cc;',
			'icon-arrow-down-2' : '&#xe0cd;',
			'icon-arrow-down-right-2' : '&#xe0ce;',
			'icon-arrow-down-left-2' : '&#xe0cf;',
			'icon-arrow-left-2' : '&#xe0d0;',
			'icon-arrow-up-left-3' : '&#xe0d1;',
			'icon-arrow-up-right-3' : '&#xe0d2;',
			'icon-arrow-up-3' : '&#xe0d3;',
			'icon-arrow-right-3' : '&#xe0d4;',
			'icon-arrow-down-right-3' : '&#xe0d5;',
			'icon-arrow-down-3' : '&#xe0d6;',
			'icon-arrow-down-left-3' : '&#xe0d7;',
			'icon-arrow-left-3' : '&#xe0d8;',
			'icon-tab' : '&#xe0d9;',
			'icon-right-to-left' : '&#xe0da;',
			'icon-left-to-right' : '&#xe0db;',
			'icon-pilcrow' : '&#xe0dc;',
			'icon-insert-template' : '&#xe0dd;',
			'icon-table' : '&#xe0de;',
			'icon-table-2' : '&#xe0df;',
			'icon-sigma' : '&#xe0e0;',
			'icon-omega' : '&#xe0e1;',
			'icon-strikethrough' : '&#xe0e2;',
			'icon-italic' : '&#xe0e3;',
			'icon-underline' : '&#xe0e4;',
			'icon-text-width' : '&#xe0e5;',
			'icon-bold' : '&#xe0e6;',
			'icon-text-height' : '&#xe0e7;',
			'icon-font' : '&#xe0e8;',
			'icon-filter' : '&#xe0e9;',
			'icon-filter-2' : '&#xe0ea;',
			'icon-scissors' : '&#xe0eb;',
			'icon-crop' : '&#xe0ec;',
			'icon-radio-unchecked' : '&#xe0ed;',
			'icon-radio-checked' : '&#xe0ee;',
			'icon-checkbox-partial' : '&#xe0ef;',
			'icon-checkbox-unchecked' : '&#xe0f0;',
			'icon-checkbox-checked' : '&#xe0f1;',
			'icon-paragraph-left' : '&#xe0f2;',
			'icon-paragraph-center' : '&#xe0f3;',
			'icon-paragraph-right' : '&#xe0f4;',
			'icon-paragraph-left-2' : '&#xe0f5;',
			'icon-paragraph-center-2' : '&#xe0f6;',
			'icon-paragraph-justify' : '&#xe0f7;',
			'icon-paragraph-right-2' : '&#xe0f8;',
			'icon-paragraph-justify-2' : '&#xe0f9;',
			'icon-indent-increase' : '&#xe0fa;',
			'icon-new-tab' : '&#xe0fb;',
			'icon-indent-decrease' : '&#xe0fc;',
			'icon-embed' : '&#xe0fd;',
			'icon-code' : '&#xe0fe;',
			'icon-console' : '&#xe0ff;',
			'icon-share' : '&#xe100;',
			'icon-mail' : '&#xe101;',
			'icon-mail-2' : '&#xe102;',
			'icon-mail-3' : '&#xe103;',
			'icon-mail-4' : '&#xe104;',
			'icon-google' : '&#xe105;',
			'icon-google-plus' : '&#xe106;',
			'icon-google-plus-2' : '&#xe107;',
			'icon-google-plus-3' : '&#xe108;',
			'icon-google-plus-4' : '&#xe109;',
			'icon-dribbble' : '&#xe10a;',
			'icon-picassa' : '&#xe10b;',
			'icon-finder' : '&#xe10c;',
			'icon-paypal' : '&#xe10d;',
			'icon-paypal-2' : '&#xe10e;',
			'icon-apple' : '&#xe10f;',
			'icon-tux' : '&#xe110;',
			'icon-picassa-2' : '&#xe111;',
			'icon-paypal-3' : '&#xe112;',
			'icon-foursquare' : '&#xe113;',
			'icon-yahoo' : '&#xe114;',
			'icon-flickr' : '&#xe115;',
			'icon-flickr-2' : '&#xe116;',
			'icon-tumblr' : '&#xe117;',
			'icon-foursquare-2' : '&#xe118;',
			'icon-flattr' : '&#xe119;',
			'icon-tumblr-2' : '&#xe11a;',
			'icon-flickr-3' : '&#xe11b;',
			'icon-flickr-4' : '&#xe11c;',
			'icon-blogger' : '&#xe11d;',
			'icon-xing' : '&#xe11e;',
			'icon-safari' : '&#xe11f;',
			'icon-IcoMoon' : '&#xe120;',
			'icon-opera' : '&#xe121;',
			'icon-xing-2' : '&#xe122;',
			'icon-blogger-2' : '&#xe123;',
			'icon-lanyrd' : '&#xe124;',
			'icon-vimeo' : '&#xe125;',
			'icon-joomla' : '&#xe126;',
			'icon-pinterest' : '&#xe127;',
			'icon-IE' : '&#xe128;',
			'icon-firefox' : '&#xe129;',
			'icon-pinterest-2' : '&#xe12a;',
			'icon-wordpress' : '&#xe12b;',
			'icon-vimeo2' : '&#xe12c;',
			'icon-vimeo-2' : '&#xe12d;',
			'icon-youtube' : '&#xe12e;',
			'icon-youtube-2' : '&#xe12f;',
			'icon-github' : '&#xe130;',
			'icon-github-2' : '&#xe131;',
			'icon-wordpress-2' : '&#xe132;',
			'icon-stackoverflow' : '&#xe133;',
			'icon-chrome' : '&#xe134;',
			'icon-css3' : '&#xe135;',
			'icon-stumbleupon' : '&#xe136;',
			'icon-stumbleupon-2' : '&#xe137;',
			'icon-html5' : '&#xe138;',
			'icon-html5-2' : '&#xe139;',
			'icon-delicious' : '&#xe13a;',
			'icon-github-3' : '&#xe13b;',
			'icon-feed-2' : '&#xe13c;',
			'icon-feed-3' : '&#xe13d;',
			'icon-github-4' : '&#xe13e;',
			'icon-lastfm' : '&#xe13f;',
			'icon-file-css' : '&#xe140;',
			'icon-file-xml' : '&#xe141;',
			'icon-lastfm-2' : '&#xe142;',
			'icon-github-5' : '&#xe143;',
			'icon-feed-4' : '&#xe144;',
			'icon-twitter' : '&#xe145;',
			'icon-steam' : '&#xe146;',
			'icon-linkedin' : '&#xe147;',
			'icon-file-powerpoint' : '&#xe148;',
			'icon-file-zip' : '&#xe149;',
			'icon-reddit' : '&#xe14a;',
			'icon-steam-2' : '&#xe14b;',
			'icon-twitter-2' : '&#xe14c;',
			'icon-twitter-3' : '&#xe14d;',
			'icon-file-excel' : '&#xe14e;',
			'icon-skype' : '&#xe14f;',
			'icon-deviantart' : '&#xe150;',
			'icon-deviantart-2' : '&#xe151;',
			'icon-instagram' : '&#xe152;',
			'icon-soundcloud' : '&#xe153;',
			'icon-file-word' : '&#xe154;',
			'icon-file-openoffice' : '&#xe155;',
			'icon-soundcloud-2' : '&#xe156;',
			'icon-forrst' : '&#xe157;',
			'icon-facebook' : '&#xe158;',
			'icon-facebook-2' : '&#xe159;',
			'icon-windows8' : '&#xe15a;',
			'icon-forrst-2' : '&#xe15b;',
			'icon-file-pdf' : '&#xe15c;',
			'icon-libreoffice' : '&#xe15d;',
			'icon-windows' : '&#xe15e;',
			'icon-dribbble-2' : '&#xe15f;',
			'icon-facebook-3' : '&#xe160;',
			'icon-google-drive' : '&#xe161;',
			'icon-dribbble-3' : '&#xe162;',
			'icon-android' : '&#xe163;',
			'icon-yelp' : '&#xe164;',
			'icon-warning-2' : '&#xe165;',
			'icon-minus-2' : '&#xe166;',
			'icon-calendar-3' : '&#xe167;',
			'icon-flag-2' : '&#xe168;',
			'icon-bookmark-2' : '&#xe169;',
			'icon-camera-3' : '&#xe16a;',
			'icon-ipod' : '&#xe16b;',
			'icon-window' : '&#xe16c;',
			'icon-picture' : '&#xe16d;',
			'icon-cloud-2' : '&#xe16e;',
			'icon-locked' : '&#xe16f;',
			'icon-folder-2' : '&#xe170;',
			'icon-monitor' : '&#xe171;',
			'icon-trashcan' : '&#xe172;',
			'icon-lamp' : '&#xe173;',
			'icon-disk-2' : '&#xe174;',
			'icon-bolt' : '&#xe175;',
			'icon-mobile-3' : '&#xe176;',
			'icon-camera-4' : '&#xe177;',
			'icon-inbox' : '&#xe178;',
			'icon-comment' : '&#xe179;',
			'icon-search-2' : '&#xe17a;',
			'icon-droplet-2' : '&#xe17b;',
			'icon-ampersand' : '&#xe17c;',
			'icon-button' : '&#xe17d;',
			'icon-database' : '&#xe17e;',
			'icon-compass-2' : '&#xe17f;',
			'icon-mouse' : '&#xe180;',
			'icon-dribbble-4' : '&#xe181;',
			'icon-mic' : '&#xe182;',
			'icon-envelope' : '&#xe183;',
			'icon-forrst-3' : '&#xe184;',
			'icon-refresh' : '&#xe185;',
			'icon-list-3' : '&#xe186;',
			'icon-credit-card' : '&#xe187;',
			'icon-atom' : '&#xe188;',
			'icon-grid' : '&#xe189;',
			'icon-location-3' : '&#xe18a;',
			'icon-feed-5' : '&#xe18b;',
			'icon-briefcase-2' : '&#xe18c;',
			'icon-cart-4' : '&#xe18d;',
			'icon-blocked-2' : '&#xe18e;',
			'icon-tag-2' : '&#xe18f;',
			'icon-volume' : '&#xe190;',
			'icon-winsows' : '&#xe191;',
			'icon-target-2' : '&#xe192;',
			'icon-volume-2' : '&#xe193;',
			'icon-phone-2' : '&#xe194;',
			'icon-target-3' : '&#xe195;',
			'icon-contrast-2' : '&#xe196;',
			'icon-play-4' : '&#xe197;',
			'icon-star-4' : '&#xe198;',
			'icon-stats-2' : '&#xe199;',
			'icon-battery' : '&#xe19a;',
			'icon-clock-3' : '&#xe19b;',
			'icon-user-5' : '&#xe19c;',
			'icon-pause-3' : '&#xe19d;',
			'icon-pointer' : '&#xe19e;',
			'icon-target-4' : '&#xe19f;',
			'icon-code-2' : '&#xe1a0;',
			'icon-forward-4' : '&#xe1a1;',
			'icon-thumbs-up-3' : '&#xe1a2;',
			'icon-bug-2' : '&#xe1a3;',
			'icon-cog-3' : '&#xe1a4;',
			'icon-music-2' : '&#xe1a5;',
			'icon-console-2' : '&#xe1a6;',
			'icon-thumbs-down' : '&#xe1a7;',
			'icon-paperclip' : '&#xe1a8;',
			'icon-keyboard-2' : '&#xe1a9;',
			'icon-headphones-2' : '&#xe1aa;',
			'icon-film-2' : '&#xe1ab;',
			'icon-twitter-4' : '&#xe1ac;',
			'icon-pencil-3' : '&#xe1ad;',
			'icon-type' : '&#xe1ae;',
			'icon-move' : '&#xe1af;',
			'icon-crop-2' : '&#xe1b0;',
			'icon-floppy' : '&#xe1b1;',
			'icon-checkmark-3' : '&#xe1b2;',
			'icon-home-4' : '&#xe1b3;',
			'icon-switch-2' : '&#xe1b4;',
			'icon-earth-2' : '&#xe1b5;',
			'icon-cancel' : '&#xe1b6;',
			'icon-filter-3' : '&#xe1b7;',
			'icon-trophy-2' : '&#xe1b8;',
			'icon-skype-2' : '&#xe1b9;',
			'icon-location-4' : '&#xe1ba;',
			'icon-star-5' : '&#xe1bb;',
			'icon-key-3' : '&#xe1bc;',
			'icon-info-3' : '&#xe1bd;',
			'icon-frame' : '&#xe1be;',
			'icon-gift-2' : '&#xe1bf;',
			'icon-diary' : '&#xe1c0;',
			'icon-cone' : '&#xe1c1;',
			'icon-eye-3' : '&#xe1c2;',
			'icon-chart' : '&#xe1c3;',
			'icon-address-book-2' : '&#xe1c4;',
			'icon-stop-3' : '&#xe1c5;',
			'icon-alarm-3' : '&#xe1c6;',
			'icon-heart-3' : '&#xe1c7;',
			'icon-apple-2' : '&#xe1c8;',
			'icon-file-5' : '&#xe1c9;',
			'icon-bookmark-3' : '&#xe1ca;',
			'icon-coffee' : '&#xe1cb;',
			'icon-smiley-3' : '&#xe1cc;',
			'icon-shit' : '&#xe1cd;',
			'icon-basket' : '&#xe1ce;',
			'icon-wrench-2' : '&#xe1cf;',
			'icon-plus-2' : '&#xe1d0;',
			'icon-sunrise' : '&#xe1d1;',
			'icon-cloudy' : '&#xe1d2;',
			'icon-cloud-3' : '&#xe1d3;',
			'icon-sun' : '&#xe1d4;',
			'icon-moon' : '&#xe1d5;',
			'icon-lightning-2' : '&#xe1d6;',
			'icon-sun-2' : '&#xe1d7;',
			'icon-sun-3' : '&#xe1d8;',
			'icon-windy' : '&#xe1d9;',
			'icon-moon-2' : '&#xe1da;',
			'icon-cloudy-2' : '&#xe1db;',
			'icon-wind' : '&#xe1dc;',
			'icon-snowflake' : '&#xe1dd;',
			'icon-cloud-4' : '&#xe1de;',
			'icon-cloud-5' : '&#xe1df;',
			'icon-cloudy-3' : '&#xe1e0;',
			'icon-cloud-6' : '&#xe1e1;',
			'icon-lightning-3' : '&#xe1e2;',
			'icon-rainy' : '&#xe1e3;',
			'icon-weather' : '&#xe1e4;',
			'icon-weather-2' : '&#xe1e5;',
			'icon-rainy-2' : '&#xe1e6;',
			'icon-windy-2' : '&#xe1e7;',
			'icon-weather-3' : '&#xe1e8;',
			'icon-lines' : '&#xe1e9;',
			'icon-windy-3' : '&#xe1ea;',
			'icon-snowy' : '&#xe1eb;',
			'icon-cloud-7' : '&#xe1ec;',
			'icon-lightning-4' : '&#xe1ed;',
			'icon-snowy-2' : '&#xe1ee;',
			'icon-weather-4' : '&#xe1ef;',
			'icon-lightning-5' : '&#xe1f0;',
			'icon-rainy-3' : '&#xe1f1;',
			'icon-cloudy-4' : '&#xe1f2;',
			'icon-lightning-6' : '&#xe1f3;',
			'icon-rainy-4' : '&#xe1f4;',
			'icon-windy-4' : '&#xe1f5;',
			'icon-thermometer' : '&#xe1f6;',
			'icon-compass-3' : '&#xe1f7;',
			'icon-windy-5' : '&#xe1f8;',
			'icon-snowy-3' : '&#xe1f9;',
			'icon-none' : '&#xe1fa;',
			'icon-Celsius' : '&#xe1fb;',
			'icon-snowy-4' : '&#xe1fc;',
			'icon-snowy-5' : '&#xe1fd;',
			'icon-Fahrenheit' : '&#xe1fe;',
			'icon-chat' : '&#xe1ff;',
			'icon-mail-5' : '&#xe200;',
			'icon-beaker-alt' : '&#xe201;',
			'icon-arrow-left-4' : '&#xe202;',
			'icon-play-alt' : '&#xe203;',
			'icon-article' : '&#xe204;',
			'icon-eye-4' : '&#xe205;',
			'icon-cloud-download-2' : '&#xe206;',
			'icon-cloud-upload-2' : '&#xe207;',
			'icon-user-6' : '&#xe208;',
			'icon-read-more' : '&#xe209;',
			'icon-fullscreen-exit' : '&#xe20a;',
			'icon-arrow-left-alt1' : '&#xe20b;',
			'icon-beaker' : '&#xe20c;',
			'icon-heart-stroke' : '&#xe20d;',
			'icon-chat-alt-stroke' : '&#xe20e;',
			'icon-chat-alt-fill' : '&#xe20f;',
			'icon-heart-fill' : '&#xe210;',
			'icon-key-stroke' : '&#xe211;',
			'icon-arrow-left-alt2' : '&#xe212;',
			'icon-fullscreen-exit-alt' : '&#xe213;',
			'icon-list-4' : '&#xe214;',
			'icon-home-5' : '&#xe215;',
			'icon-fork' : '&#xe216;',
			'icon-paperclip-2' : '&#xe217;',
			'icon-clock-4' : '&#xe218;',
			'icon-list-nested' : '&#xe219;',
			'icon-fullscreen' : '&#xe21a;',
			'icon-arrow-right-4' : '&#xe21b;',
			'icon-key-fill' : '&#xe21c;',
			'icon-movie' : '&#xe21d;',
			'icon-comment-alt1-stroke' : '&#xe21e;',
			'icon-comment-alt1-fill' : '&#xe21f;',
			'icon-document-alt-stroke' : '&#xe220;',
			'icon-new-window' : '&#xe221;',
			'icon-arrow-right-alt1' : '&#xe222;',
			'icon-book-2' : '&#xe223;',
			'icon-lock-stroke' : '&#xe224;',
			'icon-fullscreen-alt' : '&#xe225;',
			'icon-iphone' : '&#xe226;',
			'icon-arrow-right-alt2' : '&#xe227;',
			'icon-lightbulb' : '&#xe228;',
			'icon-document-alt-fill' : '&#xe229;',
			'icon-comment-stroke' : '&#xe22a;',
			'icon-book-alt' : '&#xe22b;',
			'icon-lock-fill' : '&#xe22c;',
			'icon-unlock-stroke' : '&#xe22d;',
			'icon-book-alt2' : '&#xe22e;',
			'icon-battery-empty' : '&#xe22f;',
			'icon-arrow-up-4' : '&#xe230;',
			'icon-spin-alt' : '&#xe231;',
			'icon-document-stroke' : '&#xe232;',
			'icon-comment-fill' : '&#xe233;',
			'icon-comment-alt2-stroke' : '&#xe234;',
			'icon-document-fill' : '&#xe235;',
			'icon-spin' : '&#xe236;',
			'icon-arrow-up-alt1' : '&#xe237;',
			'icon-battery-half' : '&#xe238;',
			'icon-pen-2' : '&#xe239;',
			'icon-unlock-fill' : '&#xe23a;',
			'icon-tag-stroke' : '&#xe23b;',
			'icon-pen-alt-stroke' : '&#xe23c;',
			'icon-battery-full' : '&#xe23d;',
			'icon-arrow-up-alt2' : '&#xe23e;',
			'icon-curved-arrow' : '&#xe23f;',
			'icon-plus-3' : '&#xe240;',
			'icon-comment-alt2-fill' : '&#xe241;',
			'icon-checkmark-4' : '&#xe242;',
			'icon-plus-alt' : '&#xe243;',
			'icon-arrow-down-4' : '&#xe244;',
			'icon-undo-3' : '&#xe245;',
			'icon-battery-charging' : '&#xe246;',
			'icon-tag-fill' : '&#xe247;',
			'icon-pen-alt-fill' : '&#xe248;',
			'icon-pen-alt2' : '&#xe249;',
			'icon-compass-4' : '&#xe24a;',
			'icon-sun-stroke' : '&#xe24b;',
			'icon-arrow-down-alt1' : '&#xe24c;',
			'icon-reload' : '&#xe24d;',
			'icon-minus-3' : '&#xe24e;',
			'icon-check-alt' : '&#xe24f;',
			'icon-x' : '&#xe250;',
			'icon-minus-alt' : '&#xe251;',
			'icon-reload-alt' : '&#xe252;',
			'icon-arrow-down-alt2' : '&#xe253;',
			'icon-box' : '&#xe254;',
			'icon-brush' : '&#xe255;',
			'icon-sun-fill' : '&#xe256;',
			'icon-moon-stroke' : '&#xe257;',
			'icon-brush-alt' : '&#xe258;',
			'icon-folder-stroke' : '&#xe259;',
			'icon-cd' : '&#xe25a;',
			'icon-loop-4' : '&#xe25b;',
			'icon-pin' : '&#xe25c;',
			'icon-x-altx-alt' : '&#xe25d;',
			'icon-denied' : '&#xe25e;',
			'icon-link-2' : '&#xe25f;',
			'icon-steering-wheel' : '&#xe260;',
			'icon-folder-fill' : '&#xe261;',
			'icon-loop-alt1' : '&#xe262;',
			'icon-eyedropper' : '&#xe263;',
			'icon-moon-fill' : '&#xe264;',
			'icon-cloud-8' : '&#xe265;',
			'icon-layers-alt' : '&#xe266;',
			'icon-at' : '&#xe267;',
			'icon-microphone' : '&#xe268;',
			'icon-loop-alt2' : '&#xe269;',
			'icon-bolt-2' : '&#xe26a;',
			'icon-cursor' : '&#xe26b;',
			'icon-rss' : '&#xe26c;',
			'icon-move-2' : '&#xe26d;',
			'icon-loop-alt3' : '&#xe26e;',
			'icon-headphones-3' : '&#xe26f;',
			'icon-ampersand-2' : '&#xe270;',
			'icon-layers' : '&#xe271;',
			'icon-rain' : '&#xe272;',
			'icon-umbrella' : '&#xe273;',
			'icon-image-3' : '&#xe274;',
			'icon-info-4' : '&#xe275;',
			'icon-volume-3' : '&#xe276;',
			'icon-loop-alt4' : '&#xe277;',
			'icon-move-alt1' : '&#xe278;',
			'icon-rss-alt' : '&#xe279;',
			'icon-wrench-3' : '&#xe27a;',
			'icon-move-alt2' : '&#xe27b;',
			'icon-volume-mute-3' : '&#xe27c;',
			'icon-transfer' : '&#xe27d;',
			'icon-question-mark' : '&#xe27e;',
			'icon-camera-5' : '&#xe27f;',
			'icon-star-6' : '&#xe280;',
			'icon-map-pin-stroke' : '&#xe281;',
			'icon-aperture' : '&#xe282;',
			'icon-pilcrow-2' : '&#xe283;',
			'icon-play-5' : '&#xe284;',
			'icon-move-vertical' : '&#xe285;',
			'icon-equalizer-2' : '&#xe286;',
			'icon-dial' : '&#xe287;',
			'icon-cog-4' : '&#xe288;',
			'icon-award-fill' : '&#xe289;',
			'icon-pause-4' : '&#xe28a;',
			'icon-move-vertical-alt1' : '&#xe28b;',
			'icon-hash' : '&#xe28c;',
			'icon-map-pin-fill' : '&#xe28d;',
			'icon-map-pin-alt' : '&#xe28e;',
			'icon-chart-2' : '&#xe28f;',
			'icon-stop-4' : '&#xe290;',
			'icon-move-vertical-alt2' : '&#xe291;',
			'icon-award-stroke' : '&#xe292;',
			'icon-calendar-4' : '&#xe293;',
			'icon-left-quote' : '&#xe294;',
			'icon-right-quote' : '&#xe295;',
			'icon-aperture-alt' : '&#xe296;',
			'icon-target-5' : '&#xe297;',
			'icon-chart-alt' : '&#xe298;',
			'icon-eject-2' : '&#xe299;',
			'icon-move-horizontal' : '&#xe29a;',
			'icon-magnifying-glass' : '&#xe29b;',
			'icon-calendar-alt-stroke' : '&#xe29c;',
			'icon-calendar-alt-fill' : '&#xe29d;',
			'icon-trash-stroke' : '&#xe29e;',
			'icon-move-horizontal-alt1' : '&#xe29f;',
			'icon-first-2' : '&#xe2a0;',
			'icon-left-quote-alt' : '&#xe2a1;',
			'icon-bars-3' : '&#xe2a2;',
			'icon-download-4' : '&#xe2a3;',
			'icon-upload-4' : '&#xe2a4;',
			'icon-bars-alt' : '&#xe2a5;',
			'icon-right-quote-alt' : '&#xe2a6;',
			'icon-last-2' : '&#xe2a7;',
			'icon-move-horizontal-alt2' : '&#xe2a8;',
			'icon-trash-fill' : '&#xe2a9;',
			'icon-share-2' : '&#xe2aa;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};